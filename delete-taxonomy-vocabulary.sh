# DKDK this is for VB-4394, technically deleting a taxanomy vocabulary
# Note that somehow this operation shows Apache Solr errors but the vocabulary could be deleted anyway
# If Solr is connected via ssh, then the errors do not show up!

# get vid in single line
# drush php-eval '$tax=taxonomy_vocabulary_machine_name_load("dkvoca1"); echo $tax->vid;'
drush scr delete-taxonomy-vocabulary.php