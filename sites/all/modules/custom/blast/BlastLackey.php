<?php

use VectorBase\ToolHelpers\JobSpec;


//require_once(drupal_get_path('module', 'tool_helpers'). '/Lackey.php');
require_once('/vectorbase/web/root/sites/all/modules/custom/tool_helpers/Lackey.php');
class BlastLackey extends VectorBase\ToolHelpers\Lackey {

    protected function buildSubmitFiles(JobSpec $job_specification, $prefix){
        return $this->makeflow_job($job_specification, $prefix);
    }

    protected function makeflow_prologue(JobSpec $job_specification, $prefix, $path_to_result_files) {
	    $job_attribs = $job_specification->get_attributes();
	    $server_ip   = $job_attribs['server_ip'];
	    $binary      = $job_specification->get_program();

	    $prologue = <<<EOF
QUERY_ID=$prefix
QUERY=\$(QUERY_ID).query.fa

DATABASE_IP=$server_ip

BLAST_DEPOT=/vectorbase/dbs
BLAST_BINARIES=/opt/local/blast/bin
BLAST_BINARY=\$(BLAST_BINARIES)/$binary

RESULT_DIR=$path_to_result_files

CORES=1
MEMORY=512
DISK=512

EOF;
	    return $prologue;
    }

    protected function makeflow_analysis_rules(JobSpec $job_specification, $prefix, $path_to_result_files) {
	    $job_attribs = $job_specification->get_attributes();
	    $target_dbs  = $job_specification->get_target_dbs();

	    $extra_args = '';
	    foreach ($job_attribs as $attrib => $value) {
		    if($attrib != 'server_ip') {
			    $extra_args .= "$attrib $value ";
		    }
	    }

	    $rules = '';

	    $i = 0;
	    foreach ($target_dbs as $db) {
		    $rules .= $this->makeflow_one_analysis_rule($db, $extra_args, '_' . $i);
		    $i++;

	    }

	    return $rules;
    }

    protected function makeflow_one_analysis_rule($db, $extra_args, $suffix) {
	    $rules =<<<EOF

# Set filenames in default category, so that they are globally available.
CATEGORY=default

BLAST_NAME=$db
OUTPUT$suffix=\$(QUERY_ID).results.\$(BLAST_NAME).out
ERROR$suffix=\$(QUERY_ID).results.\$(BLAST_NAME).err

# Switch to corresponding category, so that rules can be appropietely labeled. 
# Variables not found in a category, fallback to the default category.
CATEGORY=\$(BLAST_NAME)

# This rule runs blast in the compute node.
\$(OUTPUT$suffix) \$(ERROR$suffix): \$(QUERY)
\t\$(BLAST_BINARY) -query \$(QUERY) -db \$(BLAST_DEPOT)/$(BLAST_NAME) $extra_args > \$(OUTPUT$suffix) 2> \$(ERROR$suffix)

# Transfer of the following files is done locally.
# Copy stdout to result dir
\$(RESULT_DIR)/\$(OUTPUT$suffix): \$(OUTPUT$suffix)
\tLOCAL cp \$(QUERY_ID).results.\$(BLAST_NAME).out \$(RESULT_DIR)

# Copy stderr to result dir
\$(RESULT_DIR)/\$(ERROR$suffix): \$(ERROR$suffix)
\tLOCAL cp \$(QUERY_ID).results.\$(BLAST_NAME).err \$(RESULT_DIR)
EOF;

	    return $rules;
    }


    protected function makeflow_parse_rules(JobSpec $job_specification, $prefix, $path_to_result_files) {
	    $total          = count($job_specification->get_target_dbs());
	    $job_attribs    = $job_specification->get_attributes();
	    $num_alignments = $job_attribs['-num_alignments'];

	    $rule_copy_query =<<<EOF


CATEGORY=default
PARSER=/opt/local/cctools/bin/blastParse.pl
NUM_RESULTS=$num_alignments


# transfer query to result dir, for log purposes.
\$(RESULT_DIR)/\$(QUERY): \$(QUERY)
\tLOCAL /bin/cp \$(QUERY) \$(RESULT_DIR)/

EOF;

# put all names of stdouts and stderr files into variables.
	    $all_outputs = '';
	    $all_errors  = '';
	    for($i = 0; $i < $total; $i++) {
		    $all_outputs .= "\$(OUTPUT_$i) ";
		    $all_errors  .= "\$(ERROR_$i) ";
	    }

	    $rule_concat_all =<<<EOF

# consolidate all stdouts and stderrs into a single file
\$(QUERY_ID): $all_outputs $all_errors
\tLOCAL cat $all_outputs $all_errors > $(QUERY_ID)

EOF;

            $rule_copy =<<<EOF

# copy single file to result dir
\$(RESULT_DIR)/\$(QUERY_ID): \$(QUERY_ID)
\tLOCAL /bin/cp \$(QUERY_ID) \$(RESULT_DIR)/$(QUERY_ID)

EOF;
	    $rule_parse =<<<EOF

# commit to the database. the file %.ribbon is simply created to mark the completion of the rule.
\$(QUERY).ribbon: \$(QUERY_ID)
\tLOCAL \$(PARSER) \$(QUERY_ID) \$(NUM_RESULTS) \$(QUERY_ID) \$(DATABASE_IP) >> \$(QUERY).ribbon

EOF;

	    $rules = '';
	    $rules .= $rule_copy_query;
	    $rules .= $rule_concat_all;
	    $rules .= $rule_parse;
            $rules .= $rule_copy;

	    return $rules;
    }
}

