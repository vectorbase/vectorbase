<?php

// **** load drupal enviornment ****
define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);


blast_listHspByHit($_POST['id'],$_POST['db'], $_POST['data-type'], $_POST['data-num']);

// display a pretty table of hsp info on blast job; should be fed raw blast job id and database filename
//function blast_resultsHspByHit($id, $dbFile){
//This function was also edited in order to create the GFF files here.  The reason is that when
//All databases are used to blast against, the DB calls in generateGFF cause a PROXY error to occur
function blast_listHspByHit($id, $dbFile, $type, $num){
	
	$details='';
	//Modified to get the database_name as well
	$results=db_query("select br_id,query_name,query_description,query_length,algorithm, database_name from blast_results where search_id=$id and database_name='$dbFile'");

	foreach($results as $result){
		$brid=$result->br_id;
		$queryName=$result->query_name;
		$queryLength=$result->query_length;
		$program=$result->algorithm;
		$dbName=$result->database_name;

		//Generate the GFF for the result, by passing info  of the result
		//Uses a modified version of generateGFF where the br_id is the parameter
		//The check is used to make sure that gff file are not being created if
		//it has been created once already
		if (!file_exists("/vectorbase/web/root/data/$id" . "_" . "$brid.gff")) {
			$modifiedQueryName = trim($queryName.' '.$result->query_description);
			blast_generateGFF($id, $brid, $modifiedQueryName, $dbName);
		}
		//get all hit info for this result
		$hits=db_query("select * from blast_hits where br_id=$brid");
		foreach($hits as $hit){
			//get hit name to list all the hsps under
			$hitName=$hit->name;
			$hitDesc = trim($hit->description) ? trim($hit->description) : 'No description available.';
			
			//Adding the following code to remove the redundancy between the Hit and Hit Desc when it
			//it is displayed for EST datasets.
			$hitNameSplit = explode(".", $hitName);
			$hitDesc = trim(str_replace($hitNameSplit[0], "", $hitDesc));
			
			// The following lines come from VB-4261
			$gene_name_and_description = Null;
			$gene_name_match = Null;
			$hit_desc_match = Null;
			
			// the full gene name + description
			preg_match( '/^[^|]*/', $hitDesc, $gene_name_and_description);
			preg_match( '/^[^:]*(?=:)/', $gene_name_and_description[0], $gene_name_match);
			
			// if there is something in gene name match, then it is of the form 'gene_name: gene_description, so pull out only the description
			if( $gene_name_match[0] ){
				preg_match( '/(?<=: ).*/', $gene_name_and_description[0], $hit_desc_match);
			}
			
			// if the above if block was executed, then there was both a gene name and a description, so make sure to grab the description.
			// if not, then grab the entire 'gene name and description' which in this case is only the description since no gene name is present.
			// This logic helps avoid the cases of empty tokens being passed around in edge cases.
			$hitDesc = $hit_desc_match ? $hit_desc_match[0] : $gene_name_and_description[0];

			$hitGeneName = $gene_name_match[0];

			$bhid=$hit->bh_id;
			$hitLength=$hit->length;

			// now find hsps for each hit
			$hsps=db_query("select * from blast_hsps where bh_id=$bhid;");
			foreach($hsps as $hsp){
				$hspId=$hsp->bs_id;
				$hitString=$hsp->hit_string;
				$homoString=$hsp->homology_string;
				$queryString=$hsp->query_string;
				$evalue=$hsp->evalue;
				$score=$hsp->score;
				$identity=$hsp->percent_identity;
				$strandHit=$hsp->strandhit;
				$strandQuery=$hsp->strandquery;
				$startHit=$hsp->starthit;
				$startQuery=$hsp->startquery;
				$endQuery=$hsp->endquery;
				$endHit=$hsp->endhit;
				$frameHit=$hsp->framehit;
				$frameQuery=$hsp->framequery;

				//blastp will always be positive direction and bioperl returns negative for both query and subject
				if($program!="BLASTP"){
					// hit in reverse direction, swap end/start values
					if($strandQuery<0){
						$temp=$startQuery;
						$startQuery=$endQuery;
						$endQuery=$temp;
					}
					if($strandHit<0){
						$temp=$startHit;
						$startHit=$endHit;
						$endHit=$temp;
					}
				}


				if($startQuery>$endQuery){
					$startDisplay=$endQuery;
					$endDisplay=$startQuery;
				}else{
					$startDisplay=$startQuery;
					$endDisplay=$endQuery;
				}

				if($startHit>$endHit){
					$startHitDisplay=$endHit;
					$endHitDisplay=$startHit;
				}else{
					$startHitDisplay=$startHit;
					$endHitDisplay=$endHit;
				}


				if (strlen($hitName)<13){
					$hitNameWidth=strlen($hitName).'em';
				}else
					$hitNameWidth="auto";

				$details.="<tr><td><input type=\"checkbox\" class=\"hsps\" name=\"selectedHsps\" value=\"$hspId\" hstart=\"$startHit\" hend=\"$endHit\" data-num=\"$num\" hitName=\"$hitName\"/></td>
				<td class=\"hspHitName\" ><a class=\"hsp\" id=\"$hspId\" title=\"$hitName\">$hitName</a></td>";
				if( $type == 'Transcripts' || $type == 'Peptides' ) {
					$details.="<td class=\"hspHitGeneName\" title=\"$hitGeneName\">$hitGeneName</td>";
				}
				$details.="<td class=\"hspHitDesc\" title=\"" . trim($hitDesc) . "\">" . trim($hitDesc) . "</td>";
				$details.="<td class=\"hspQueryName\" title=\"$queryName\">$queryName</td>";
				$details.="<td>";
				$details.= ($endQuery - $startQuery + 1);
				$details.= "</td>";
				$details.="<td>$evalue</td>
				<td>$score</td>
				<td>".round($identity,1)."&#37;</td>

				<td style=\"display:none;\">$startDisplay</td>
				".hspHitLocationGraphic($startQuery,$endQuery,$queryLength)."
				<td style=\"display:none;\">$endDisplay</td>
				<td style=\"display:none;\">$startHitDisplay</td>
				".hspHitLocationGraphic($startHit,$endHit,$hitLength)."
				<td style=\"display:none;\">$endHitDisplay</td>
				</tr>";
			}
		}
	}

	// Pass to clustal check
	$toolType = $program; // blastn, tblastn, tblastx, blastp, or blastx
	$passToClustalw = false;
	switch(strtolower($toolType)) {  // Gotta normalize the case on the algorithm variable
		case 'blastn':
		case 'blastp':	
		case 'tblastx':
			$passToClustalw = true;
		break;
	}
	$dataType = $type; // contigs, scaffolds, ESTs, etc. - probably FileDataType field.
	
	$passToClustalwHtml = $passToClustalw ? 'checked' : 'disabled';
	
//	$out.='
//		<div id="hspControl">
//			<fieldset id="hspControlPanel" class="form-wrapper" style="padding:6px;margin:8px;4px;">
//				<legend>Checked Hits</legend>
//				<div id="hspControlMessage">';
//	if($dataType === 'Chromosomes') {
//		$out.='Note: Downloaded hits from <i>Chromosomes</i> are truncated because of their generally large size. See the <a href="downloads">download section</a> for full chromosome sequences.';
//	}
//	$out.='
//				</div>
//				<button type="button" id ="downloadSequences">Download</button>
//				<div id="passToClustalw">
//					<button type="button" id="sendToClustal">Pass to ClustalW</button>
//					<input id="passWithQuery" form="hspControl" type="checkbox" ' . $passToClustalwHtml . '> include query </input>';
//	$out.='			</div>
//				<button type=button id="quickAlign">Quick align</button>
//				<div id="downloadSequencesStatus"></div>
//			</fieldset>
//		</div>

	// VB-7642 Add gff download
  	if ($dataType == 'Chromosomes' || $dataType == 'Scaffolds' || $dataType == 'Contigs')	{
  		$out.='		<div style="float:right; clear:both; padding:2px 0px;"><a href="/tool_helpers/rest/' .$id. '/gff_apollo?brid=' .$brid. '">GFF file for Apollo</a></div>';
  	}
  	$out.='		<div style="float:right; clear:both; padding:2px 0px;"><a id="hspGraphicTextSwitch" class="hspGraphicText hspnum';
	$out.=$num;
	$out.='">Show Query/Hit Numbers</a></div>
		<table id="blastHsps" class="tablesorter hspTableHits" style="width: 870px;">
			<thead>
				<tr>
					<th style="width:16px; padding-left:3px;">
						<input type="checkbox" id="hspsMaster" name="selectedHsps" value="all" />
					</th>
					<th class="tableOrgName" style="display:none;">Organism</th>
					<th class="hspHitName">Hit</th>';
					if( $dataType == 'Transcripts' || $dataType == 'Peptides' ) {
						$out.='<th>Gene Name</th>';
					}
	$out.='				<th class="hspHitDesc">Description</th>
					<th class="hspQueryName">Query</th>
					<th class="hspQueryStringLen">Aln Length</th>
					<th class="sorter-scinot" style="width:8%;">E-value</th>
					<th style="width:7%;">Score</th>
					<th style="width:7%;">Identity</th>
					<th id="queryStart" class="rightAln queryText" style="display:none;">Query Start</th>
					<th id="queryGraphic" class="queryText sorter-false" style="width:114px;">Query Hit</th>
					<th id="queryEnd" class="leftAln queryText" style="display:none;">Query End</th>
					<th id="hitStart" class="rightAln dbText" style="display:none;">Hit Start</th>
					<th id="hitGraphic" class="dbText sorter-false" style="width:114px;">DB Sequence Hit</th>
					<th id="hitEnd" class="leftAln dbText" style="display:none;">Hit End</th>
				</tr>
			</thead>
			<tbody>';
	$out=$out.$details;
	$out=preg_replace("#<th class=\"hspHitName\">Hit</th>#","<th class=\"hspHitName\" style=\"width:$hitNameWidth;\">Hit</th>",$out);
	$out.='
			</tbody>
		</table>';
	//	<div id="blastErrorDialog" title="BLAST Error Details"></div>';
	echo $out;

}



function hspHitLocationGraphic($start,$end,$length){
				//typical scenerion. forward facing arrow
				if($start<$end){
					$startPercent=round(($start-1)/$length,1)*100;	// take into account index starts a 1
					$stopPercent=round(($end)/$length,1)*100;
					$color="#95CF1A";
					$textColor="black";
					$text=">";

				// hit is in reverse direction
				}else{
					$startPercent=round(($end-1)/$length,1)*100;
					$stopPercent=round(($start)/$length,1)*100;
					$color="#4186B5";
					$textColor="white";
					$text="<";
					$reverse=true;
				}

				// require hit match box to be at least a certain size so the direction character shows up
				$widthPercent=$stopPercent-$startPercent;
				$minSize=7;
				if($widthPercent<$minSize){
					$widthPercent=$minSize;
				}

				// make sure the hit match box doesn't overflow past the end of it's box
				if($startPercent>100-$minSize){
					$startPercent=$startPercent-($minSize-(100-$startPercent));
				}


				$out="<td class=\"hspGraphic\" title=\"Start: $start   End: $end  Sequence Length: $length\">".'
					<div style="border: 1px solid #bbb; margin-right:2px;margin-left:2px; width:75px;">
<div class="hitGraph" style="left:'.$startPercent.'%; width:'.$widthPercent.'%; background-color:'.$color.'; color:'.$textColor.';">'.$text.'</div>
					</div></td>';
	return $out;
}
