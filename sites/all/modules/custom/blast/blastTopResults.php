<?php


// display a pretty table of top level info on blast job; should be fed raw blast job id
function blast_results_topLevel($id){
	$out='';

// 1) get total number of hits for each db (for all queries combined)
// 1.1) find dbs that were hit
$dbsHit=blast_getDbsForSearchJob($id);
// 1.2) get total number of hits for each db, a new implementation was done to get a count since a limit caused problems
//TODO: Need to update this to only use the id of the job and then use the $query object to get the necessary results
$hasHits=FALSE;
if($dbsHit){
	$numHits = array();
	$types = array();

	//Get the different hits from Blast, beforei to to be in the loop and would get the Count of a specific
	//database, but it would cause problems
	$hitResults = db_query("
			SELECT r.database_name
			FROM
				blast_results r,
				blast_hits h,
				blast_hsps s
			WHERE
				r.br_id = h.br_id AND
				h.bh_id = s.bh_id AND
				r.search_id = :id",
			array(':id'=>$id));

	foreach($dbsHit as $db){
		//Counter for each database to get the number of hits
		$numHits[$db] = 0;
        	$typeQuery = db_query("
			SELECT term.name 
			FROM 
				file_managed f, 
				taxonomy_term_data term, 
				node n, 
				field_data_field_download_file_type type, 
				field_data_field_file file
        		WHERE 
				f.filename LIKE :filename AND 
				f.fid = file.field_file_fid AND 
				file.entity_id = n.nid AND 
				n.nid = type.entity_id AND 
				type.field_download_file_type_tid = term.tid", 
			array(':filename'=>"$db%")
		)->fetchField();
		
		$types[$db] = $typeQuery;
	}
	$dataNumz = 0;
	//Check first if any results were returned and then calculate the number of hits per DB
	if ($hitResults->rowCount()) {
		$hasHits = TRUE;
		foreach ($hitResults as $hit) {
			//Increment the corresponding database that had a hit
			$numHits[$hit->database_name] += 1;
		}
	}

	// 2) display results
	// only show table if there are results
	if($hasHits){

	$out.="
        <div id='hspControl'></div>
	<div id='topLevelResults'>
		<table id=\"topLevelTable\" class=\"tablesorter\" data-sorter=\"false\" data-initial=\"sortme\">
			<thead>
				<tr>
					<th>Organism</th>
					<th>Database</th>
					<th title=\"High-scoring Segment Pairs\">HSPs</th>
				</tr>
			</thead>

			<tbody>\n";
		
		foreach($numHits as $key => $value){
			// only print row for dbs with hits
			if($value>0){
				// look up organism names
				// look up pretty db names
				$dbinfo=blast_dbinfo($key);		
		                $dataNumz = $dataNumz + 1;
				// dirty hack to remove stupid s.s from a. gambiae name
				$orgName=str_replace(" s.s.", "", $dbinfo['organism']);
                                $orgName1=preg_replace('/\s+/', '', $orgName);
                                $dataNum =
				$link="<a class=\"dbResult\" data-id=\"$id\" data-db=\"$key\" data-num=\"$dataNumz\" data-type=\"{$types[$key]}\"><b>({$types[$key]})</b> {$dbinfo['description']}</a>";
				$out.= "		<tr class=\"data-num$dataNumz\">
					<td>$orgName</td>
					<td>$link</td>
					<td>$value</td></tr>\n";

			}
		}

		$out.="		</tbody>
			</table>
		</div>
		<div id=\"hspLevelResults\"></div>
		<div id=\"hspDialog\" title=\"BLAST HSP Details\"></div>

		";

	}else
		$out="<div id=\"noResults\">No results found.</div>";
}else
	$out="<div id=\"noResults\">No hits found.</div>";

//return "";
return $out;
}

