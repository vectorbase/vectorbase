<?php

$id = '';

if (isset($_POST['id']) && strlen($_POST['id']) < 10){
        $id=filter_var($_POST['id'], FILTER_SANITIZE_STRING);
}
else if(isset($_GET['id']) && strlen($_GET['id']) < 10) {
	$id=$_GET['id'];
}
else  {
     $id = 0;//shows error for id of 0
}

// **** load drupal enviornment ****
define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
echo blast_getResults($id);

