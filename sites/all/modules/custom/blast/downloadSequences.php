<?php
// this file is posted some hsp ids and it will retrieve the fasta sequences for the corrosponding hits

require_once('utilities.php');

header("Content-type:appliation/octet-stream");
header("Content-Disposition: attachment; filename=\"vbBLASTsequences_".$_POST['jobid']."\"");

$dataDbs = explode(',', $_POST['data-db']);
//echo print_r($_POST);

foreach ($dataDbs as $dataDb) {
  // get hsp ids from the input variables
  $hspIds_key = str_replace('.', '_', str_replace('.fa', '', $dataDb));
  $hspIds= $_POST[$hspIds_key];
  $placeHolders = '(';
  $count = 0;
  //Even though hspIds now contains all the hsp values for the dataset, leaving
  //a variation of the following code for $placeHolders variable
  foreach($hspIds as $value) {
    $count++;
    $placeHolders.="\$$count, ";
  }
  $dataType_key = $hspIds_key . '-data-type';
  $dataType = $_POST[$dataType_key];
  $truncate_key = $hspIds_key . '-truncate';
  $truncate = $_POST[$truncate_key] === 'true' ? true : false;

  // Exit if there were no ids provided.
  if(empty($hspIds)) {
    echo 'No information could be found.';
    exit;
  }

  $placeHolders = substr($placeHolders, 0, -2);
  $placeHolders.=')';

  $conn = pg_connect("host=localhost dbname=vb_drupal user=db_public password=limecat");

  $namesQuery = "SELECT hits.name, hsps.starthit, hsps.endhit, hsps.strandhit, hsps.bh_id, hsps.bs_id FROM blast_hits hits, blast_hsps hsps WHERE hsps.bs_id in $placeHolders AND hsps.bh_id = hits.bh_id";
  $namesResult = pg_prepare($conn, 'namesQuery', $namesQuery);
  $namesResult = pg_execute($conn, 'namesQuery', $hspIds);

  $names = array();
  $hspsKey = 'hspsId';
  if($namesResult) {
    while($row = pg_fetch_assoc($namesResult)) {
      $info = array();
      $info['name'] = $row['name'];
      $info[$hspsKey] = $row['bs_id'];
      $info['start'] = $row['starthit'];
      $info['end'] = $row['endhit'];
      $info['direction'] = $row['strandhit'] == 1 ? '+' : '-';
      $names[$info[$hspsKey]]= $info;
    }
  }
  pg_close($conn);

  if(!$names) {
    echo "Could not find blast hit names.\n\tQuery: $namesQuery\n\tParameters: " . print_r($hspIds, true);
    exit;
  }
  uasort($names, function($a, $b) use ($hspsKey, $hspIds) {
      $aIdx = array_search($a[$hspsKey], $hspIds);
      $bIdx = array_search($b[$hspsKey], $hspIds);
      return intval($aIdx) - intval($bIdx);
  });
  $conn = pg_connect("host=localhost port=5432 dbname=blast_sequences user=db_public password=limecat");

  if($truncate) {
    $sequenceQuery = "SELECT primary_id, substring(sequence from $1::integer for $2::integer) as seq, description FROM raw_sequences WHERE filename = $3 AND primary_id = $4 LIMIT 1;";
  } else {
    $sequenceQuery = "SELECT primary_id, sequence as seq, description FROM raw_sequences WHERE filename = $1 AND primary_id = $2 LIMIT 1;";
  }
  if(pg_prepare($conn, 'sequenceQuery', $sequenceQuery) === false) {
    echo 'Could not prepare sequences query: ' . pg_last_error();
    exit;
  }

  foreach($names as $name => $hspBounds) {

    $anchor = $hspBounds['start'];
    $seqLen = $hspBounds['end'] - $hspBounds['start'];
    $pRams = array($dataDb, $hspBounds['name']);
    if($truncate) {
      array_unshift($pRams, $anchor, $seqLen);
    }

    $result = pg_execute($conn, 'sequenceQuery', $pRams);
    if($result === false) {
      echo "Could not execute prepared sequence query for '$name': " . pg_last_error();
      exit;
    }
    $row = pg_fetch_array($result, 0, PGSQL_ASSOC);
    if($row === false) {
      echo "Could not fetch row data for $dataDb/{$hspBounds['name']}: " . pg_last_error();
      exit;
    }
    if($truncate) {
      echo ">{$row['primary_id']} {$row['description']}|{$hspBounds['start']}-{$hspBounds['end']}\n{$row['seq']}\n";
    } else {
      echo ">{$row['primary_id']} {$row['description']}|1-".strlen($row['seq'])."\n{$row['seq']}\n";
    }
  }
  pg_close($conn);
}

