<?php

/**
 *  Implements hook_menu()
 *
 * 
 *  This function allows us to create the URL that will be used to to give
 *  autcomplete suggestions to our VectorBase Search page.
 *  @return array
 */

function blast_rest_menu() {
  $items = array();

  //Adding REST URLs to Submit Jobs
  $items['blast/rest/submit'] = array(
    'title' => 'Submit Blast',
    'page callback' => 'blast_rest_submit',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 *
 * Callback function used to submit a blast job.
 *
 */


function blast_rest_submit() {
  if (!empty($_POST)) {
    //Check if values are set, otherwise use defaults
    $args = array(
      'sequence' => $_POST['sequence'],
      'program' => $_POST['program'],
      'description' => $_POST['description'],
      'dbs' => array(),
      'maxEvalue' => (isset($_POST['evalue']) && blast_rest_valid_evalue($_POST['evalue']))? $_POST['evalue'] : 10,
      'numberOfResults' => (isset($_POST['num_alignments']) && blast_rest_valid_num_alignments($_POST['num_alignments'])) ? $_POST['num_alignments'] : 10,
      'complexityMasking' => (isset($_POST['complexityMasking']) && blast_rest_valid_complexity_masking($_POST['complexityMasking'])) ? $_POST['complexityMasking'] : 'Default',
    );

    if ($_POST['program'] == 'blastn') {
      $args['wordsize'] = (isset($_POST['word_size']) && blast_rest_valid_word_size($_POST['word_size'])) ? $_POST['word_size'] : 11;
    } else {
      $args['wordsize'] = (isset($_POST['word_size']) && blast_rest_valid_word_size($_POST['word_size'])) ? $_POST['word_size'] : 3;
      //Even though the default is BLOSUM45, using BLOSUM80 to actually get hits
      //for now
      $args['scoringMatrix'] = (isset($_POST['matrix']) && blast_rest_valid_scoring_matrix($_POST['matrix'])) ? $_POST['matrix'] : 'BLOSUM80';
    }

    $datasets = blast_generateDatasets();

    //Get subset of datasets depending of type of blast being done
    if ($args['program'] != 'blastp' && $args['program'] != 'blastx') {
      $datasets = $datasets['org_dbs'][$_POST['dbs']]['Nucleotide'];
    } else {
      $datasets = $datasets['org_dbs'][$_POST['dbs']]['Peptide'];
    }

    //Get only the requested datasets to blast against
    foreach ($datasets as $dataset) {
      if (isset($dataset['#attributes']['data-filename'])) {
        $args['dbs'][] = $dataset['#attributes']['data-filename'][0];
      }
    }
  }

  //Validating the passed arguments, sending $args as both parameters because of
  //the current state of function
  $status = blast_validate_job($args);

  //Construct the argument value pairs that will be stored in the database
  $args = blast_build_argument_value_pairs($args);

  if ($status == '') {
    $id = blast_submit_job($args);
    $status = tool_helpers_wait_for_completion($id);

    //Calculating these here so that the lookup in blast page can be quicker
    $runtime = tool_helpers_runtime($id);
    $dateSubmitted = tool_helpers_submit_timestamp($id);
  } else {
    $id = '0';
  }

  return drupal_json_output(array('id' => $id, 'status' => $status));
}

function blast_rest_valid_word_size($word_size) {
  $valid_word_size = array(3, 5, 7, 8, 11, 15, 30, 60);

  if (in_array($word_size, $valid_word_size)) {
    return true;
  } else {
    return false;
  }
}

function blast_rest_valid_evalue($evalue) {
  $valid_evalue = array("10", "1", "1E-3", "1E-5", "1E-10", "1E-20", "1E-40", "1E-80");

  if (in_array($evalue, $valid_evalue)) {
    return true;
  } else {
    return false;
  }
}

function blast_rest_valid_num_alignments($num_alignments) {
  $valid_num_alignments = array("1", "10", "50", "100", "250", "500");

  if (in_array($num_alignments, $valid_num_alignments)) {
    return true;
  } else {
    return false;
  }
}

function blast_rest_valid_scoring_matrix($matrix) {
  $valid_scoring_matrix = array("BLOSUM45", "BLOSUM62", "BLOSUM80", "PAM30", "PAM70");

  if (in_array($matrix, $valid_scoring_matrix)) {
    return true;
  } else {
    return false;
  }
}

function blast_rest_valid_complexity_masking($complexityMasking) {
  $valid_complexity_masking = array("Default", "no");

  if (in_array($complexityMasking, $valid_evalue)) {
    return true;
  } else {
    return false;
  }
}
