<?php

namespace VectorBase\Blast {

class Utilities {
    
	/**
	 * Get the reverse compliment of the given string. For non-RNA
	 * sequences only (ATGC). Case sensitive, so any mix of 
	 * upper/lower-case will stay that way.
	 */
	public static function revcomp($nucleotides) {
		$orig = array('A','a', 'T', 't', 'G', 'g', 'C','c'); 
		$temp = array('W','w','X','x','Y','y','Z','z');
		$nu = array('T', 't', 'A', 'a', 'C', 'c', 'G', 'g');
		$revComp = str_replace($orig, $temp, $nucleotides);
		$revComp = str_replace($temp, $nu, $revComp);
		return strrev($revComp);
	}


	/* Note that these are treated as if they are 0-based coordinates.
         * Sucessful return will give an array with two integer elements: 'start' and 'end'.
         * If an error occurred, a user-friendly message (string) will be returned;
	 */
	public static function bestEffortSegment($start, $end, $maxSize, $seqLen) {

		$initStart = $start;
		$initEnd = $end;

		if($start === $end) {
			return "Starting segment has no length.";
		}   
		if($start > $end) {
			return "Start of segment is greater than end.";
		}

		$segLen = $end-$start+1; // b/c we are 0-based, to get size based on coordinates, you need to add 1
		$segMaxSizeDelta = $maxSize - $segLen;
		if($segMaxSizeDelta > 0) {
			if($segMaxSizeDelta % 2) { // If it is odd, give the min the sightly larger chunk
				$halfMinDelta = round($segMaxSizeDelta/2, 0, PHP_ROUND_HALF_UP);
				$halfMaxDelta = $halfMinDelta - 1;
			} else {
				$halfMinDelta = $segMaxSizeDelta/2;
				$halfMaxDelta = $halfMinDelta;
			}   
			$startHalfDelta = $start - $halfMinDelta;
			if($startHalfDelta <= 0) {
				$start = 0;
			} else {
				$start-=$halfMinDelta;
			}   
			$endHalfDelta = $end + $halfMaxDelta;
			if($endHalfDelta >= $seqLen - 1) { // Comparing 0-based coordinates to size here, so need to convert size to the max coordinate, which is size-1
				$end = $seqLen - 1;    
			} else {
				if($startHalfDelta <= 0) {
					$end += abs($startHalfDelta);
				}   
				$end += $halfMaxDelta;
			}   
			//return "max size: $maxSize\nseq len: $seqLen\ninit start: $initStart\ninit end: $initEnd\nfinal start: $start\nfinal end: $end";
		} else if($segMaxSizeDelta < 0) {
			$segMaxSizeDelta = abs($segMaxSizeDelta);
			if($segMaxSizeDelta % 2) { // If it is odd, give the min the sightly larger chunk
				$halfMinDelta = round($segMaxSizeDelta/2, 0, PHP_ROUND_HALF_DOWN);
				$halfMaxDelta = $halfMinDelta + 1;
			} else {
				$halfMinDelta = $segMaxSizeDelta/2;
				$halfMaxDelta = $halfMinDelta;
			}   
			$start += $halfMinDelta;
			$end -= $halfMaxDelta;
			//return "max size: $maxSize\nseq len: $seqLen\ninit start: $initStart\ninit end: $initEnd\nfinal start: $start\nfinal end: $end");
		} 
                //else { // don't need to do anything, the coordinates are the full size of the sequence
			//return "max size: $maxSize\nseq len: $seqLen\ninit start: $initStart\ninit end: $initEnd\nfinal start: $start\nfinal end: $end");
		//}
		return array($start, $end);
	}


	/* An attempt to consolidate duplicate code that extracts 
	   sequences based on blast HSP (high scoring segment pair) 
	   results. The sequences are output in  fasta format.
         * 
         * If an error occurs, a single string with a user friendly message 
         * will be returned. Otherwise, an array of fasta-formatted strings will 
         * be returned (every entry has a trailing return character). All that is needed
         * to put this into a file is iterate and print every element.
	 */
	public static function getHspSequences(
			array $hspIds = array(), 
			string $dbFile = null, 
			string $dataType = null, 
			string $action = null) {

		// Exit if there were no ids provided.
		if(empty($hspIds)) {
			return 'No information could be found.';
		}
                
                $validActions[] = new HspAction('quickAlign', 10, 1000);
                $validActions[] = new HspAction('sendToClustalw', 50, 2000);
                $validActions[] = new HspAction('downloadSequences', 100, 3000);
                
		$hspAction = null;	
		foreach($validActions as $candidate) {
			if($candidate->getName() === $action) {
                            if(count($hspIds) > $candidate->getMaxHits()) {
                                return 'Max hits exceeded. Given: ' . count($hspIds) . ' / Allowed: ' . $candidate->getMaxHits();
                            }
			    $hspAction = $candidate;
			}
		}
                
		// get hsp ids from the input variables
		$ids=array();
		$placeHolders = '(';
		$count = 0;

		//must encode the sequence lengths in the hspid fields. Parse, if possible. hsp_id -> sequence length
		foreach($hspIds as $idStr) {
			$hsp_id = $idStr;
			$seqLen = 0;
			if(strstr($value, ':')) {
				$vals = explode(':', $idStr);
				$hsp_id = $vals[0];
				$seqLen = $vals[1];
			}
			$ids[$hsp_id]=$seqLen;
			$count++;
			$placeHolders.="\$$count, ";
		}
		$placeHolders = substr($placeHolders, 0, -2);
		$placeHolders.=')';

		$conn = pg_connect("host=localhost dbname=vb_drupal user=db_public password=limecat");

		$namesQuery = "SELECT hits.name, hsps.starthit, hsps.endhit, hsps.strandhit, hsps.bh_id, hsps.bs_id FROM blast_hits hits, blast_hsps hsps WHERE hsps.bs_id in $placeHolders AND hsps.bh_id = hits.bh_id";
		$namesResult = pg_prepare($conn, 'namesQuery', $namesQuery);
		$namesResult = pg_execute($conn, 'namesQuery', array_keys($ids));

		$names = array();
		$hspsKey = 'hspsId';
		if($namesResult) {
			while($row = pg_fetch_assoc($namesResult)) {
				$info = array();
				$info['name'] = $row['name'];
				$info[$hspsKey] = $row['bs_id'];
				$info['seqLen'] = $ids[$info[$hspsKey]];
                                
                                // important step: finds the limiting bounds of the sequence to extract from the db
				$bounds = bestEffortSegment(
					$row['starthit']-1, 
					$row['endhit']-1, 
					$action->getMaxSize(), 
					$info['seqLen']);
                                if(!is_array($bounds)) { // If bestEffortSegment returned not an array, it means a user readable error was retrned as a string.
                                    return $bounds;
                                }
				$info['start'] = $bounds['start']+1;
				$info['end'] = $bounds['end']+1;
				$info['direction'] = $row['strandhit'] == 1 ? '+' : '-';
				$names[$info[$hspsKey]]= $info;
			}
		}
                
		pg_close($conn);

		if(empty($names)) {
			return "Could not find blast hit names.\n\tQuery: $namesQuery\n\tParameters: " . print_r($hspIds, true);
		}

		uasort($names, function($a, $b) use ($hspsKey, $ids) {
				$aIdx = array_search($a[$hspsKey], $ids);
				$bIdx = array_search($b[$hspsKey], $ids);
				return strcmp($aIdx, $bIdx);
				});

		$conn = pg_connect("host=localhost port=5432 dbname=blast_sequences user=db_public password=limecat");
/*	$seqParams = array($name);
	//$prepQueryName = '';
	if(!is_null($bounds) && $bounds[0] < $bounds[1]) {
		$sql='SELECT substring(sequence from $1::integer for $2::integer) as seq FROM raw_sequences where primary_id=$3'; // $name
		array_unshift($seqParams, $bounds[1] - $bounds[0]);
		array_unshift($seqParams, $bounds[0]);
		//$prepQueryName = 'seqForClustalWClipped';
	} else {
		$sql='SELECT sequence as seq FROM raw_sequences where primary_id=$1';
		//$prepQueryName = 'seqForClustalW';
	}
*/
		$boundedQuery='SELECT substring(sequence from $1::integer for $2::integer) as seq, primary_id, description FROM raw_sequences where filename = $3 and primary_id = $4';
		$openQuery = "SELECT primary_id, sequence, description FROM raw_sequences WHERE filename = $1 AND primary_id = $2 LIMIT 1;";
		if(pg_prepare($conn, 'boundedHsp', $boundedQuery) === false) {
			return 'Could not prepare bounded sequence query: ' . pg_last_error();
		}
		if(pg_prepare($conn, 'openHsp', $openQuery) === false) {
			return 'Could not prepare open sequence query: ' . pg_last_error();
		}

		$prepQueryName = '';
                
                $sequences = array();
                
		foreach($names as $name => $hspBounds) {
			$seqParams = array($name);	
			if($hspBounds['seqLen'] > $hspBounds['end']-1) {
 				array_unshift($seqParams, $dbFile);	
				array_unshift($seqParams, $hspBounds['end'] - $hspBounds['start']);
				array_unshift($seqParams, $hspBounds['start']);
				$prepQueryName = 'boundedHsp';
			} else {
				$sql='SELECT sequence as seq FROM raw_sequences where primary_id=$1';
				$prepQueryName = 'openHsp';
			}
			$result = pg_execute($conn, $prepQueryName, $seqParams);
			if($result === false) {
				return "Could not execute prepared sequence query for '$name': " . pg_last_error();
			}
			$row = pg_fetch_array($result, 0, PGSQL_ASSOC);
			if($row === false) {
				return "Could not fetch row data for $dataDb/{$hspBounds['name']}: " . pg_last_error();
			}
			//$len = $hspBounds['end'] - $hspBounds['start'] + 1;
			//$start = $hspBounds['start'] - 1;

			//if(strcasecmp($dataType, 'Chromosomes') === 0) {
		//		$sequence = substr($row['sequence'], $start, $len);
		//	} else {
		//		$sequence = $row['sequence'];
		//	}
			if($hspBounds['direction'] == '+') {
				$sequences[] = ">{$row['primary_id']} {$row['description']}|{$hspBounds['start']}-{$hspBounds['end']}|+\n$sequence\n";
			} else {
				$sequences[] = ">{$row['primary_id']} {$row['description']}|{$hspBounds['end']}-{$hspBounds['start']}|-\n" . revcomp($sequence) . "\n";
			}
		}
		pg_close($conn);
                return $sequences;
	}
        
        /**
         * Determines if you have a DNA or Peptide sequence based on the following
         * rules.
         * [GATCN] > X, it is DNA
         * [GATCN] < X, it is Peptide
         * [GATCN] = X, Undetermined
         * Invalid input, return false
         * X = 0.8 (just random confidence value).
         *
         * if there are previous peptides, it assumes the next one will be as well
         * @param string $sequence
         * @return 'dna' if DNA is detected, 'peptide' if peptide is detected, false if error or unknown sequence type.
         */
        public static function detectSequenceType($sequence = '', &$prevCountPep, $debug = false) {
            if(!$sequence || gettype($sequence) !== 'string') {
                return false;
            }
            $sequence = preg_replace('/[-*]/', '', $sequence);
            
            if($debug) drupal_set_message(t( "Cleaned sequence: $sequence\n"));
            $len = strlen($sequence);
            
            $pDnaInPeptide = 0.16;
            $thresholdOffset = 0.8;
            
            $threshold = $thresholdOffset + pow($pDnaInPeptide, ($len/16));
            if($threshold > 1.0) {
                $threshold = 1.0;
            }
            if($debug) drupal_set_message(t( 'Need ' . ($threshold*100) . "% fidelity with $len significant chars\n"));
            // Try for DNA
            preg_match_all('/[ATGCN]/i', $sequence, $matches);
            $typeCount = count($matches[0]);            
            $ratio = (float)$typeCount / (float)$len;
            
            if($debug) drupal_set_message(t( "DNA ratio: $ratio\n"));

            // Try for Peptide
            if($ratio < $threshold) {
                if($debug) drupal_set_message(t( "Not DNA, trying Peptide\n"));
                preg_match_all('/[ABCDEFGHIKLMNPQRSTVWXYZ]/i', $sequence, $matches);
                $typeCount = count($matches[0]);
                $ratio = $typeCount / $len;
                if($debug) drupal_set_message(t( "Peptide ratio: $ratio\n"));
                if($ratio >= $threshold) {
                    if($debug) drupal_set_message(t( ">>> Peptide string detected.\n"));
                    $prevCountPep++;
                    return 'peptide';
                } else {
                    if($debug) drupal_set_message(t( ">>> Unknown sequence type.\n"));
                    return false;
                }
                
            } else if ($ratio =1 and $prevCountPep > 0 ){  //if there is another peptide, this is also to be tried as a peptide
                return 'peptide';
            } else {
                if($debug) drupal_set_message(t( ">>> DNA string detected.\n"));
                return 'dna';
            }
            if($debug) drupal_set_message(t( "\nDone.\n"));
        }
}
//drupal_set_message(t( Utilities::detectSequenceType($argv[1]);
class HspAction {

    private $name = '';
    private $maxHits = 0;
    private $maxSize = 0;

    public function __construct($n, $mH, $mS) {
            setDataType($n);
            setMaxHits($mH);
            setMaxSize($mS);
    }

    private function setName(string $n) {
            if(isset($n) && !is_null($n)) {
                    $name = $n;
            }
    }
    public function getName() {
            return $name;
    }
    private function setMaxHits(integer $mH) {
            if(isset($mH) && !is_null($mH)) {
                    $maxHits = $mH;
            }
    }
    public function getMaxHits() {
            return $maxHits;
    }
    private function setMaxSize(integer $mS) {
            if(isset($mS) && !is_null($mS)) {
                    $maxHits = $mS;
            }
    }
    public function getMaxSize() {
            return $maxSize;
    }
}
}
