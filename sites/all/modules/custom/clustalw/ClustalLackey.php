<?php

use VectorBase\ToolHelpers\JobSpec;
require_once('/vectorbase/web/root/sites/all/modules/custom/tool_helpers/Lackey.php');

class ClustalLackey extends VectorBase\ToolHelpers\Lackey {
    
    protected function buildSubmitFiles(JobSpec $job_specification, $prefix){
	return $this->makeflow_job($job_specification, $prefix);
    }

    protected function makeflow_prologue(JobSpec $job_specification, $prefix, $path_to_result_files) {
	    $binary      = 'clustalw2';

	    $prologue = <<<EOF
QUERY_ID=$prefix
QUERY=\$(QUERY_ID).query.fa

BINARIES=/opt/local/clustalw-2.1
BINARY=\$(BINARIES)/$binary

RESULT_DIR=$path_to_result_files

CORES=1
MEMORY=512
DISK=512

EOF;
	    return $prologue;
    }
        
    public function getRunTime() {
	    return 0;
    }

    protected function makeflow_analysis_rules(JobSpec $job_specification, $prefix, $path_to_result_files) {
        $job_attribs = $job_specification->get_attributes();

	dpm($job_specification->get_attributes());

	$extra_args = '';
        foreach ($job_specification->get_attributes() as $attrib => $value) {
            $extra_args .= $attrib . "=" . "$value ";
        }

	    $rules =<<<EOF

CATEGORY=clustalw

OUTPUT=\$(QUERY_ID).results.\$(CATEGORY).out
ERROR=\$(QUERY_ID).results.\$(CATEGORY).err

ALIGNMENT_FILE=\$(QUERY_ID).query.aln
TREE_FILE=\$(QUERY_ID).query.dnd

# This rule runs clustalw2 in the compute node.
\$(OUTPUT) \$(ERROR) \$(ALIGNMENT_FILE) \$(TREE_FILE): \$(QUERY)
\t\$(BINARY) -infile=\$(QUERY) $extra_args > \$(OUTPUT) 2> \$(ERROR)

# Transfer of the following files is done locally.
# Copy stdout to result dir
\$(RESULT_DIR)/\$(OUTPUT): \$(OUTPUT)
\tLOCAL cp \$(QUERY_ID).results.\$(CATEGORY).out \$(RESULT_DIR)

# Copy stderr to result dir
\$(RESULT_DIR)/\$(ERROR): \$(ERROR)
\tLOCAL cp \$(QUERY_ID).results.\$(CATEGORY).err \$(RESULT_DIR)

# transfer query to result dir, for log purposes.
\$(RESULT_DIR)/\$(QUERY): \$(QUERY)
\tLOCAL /bin/cp \$(QUERY) \$(RESULT_DIR)/

# write results as clustalw.module expects them
\$(QUERY).ribbon: \$(OUTPUT) \$(ALIGNMENT_FILE) \$(TREE_FILE)
\tLOCAL (cat \$(OUTPUT)                    && \
         echo "====END OF STDOUT===="      && \
         echo "====START OF DND===="       && \
         cat \$(TREE_FILE)                 && \
         echo "====END OF DND===="         && \
         echo "====START OF ALIGNMENT====" && \
         cat \$(ALIGNMENT_FILE))           > \$(QUERY).ribbon

\$(RESULT_DIR)/\$(QUERY_ID): \$(QUERY).ribbon
\tLOCAL /bin/cp \$(QUERY).ribbon \$(RESULT_DIR)/\$(QUERY_ID)

EOF;

	    return $rules;
    }

    protected function makeflow_parse_rules(JobSpec $job_specification, $prefix, $path_to_result_files) {

	    $rule_copy_query =<<<EOF

CATEGORY=clustalw

# transfer query to result dir, for log purposes.
\$(RESULT_DIR)/\$(QUERY): \$(QUERY)
\tLOCAL /bin/cp \$(QUERY) \$(RESULT_DIR)/

\$(QUERY).ribbon: \$(OUTPUT) \$(ALIGNMENT_FILE) \$(TREE_FILE)
\tLOCAL cat \$(OUTPUT)       && \
        echo "====END OF STDOUT====" && \
        echo "====START OF DND===="  && \
        cat \$(TREE_FILE)    && \
        echo "====END OF DND===="    && \
        echo "====START OF ALIGNMENT====" && \
        cat \$(ALIGNMENT_FILE) | tee \$(QUERY).ribbon
EOF;

	    return $rules;
    }
}

