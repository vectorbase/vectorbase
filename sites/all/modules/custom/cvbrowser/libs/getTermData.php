<?php

define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
require_once(DRUPAL_ROOT . '/includes/bootstrap.inc');
$phase = drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

require_once( "dbFunctions.php" );

$db = connectToDb();

$xrefId = $db->real_escape_string( $_GET[ 'xid' ] );

$q = "SELECT * FROM cv_term WHERE xref_id='$xrefId'";
$qr = $db->query( $q );
$row = $qr->fetch_assoc();

if( $row === FALSE )
{
	print "No data found for this ID.";
	return;
}

$cvId = $row['cv_id'];

$termId = $row['id'];
$termName = $row['name'];
$definition = $row['definition'];

$xrefSourceStartPos = strrpos( $definition, "[" );

if( $xrefSourceStartPos !== FALSE )
{
	$xrefSourceLastPos = strrpos( $definition, "]" ) - 1;
	
	$xrefSource = substr( $definition, $xrefSourceStartPos + 1, $xrefSourceLastPos - $xrefSourceStartPos );
	
	$definition = substr( $definition, 1, $xrefSourceStartPos - 3 );
}

$comment = str_replace( "\\n", "\n", $row['comment'] );

print "<table style='width: 100%;'>";
print "<tr><td colspan='2' align='center' style='border-bottom: solid 3px black;'><b>Term information</b></td></tr>\n";
print "<tr><td style='border-bottom: solid 1px black;'><b>ID</b></td><td style='border-bottom: solid 1px black;'>".$termId;
if( $row['obsolete'] ==	"1" )
{
	print " <b>[OBSOLETE]</b>";
}
print "</td></tr>\n";
print "<tr><td valign='top' style='border-bottom: solid 1px black;'><b>Name</b></td><td style='border-bottom: solid 1px black;'>{$row['name']}</td></tr>\n";
print "<tr><td valign='top' style='border-bottom: solid 1px black;'><b>Definition</b></td><td style='border-bottom: solid 1px black;'>$definition</td></tr>\n";

if(!$xrefSource)
{
	$xrefSource = 'N/A';	
}

print "<tr><td valign='top' style='border-bottom: solid 1px black;'><b>Source</b></td><td style='border-bottom: solid 1px black;'>$xrefSource</td></tr>";
print "<tr><td valign='top' style='border-bottom: solid 1px black;'><b>Comment</b></td><td style='border-bottom: solid 1px black;'>$comment</td></tr>";

$q = "SELECT synonym FROM cv_term_synonym WHERE term_xref_id='$xrefId'";
$qr = $db->query( $q );

if( $qr->num_rows > 0 )
{
	print "<tr><td valign='top' style='border-bottom: solid 1px black;'><b>Synonym</b></td><td style='border-bottom: solid 1px black;'>";
	
	$n = 0;
	$row = $qr->fetch_assoc();
	
	while( $row != FALSE )
	{
		if( $n > 0 )
		{
			print ", ";
		}
		print $row['synonym'];
		$row = $qr->fetch_assoc();
		$n++;
	}
	
	print "</td></tr>\n";
}

$q = "SELECT relationship, name FROM cv_term_relationship, cv_term WHERE descendant_xref_id='$xrefId' AND ancestor_xref_id=cv_term.xref_id ORDER BY priority ASC";
$qr = $db->query( $q );

if( $qr->num_rows > 0 )
{
        print "<tr><td valign='top' style='border-bottom: solid 1px black;'><b>Relationship(s)</b></td><td style='border-bottom: solid 1px black;'>";

        $n = 0;
        $row = $qr->fetch_assoc();

        while( $row != FALSE )
        {
                if( $n > 0 )
                {
                        print ", ";
                }
                print "<i>".$row['relationship']."</i>  ".$row['name'];
                $row = $qr->fetch_assoc();
                $n++;
        }

        print "</td></tr>\n";
}

print "</table>\n";

$namespace = substr( $termId, 0, strpos( $termId, ":") );

print "<br>\n<table align=\"center\" style='width: 100%;'>\n";

$n = 0;
$offset = 0;
$figure = array();

if( $cvId == 2 )
{
	$figureQualifier = "Fig.";
}
else
{
	$figureQualifier = "Fig.";
}

$commentLines = explode( "\n", $comment );

for( $n = 0; $n < count( $commentLines ); $n++ )
{
	$offset = 0;
	$figureDelimeter = $figureQualifier;
	$figuresInThisLine = 0;

	while( strpos( $commentLines[ $n ], $figureDelimeter, $offset ) !== FALSE ) 
	{	
		if( $cvId == 2 )
		{
			if( $figuresInThisLine == 0 )
			{
				$figureDelimeter = $figureQualifier;
			}
			else
			{
				$figureDelimeter = ",";	
			}
		}
		else
		{
			$figureDelimeter = $figureQualifier;
		}

		$figureNameStartPos = strpos( $commentLines[ $n ], $figureDelimeter, $offset );

		if( $cvId == 2 && $figuresInThisLine >= 0 )
		{
			$figureDelimeter = ",";	
		}
		
		if( $cvId == 3 )
		{
			$figureDelimeter = ",";
		}
		
		$figureNameEndPos = strpos( $commentLines[ $n ], $figureDelimeter, $figureNameStartPos + 1 );
		
		if( $cvId == 2 && $figureNameEndPos == FALSE && $figuresInThisLine == 0 )
		{
			$figureNameEndPos = strpos( $commentLines[ $n ], " ", $figureNameStartPos + 5 );
		}
		
		if( $cvId == 2 && $figureNameEndPos == FALSE && $figuresInThisLine >= 0 )
		{
			$figureNameEndPos = strpos( $commentLines[ $n ], " ", $figureNameStartPos + 1 );
		}
		
		$figureName = substr( $commentLines[ $n ], $figureNameStartPos, $figureNameEndPos - $figureNameStartPos );

		$figureName = str_replace( ",", "", $figureName );	
		$figureName = str_replace( " ", " ", $figureName );
		$figureName = trim( $figureName );
		
		if( $cvId == 2 && count( $figure ) == 0 )
		{
			$figureName = str_replace( "Fig. 0", "Fig. ", $figureName );	
			$figureName = str_replace( "Fig ", "Fig. ", $figureName );	
		}
		
		if( $cvId == 2 && count( $figure ) >= 0 )
		{
			if( substr( $figureName, 0 , 1 ) == "0" )
			{
				$figureName = "Fig. ".trim( substr( $figureName, 1 ) );
			}
			
			if( substr( $figureName, 0 , 1 ) != "F" )
			{
				$figureName = "Fig. ".trim( $figureName );
			}
		}
		
		if( $cvId == 2 )
		{
			if( $n == 0 )
			{
				$figureName = "$namespace/Anopheles|$figureName";
			}

			if( $n > 0 )
			{
				$figureName = "$namespace/Aedes|$figureName";
			}
		}
		else
		{
			$figureName = "$namespace|$figureName";
		}
			
		$figure[] = trim( $figureName );
		
		if( $cvId == 3 )
		{
			$offset = $figureNameEndPos + 1;
		}
		else
		{
			$offset = $figureNameEndPos ;
		}

		$figuresInThisLine++;
	}
}

$figure = array_unique( $figure );
$figureCount = count( $figure );

if( $figureCount > 0 ) 
{
	$imagePath = drupal_get_path('module', 'cvbrowser') . '/libs/images/';
	print "<tr><td colspan=\"3\" style='border-bottom: solid 3px black; width: 100%; text-align: center'><b>Images</b></td></tr>";
	for( $t = 0; $t < $figureCount; )
	{
		print "<tr>";
		
		$tok = explode( "|", str_replace( "Fig. ", "Fig", $figure[ $t ] ).".jpg" );
		$namespace = $tok[0];
		$fileName = $tok[1];
		
		print "<td align=\"center\"><a href=\"$imagePath/$namespace/$fileName\" target=\"_blank\"><img src=\"$imagePath/$namespace/tn/$fileName\" width=\"100\"></a></td>";
	
		if( $figureCount > $t + 1 )
		{
		$tok = explode( "|", str_replace( "Fig. ", "Fig", $figure[ $t + 1] ).".jpg" );
		$namespace = $tok[0];
		$fileName = $tok[1];
					
			print "<td align=\"center\"><a href=\"$imagePath/$namespace/$fileName\" target=\"_blank\"><img src=\"$imagePath/$namespace/tn/$fileName\" width=\"100\"></a></td>";
		}
		else 
		{
			print "<td align=\"center\">&nbsp;</td>";
		}
		
		if( $figureCount > $t + 2 )
		{
		$tok = explode( "|", str_replace( "Fig. ", "Fig", $figure[ $t + 2] ).".jpg" );
		$namespace = $tok[0];
		$fileName = $tok[1];
					
			print "<td align=\"center\"><a href=\"$imagePath/$namespace/$fileName\" target=\"_blank\"><img src=\"$imagePath/$namespace/tn/$fileName\" width=\"100\"></a></td>";
		}
		else 
		{
			print "<td align=\"center\">&nbsp;</td>";
    }

		print "</tr>\n";
		$t = $t + 3;
	}
}

print "</table>\n";

print "<br><br>\n";

$hostname=gethostname();

#Quick fix for bella.vectorbase.org getting resolved as bender not getting resolved properly
if ($hostname == 'bella.vectorbase.org') {
  $hostname = 'www.vectorbase.org';
} else if ($hostname == 'adama.vectorbase.org') {
  $hostname = 'pre.vectorbase.org';
} else if ($hostname == 'bender.vectorbase.org') {
  $hostname = 'dev.vectorbase.org';
}

$url="https://$hostname/vbsearch_facets?terms=$termId";
//$url="https://www.vectorbase.org/vbsearch_facets?terms=$termId";

$conn = fopen( $url, "r" );
$response = "";

if( $url )
{
	while( $line = @fgets( $conn, 1024 ) )
	{
		$response .= $line;
	}
}

fclose( $conn );


$a = json_decode( $response, true );

if( count( $a['sites'] > 0 ) )
{
	print "<table style='width: 100%'>\n";
	print "<tr><td align='center' style='border-bottom: solid 3px black;'><b>Domain</b></td><td align='center' style='border-bottom: solid 3px black;'><b>Hits</b></td></tr>\n";

	foreach( $a['sites'] as $site=>$value )
	{
		print "<tr><td valign='top' style='border-bottom: solid 1px black;'><a href=\"".$a['sites'][ $site ]['url']."\">".$site."</a></td>\n";
		print "<td valign='top' style='border-bottom: solid 1px black; text-align: right;'>".$a['sites'][ $site ]['count']."</td></tr>\n";
	}

	print "</table>\n";
}

print "<br><br>";

if( count( $a['species'] ) > 0 )
{
	print "<table style='width: 100%'>";
	print "<tr><td align='center' style='border-bottom: solid 3px black;'>";
	print "<b>Species</b></td><td align='center' style='border-bottom: solid 3px black;'><b>Hits</b></td></tr>";

	foreach( $a['species'] as $species=>$value )
	{
		print "<tr><td valign='top' style='border-bottom: solid 1px black;'><i><a href=\"".$a['species'][ $species ]['url']."\">".$species."</a></i></td>";
		print "<td valign='top' style='border-bottom: solid 1px black; text-align: right;'>".$a[ 'species' ][ $species ][ 'count' ]."</td></tr>";
	}

	print "</table>";
}
