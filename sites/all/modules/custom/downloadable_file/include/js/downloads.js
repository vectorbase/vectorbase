/*
 * Javascript for the downloads page.
 */

(function($) {
  $(function() {
    // Remove hidden class from selected suboptions in organism widget
    $('.suboption.hidden[selected]').each(function() {
      if ($(this).hasClass('hidden')) {
        $(this).removeClass('hidden');
        $(this).prevUntil('option:not(.suboption)').removeClass('hidden');
        $(this).nextUntil('option:not(.suboption)').removeClass('hidden');
      }
    });

    // Show suboptions on organism double-click
    $('#edit-field-organism-taxonomy-tid > option:not(.suboption)').dblclick(function() {
      $(this).nextUntil('option:not(.suboption)').toggleClass('hidden');
    });
  })
})(jQuery);

