<?php

use VectorBase\ToolHelpers\JobSpec;
require_once('/vectorbase/web/root/sites/all/modules/custom/tool_helpers/Lackey.php');
//require_once(drupal_get_path('module', 'tool_helpers'). '/Lackey.php');

class HMMerLackey extends VectorBase\ToolHelpers\Lackey {
	protected function buildSubmitFiles(JobSpec $job_specification, $prefix){
		return $this->makeflow_job($job_specification, $prefix);
	}

	protected function makeflow_prologue(JobSpec $job_specification, $prefix, $path_to_result_files) {
		$job_attribs = $job_specification->get_attributes();
		$server_ip   = $job_attribs['server_ip'];

		$prologue = <<<EOF

QUERY_ID=$prefix
QUERY=\$(QUERY_ID).query.fa

HMMER_DEPOT=/vectorbase/dbs
BINARIES=/opt/local/hmmer3/bin
BUILD_BINARY=\$(BINARIES)/hmmbuild
SEARCH_BINARY=\$(BINARIES)/hmmsearch
SEARCHP_BINARY=\$(BINARIES)/phmmer

RESULT_DIR=$path_to_result_files

CORES=1
MEMORY=512
DISK=512

EOF;
		return $prologue;
	}


    protected function makeflow_analysis_rules(JobSpec $job_specification, $prefix, $path_to_result_files) {
	    $job_attribs = $job_specification->get_attributes();
	    $target_dbs  = $job_specification->get_target_dbs();
	    $binary      = $job_specification->get_program();

	    $extra_args = '';
	    foreach ($job_attribs as $attrib => $value) {
		    if($attrib != 'server_ip') {
			    $extra_args .= "$attrib $value ";
		    }
	    }

	    $rules = '';

	    $i = 0;
	    foreach ($target_dbs as $db) {
		    $rules .= $this->makeflow_one_analysis_rule($db, $prefix, $binary, $extra_args, '_' . $i, $path_to_result_files);
		    $i++;

	    }

	    return $rules;
    }

    protected function makeflow_one_analysis_rule($db, $prefix, $binary, $extra_args, $suffix, $path_to_result_files) {
	$rules = $this->makeflow_one_analysis_rule_preamble($db, $suffix);

	if(strcmp($binary, 'hmmsearch') == 0) { 
		$rules .= $this->makeflow_one_analysis_rule_search_dna($db, $prefix, $extra_args, $suffix, $path_to_result_files);
	} else {
		$rules .= $this->makeflow_one_analysis_rule_search_protein($db, $prefix, $extra_args, $suffix, $path_to_result_files);
	}

	return $rules;
    }

    protected function hmmer_db_name($db, $prefix, $path_to_result_files) {
	    if( preg_match('/^custom_user_db_/', $db)) {
		    $db_filename = substr($db,15); // cut out the prepended 'custom_user_db_' string
		    $dbFullPath = drupal_realpath("public://userHMMer/$db_filename"); // uri to path conversion

		    // effectively a 'move file to job_results folder and add the job id before the filename'
		    $db_contents = file_get_contents($dbFullPath);
		    file_put_contents("$path_to_result_files/$prefix.$db_filename", $db_contents);

		    // after moving the file to the job_results folder need to delete it from the drupal db/filesystem
		    $files = file_load_multiple(array(), array('uri' => "public://userHMMer/$db_filename"));
		    $file = reset($files);
		    file_delete($file);

		    // assign the actual db_file path to add to the condor submit script
		    $db_file = "$path_to_result_files/$prefix.$db_filename";
	    }
	    else {
		    $db_file =  "/vectorbase/dbs/$db";
	    }
	
	    return $db_file;
    }

    protected function makeflow_one_analysis_rule_preamble($db, $suffix) {
	    $rules =<<<EOF

# Set filenames in default category, so that they are globally available.
CATEGORY=default

NAME=$db
OUTPUT$suffix=\$(QUERY_ID).results.\$(NAME).out
ERROR$suffix=\$(QUERY_ID).results.\$(NAME).err

# Transfer of the following files is done locally.
# Copy stdout to result dir
\$(RESULT_DIR)/\$(OUTPUT$suffix): \$(OUTPUT$suffix)
\tLOCAL cp \$(QUERY_ID).results.\$(NAME).out \$(RESULT_DIR)

# Copy stderr to result dir
\$(RESULT_DIR)/\$(ERROR$suffix): \$(ERROR$suffix)
\tLOCAL cp \$(QUERY_ID).results.\$(NAME).err \$(RESULT_DIR)

EOF;
	    return $rules;
    }

    protected function makeflow_one_analysis_rule_search_dna($db, $prefix, $extra_args, $suffix, $path_to_result_files) {

	$db_file = $this->hmmer_db_name($db, $prefix, $path_to_result_files);

	$rules =<<<EOF

# Switch to corresponding category, so that rules can be appropietely labeled. 
# Variables not found in a category, fallback to the default category.
CATEGORY=\$(NAME)

\$(OUTPUT$suffix) \$(ERROR$suffix): \$(QUERY)
\t((\$(BUILD_BINARY) \$(QUERY_ID).hmm \$(QUERY) 2>&1 >/dev/null) && (\$(SEARCH_BINARY) $extra_args \$(QUERY_ID).hmm $db_file 2>&1)) > \$(OUTPUT$suffix) 2> \$(ERROR$suffix)

EOF;

	return $rules;
    }

    protected function makeflow_one_analysis_rule_search_protein($db, $prefix, $extra_args, $suffix, $path_to_result_files) {
	$db_file = $this->hmmer_db_name($db, $prefix, $path_to_result_files);

	$rules =<<<EOF

# Switch to corresponding category, so that rules can be appropietely labeled. 
# Variables not found in a category, fallback to the default category.
CATEGORY=\$(NAME)

\$(OUTPUT$suffix) \$(ERROR$suffix): \$(QUERY)
\t(\$(SEARCHP_BINARY) $extra_args \$(QUERY) $db_file 2>&1) > \$(OUTPUT$suffix) 2> \$(ERROR$suffix)

EOF;
	return $rules;
    }

    protected function makeflow_parse_rules(JobSpec $job_specification, $prefix, $path_to_result_files) {
	    $total          = count($job_specification->get_target_dbs());
	    $job_attribs    = $job_specification->get_attributes();

	    $rule_copy_query =<<<EOF


CATEGORY=default

# transfer query to result dir, for log purposes.
\$(RESULT_DIR)/\$(QUERY): \$(QUERY)
\tLOCAL /bin/cp \$(QUERY) \$(RESULT_DIR)/

EOF;

# put all names of stdouts and stderr files into variables.
	    $all_outputs = '';
	    $all_errors  = '';
	    for($i = 0; $i < $total; $i++) {
		    $all_outputs .= "\$(OUTPUT_$i) ";
		    $all_errors  .= "\$(ERROR_$i) ";
	    }

	    $rule_concat_all =<<<EOF

# consolidate all stdouts and stderrs into a single file
\$(QUERY_ID): $all_outputs $all_errors
\tLOCAL cat $all_outputs $all_errors > $(QUERY_ID)

# copy results to results directory
\$(RESULT_DIR)/\$(QUERY_ID): \$(QUERY_ID)
\tLOCAL cp \$(QUERY_ID) \$(RESULT_DIR)

EOF;

	    $rules = '';
	    $rules .= $rule_copy_query;
	    $rules .= $rule_concat_all;
	    return $rules;
    }

    public function getRunTime() {
	    return 0;
    }
}

