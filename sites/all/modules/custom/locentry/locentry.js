//DKDK would need to receive field_name from adddomlistener for all map functions and use it to recognize correct textfield, i.e., getElementByID value

var map;            //DKDK for circle
var marker;
var myCircle;

var map_rect;       //DKDK for rectangle
var myRectangle;    //DKDK for drawing rectangle
var marker1;        //DKDK rectangular marker - SW
var marker2;        //DKDK rectangular marker - NE

// var fieldname
// var fieldname_rect_js

function radiustoZoom(radius){
      	return Math.round(14-Math.log(radius)/Math.LN2)-1;
}

// DKDK Let's just leave the initialization as is so that it can be a default behavior
function mapinitialize(fieldname) {
    //DKDK need to make getElementByID value based on fieldname
    // console.log("at mapinitialize");
    // console.log(fieldname);
    var coords_text = 'edit-' + fieldname.split("_").join("-") + '-und-0-coords';
    var radius_text = 'edit-' + fieldname.split("_").join("-") + '-und-0-radius';
    // console.log(coords_text);

	var myzoom = radiustoZoom(20);
	var myLatlng = new google.maps.LatLng(41.6833333,-86.25);
	// var coords = document.getElementById('edit-field-search-geo-location-und-0-coords');
	// var radius = document.getElementById('edit-field-search-geo-location-und-0-radius');
    var coords = document.getElementById(coords_text);
    var radius = document.getElementById(radius_text);

	var mapOptions = {
    	center: myLatlng,
    	zoom: myzoom, //Max 21
    	scaleControl: true
    	};
    	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    	marker = new google.maps.Marker({
    	position: myLatlng,
    	map: map,
    	title: 'Selected center'
    	});
            myCircle = new google.maps.Circle({
    		map:map,
                    center:myLatlng,
                    radius:radius.value * 1000,
                    strokeColor:"0000FF",
                    strokeOpacity:0.8,
                    strokeWeight:2
             });

    	if(coords.value.length > 0 && radius.value.length > 0) {
    		// alert('Resized');
    		// mapresize();
            mapresize(fieldname);
	}
}

function mapresize(fieldname) {
    // console.log("at mapresize");
    // console.log(fieldname);
    var coords_text = 'edit-' + fieldname.split("_").join("-") + '-und-0-coords';
    var radius_text = 'edit-' + fieldname.split("_").join("-") + '-und-0-radius';
	// var coords = document.getElementById('edit-field-search-geo-location-und-0-coords');
	// var radius = document.getElementById('edit-field-search-geo-location-und-0-radius');
    var coords = document.getElementById(coords_text);
    var radius = document.getElementById(radius_text);
	var myzoom;

        if(radius.value > 0){
          myzoom = radiustoZoom(radius.value);
        }else{
          // alert('Warning - invalid radius');
          myzoom = map.zoom;
        }
        var center = centervalue(coords.value);
        google.maps.event.trigger(map, 'resize');
        map.setZoom(myzoom);
        map.setCenter(center);
        marker.setPosition(center);
        myCircle.setCenter(center);
        myCircle.setRadius(radius.value * 1000);
}

function centervalue(coords){
    var mycenter;
    //alert(coords.search(','));
    if((coords.length > 0) && (coords.search(',') > 0)){
        var coordarray = coords.split(',',2);
        var lat = coordarray[0].trim();
        var lon = coordarray[1].trim();
        if(((lat >= -90) && (lat <=90)) && ((lon >= -180) && (lon <= 180))){
            mycenter = new google.maps.LatLng(lat,lon);
        }else{
            //DKDK VB-7911 block alert
            // alert('Warning - invalid coordinates');
            mycenter = map.center;
        }
    }else{
        mycenter = map.center;
    }
    return mycenter;
}

//DKDK implementing rectangular geospatial search
function mapinitialize_rect(fieldname_rect) {
    // console.log("at mapinitialize_rect");
    // console.log(fieldname_rect);
    var coordsbox_text = 'edit-' + fieldname_rect.split("_").join("-") + '-und-0-coordsbox';        // DKDK be carefule of fieldname_rect not fieldname
    // console.log(coordsbox_text);
    var myzoom_rect = radiustoZoom(20);
    var myLatlng_rect = new google.maps.LatLng(41.6833333,-86.25);
    // var coordsbox = document.getElementById('edit-field-search-rect-geo-location-und-0-coordsbox');    //DKDK
    var coordsbox = document.getElementById(coordsbox_text);    //DKDK
    // var coordsbox = document.getElementById(fieldname_rect);    //DKDK

    var mapOptions_rect = {
        center: myLatlng_rect,
        zoom: myzoom_rect, //Max 21
        // zoom: 10,
        scaleControl: true
    };
    map_rect = new google.maps.Map(document.getElementById('map-canvas-rect'), mapOptions_rect);

    //DKDK draw markers - be careful not to redefine marker1 & 2 & myRectangle (no for, e.g., var marker1, var marker2)
    marker1 = new google.maps.Marker({
        position: new google.maps.LatLng(41.54,-86.49),
        map: map_rect
    });

    marker2 = new google.maps.Marker({
        position: new google.maps.LatLng(41.80,-86.05),
        map: map_rect
    });

//DKDK draw initial rectangle - as close as the circular case! - be careful not to use 'var myRectangle' here as myRectangle is a global variable defined on top
    myRectangle = new google.maps.Rectangle({
        strokeColor: '#66cc00',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#66cc00',
        fillOpacity: 0.35,
        map: map_rect,
        bounds: {
          north: 41.80,
          south: 41.54,
          east: -86.05,
          west: -86.49
        }
    });     // myRectangle

    if( (coordsbox.value.length > 0) ) {        //DKDK this may not work - probably due to default coordsbox.value = null
        // alert('Resized');
        // mapresize_rect();
        mapresize_rect(fieldname_rect);
    }   // if
}   // function mapinitialize_rect()


//DKDK not done yet but looks like I need to set a default view at mapresize_rect() as it is always executed when a relavant option (i.e., calling rect_geo_location) is selected, e.g., popbio
//DKDK <- this is solved by calling mapinitialize_rect(); again
function mapresize_rect(fieldname_rect) {
    console.log("at mapresize_rect");
    console.log(fieldname_rect);
    // fieldname_rect_js = fieldname_rect;
    var coordsbox_text = 'edit-' + fieldname_rect.split("_").join("-") + '-und-0-coordsbox';        // DKDK be carefule of fieldname_rect not fieldname
    // console.log(coordsbox_text);
    // var coordsbox = document.getElementById('edit-field-search-rect-geo-location-und-0-coordsbox');    //DKDK
    var coordsbox = document.getElementById(coordsbox_text);    //DKDK
    // var coordsbox = document.getElementById(fieldname_rect_js);    //DKDK
    var myzoom;

    if (coordsbox.value.length <= 0) {              //DKDK
        // alert('Warning - invalid radius');
        // myzoom = map_rect.zoom;
        // mapinitialize_rect();         //DKDK this solves an issue of no map (or no focus)
        mapinitialize_rect(fieldname_rect);         //DKDK this solves an issue of no map (or no focus)
    } else {
    // if (typeof myRectangle != 'undefined') {
        // console.log('------myRectangle-------');
        // console.log(myRectangle);
        // console.log(marker1);
        // console.log('------myRectangle-------');

        //DKDK well, the input example i have used is as close as the default map so fitbounds do not show much
        var swnecoords = boxcoords(coordsbox.value);
        var swcoords = new google.maps.LatLng(parseFloat(swnecoords[0]), parseFloat(swnecoords[1]));
        var necoords = new google.maps.LatLng(parseFloat(swnecoords[2]), parseFloat(swnecoords[3]));

        console.log(swnecoords);
        // google.maps.event.trigger(map_rect, 'resize');        //DKDK no need to use this one
        // var swcoords = new google.maps.LatLng(41.54,-86.49);
        // var necoords = new google.maps.LatLng(41.80,-86.05);

        //DKDK set bounds for a better display
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(swcoords);
        bounds.extend(necoords);
        map_rect.fitBounds(bounds);

        //DKDK redraw markers and rectangular box whenever inputs are changed
        marker1.setPosition(swcoords);
        marker2.setPosition(necoords);
        var latLngBounds = new google.maps.LatLngBounds(
            marker1.getPosition(),
            marker2.getPosition()
        );
        myRectangle.setBounds(latLngBounds);

    }   // else

}   // function mapresize_rect()

//DKDK a function to check a number/not a number in a generic way
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

//DKDK a function to check the correctness of user input - something like a validation
function boxcoords(coords) {
    //alert(coords.search(','));
    if((coords.length > 0) && (coords.search(',') > 0)) {
        var coordarray = coords.split(',',4);
        //DKDK need to check here if coordarray has 4 values
        if (coordarray.length == 4) {
            var swlat = coordarray[0].trim();
            var swlon = coordarray[1].trim();
            var nelat = coordarray[2].trim();
            var nelon = coordarray[3].trim();

            if ( isNumeric(swlat) && isNumeric(swlon) && isNumeric(nelat) && isNumeric(nelon) ) {
                if(((swlat >= -90) && (swlat <=90)) && ((swlon >= -180) && (swlon <= 180)) && ((nelat >= -90) && (nelat <=90)) && ((nelon >= -180) && (nelon <= 180))) {
                    swlat_value=Math.min(swlat,nelat);
                    swlon_value=Math.min(swlon,nelon);
                    nelat_value=Math.max(swlat,nelat);
                    nelon_value=Math.max(swlon,nelon);
    
                    return [swlat_value,swlon_value,nelat_value,nelon_value];

                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    } else {
        return null;
    }
}
