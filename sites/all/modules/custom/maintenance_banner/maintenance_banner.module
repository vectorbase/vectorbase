<?php

/*
 * @file
 * A block module that displays the VectorBase Maintenance Banner on every page
 */


/**
 * Implements hook_page_build
 *
 * Puts a message banner on every page or specifically only the tools pages 
 *
 * @param type $page
 *      The page that we are going to append the div to.
 */
function maintenance_banner_page_build(&$page){
  $node = menu_get_object();
  // if the Sitewide Banner Enabled box is checked in the Maintenance Banner menu
  $display_sitewide_banner = variable_get('sitewide_maint_banner_enabled', FALSE);

  //Check if we are in a node page, otherwise this code is not relevant
  if(!empty($node)) {
    // if the current node is in the page-specific node list and the page
    // specific checkbox is ticked, then display the page-specific banner
    // (better way would be to convert to array from comma-separation and check
    // for array item, but that's heavier)

    $node_in_list = (strpos(variable_get('ps_node_ids', ''), $node->nid) !== FALSE);
    $display_ps_banner = variable_get('ps_banner_enabled', FALSE) && $node_in_list ;
  }

  // give page-specific banner precedence over the sitewide message
  if( isset($display_ps_banner) && $display_ps_banner ) {
    $maint_banner_message = variable_get('tools_banner_message', 'Default Message');
    $maint_status = variable_get('tools_banner_status', 'status');
  } elseif ( $display_sitewide_banner) {
    $maint_banner_message = variable_get('sitewide_maint_banner_message', 'Default Message');
    $maint_status = variable_get('sitewide_maint_banner_status', 'status');
  }

  // $maint_banner_message will be null if neither of the above cases are met
  // for setting the message and status
  if(isset($maint_banner_message) && $maint_banner_message) {
    // The markup structure and classes are important here, wrapping some divs
    // to make sure we can take advantage of the default styling of the messages
    // area.
    $page['header']['vb_messages'] = array(
      '#weight' => 25, // weight shouldn't matter here since we're in the header page area.
      '#markup' => '<div id="content-messages-inner" class="content-messages-inner gutter"><div class="messages ' . $maint_status . '" style="clear:both;">' . $maint_banner_message . '</div></div>'
    );
  }

}

/**
 * Implements hook_menu()
 */
function maintenance_banner_menu() {
    $items = array();
    $items['admin/config/development/maintenance_banner'] = array(
        'title' => 'Maintenance Banner',
        'description' => 'Configuration for Maintenance Banner module',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('maintenance_banner_form'),
        'access arguments' => array('access administration pages'),
        'type' => MENU_NORMAL_ITEM,
    );
    return $items;
}

function maintenance_banner_form($form, &$form_state) {
  $form['sitewide_maint_banner_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Sitewide Message Text'),
    '#default_value' => variable_get('sitewide_maint_banner_message', 'Vectorbase is undergoing maintenance.'),
    '#size' => 100,
    '#maxlength' => 150,
    '#description' => t('The message displayed to the user when the "sitewide message" is enabled.'),
    '#required' => FALSE,
  );

  $form['sitewide_maint_banner_status'] = array(
    '#type' => 'select',
    '#title' => t('Sitewide Message Notification Level'),
    '#options' => array(
         'status' => t('Status'),
         'warning' => t('Warning'),
         'error' => t('Error'),
    ),
    '#default_value' => variable_get('sitewide_maint_banner_status', 'status'),
    '#description' => t('The level of notification of the sitewide banner.'),
  );

  $form['sitewide_maint_banner_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sitewide Message Enabled'),
    '#default_value' => variable_get('sitewide_maint_banner_enabled', FALSE),
    '#description' => t('Whether or not the sitewide message should be enabled.'),
  );

  $form['ps_banner_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Page-Specific Message Text'),
    '#default_value' => variable_get('ps_banner_message',''),
    '#size' => 100,
    '#maxlength' => 150,
    '#description' => t('The message displayed to the user on the specified node pages when the "page-specific message" is enabled. If enabled, the page-specific message overrides the sitewide message text when viewing the specified nodes.'),
    '#required' => FALSE,
  );

  $form['ps_banner_status'] = array(
    '#type' => 'select',
    '#title' => t('Page-Specific Message Notification Level'),
    '#options' => array(
      'status'=> t('Status'),
      'warning' => t('Warning'),
      'error' => t('Error'),
    ),
    '#default_value' => variable_get('ps_banner_status', 'status'),
    '#description' => t('The level of notification of the tools-specific banner.'),
  );

  $form['ps_node_ids'] = array(
    '#type' => 'textfield',
    '#title' => t('List of Specific Node IDs'),
    '#default_value' => variable_get('ps_node_ids',''),
    '#size' => 100,
    '#maxlength' => 150,
    '#description' => t('Comma separated list of nodes to show this message on'),
    '#required' => FALSE,
  );

  $form['ps_banner_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Page-Specific Message Enabled'),
    '#default_value' => variable_get('ps_banner_enabled', FALSE),
    '#description' => t('Whether or not the page-specific message should be enabled.'),
  );

  return system_settings_form($form);
}
