<?php
namespace VectorBase\ToolHelpers;

abstract class Lackey {

  protected $myId;
  function __construct($id = '') {
	  if (!$id) {
      $this->myId = db_query("SELECT nextval('job_id_sequence')")->fetchObject()->nextval;
    } else {
      $this->myId = $id;
    }
  }

  public function getId() {
    return $this->myId;
  }

  public function submit(JobSpec $job_specification) {
    $mySubmitCommands = $this->buildSubmitFiles($job_specification, $this->myId);
    //$exec_out = array('submitted to cluster ' . db_query("SELECT nextval('fake_condor_ids')")->fetchObject()->nextval . ".0");
    $exec_out = array();
    exec($mySubmitCommands[0] . " " . $mySubmitCommands[1], $exec_out);

    #parse out condor job id from the $exec_out
    $myJobIds = $this->lackey_parseJobId($exec_out);
    $this->lackey_StoreJobParamsInDB($job_specification, $myJobIds);

    return $this->myId;
  }

  abstract protected function buildSubmitFiles(JobSpec $job_specification, $prefix);

  protected function makeflow_job(JobSpec $job_specification, $prefix){
    //DRUPAL_ROOT constant cannot change after being defined so declaring a
    //drupal_root variable instead to use that for the static path
    $drupal_root = $_SERVER['DOCUMENT_ROOT'];
    if (user_is_logged_in()) {
	    $path_to_result_files = $drupal_root . "/data/job_results/users";
    }	       
    else {
	    $path_to_result_files = $drupal_root . "/data/job_results/anonymous";
    }

    $path_to_submit_files = $drupal_root . "/data/job_submit/" . $prefix;

    mkdir($path_to_submit_files, $mode=0777, $recursive=true);
    chmod($path_to_submit_files, 0777);

# write single query file
    file_put_contents("$path_to_submit_files/$prefix.query.fa", $job_specification->get_input());

    $makeflow_rules  = $this->makeflow_prologue($job_specification, $prefix, $path_to_result_files);
    $makeflow_rules .= $this->makeflow_analysis_rules($job_specification, $prefix, $path_to_result_files);
    $makeflow_rules .= $this->makeflow_parse_rules($job_specification, $prefix, $path_to_result_files);

    $makeflow_file = "$path_to_submit_files/$prefix" . ".mf";
    file_put_contents($makeflow_file, $makeflow_rules);

# Lackey expects executable + one argument:
    $wrapper = $this->makeflow_wrapper_job($job_specification, $prefix, $path_to_submit_files);
    $wrapper_file = "$path_to_submit_files/$prefix" . ".sh";
    file_put_contents($wrapper_file, $wrapper);

    return array("bash", $wrapper_file);
  }

  protected function makeflow_wrapper_job(JobSpec $job_specification, $prefix, $path_to_submit_files) {

	  $wrapper = <<<EOF
#! /bin/sh -e

export PATH=/opt/local/cctools/bin:/localhome/condor/software/bin:\${PATH}
export PERL5LIB=/opt/local/perl5/lib/perl5:\${PERL5LIB}
export CONDOR_CONFIG=/localhome/condor/software/etc/condor_config

cd $path_to_submit_files

condor_submit << EJOB
universe    = local
executable  = /opt/local/cctools/bin/makeflow
arguments   = -Tcondor -dall -o$prefix.debug $prefix.mf
log         = condor.local.log
getenv      = True
initialdir  = $path_to_submit_files

queue
EJOB

EOF;

	return $wrapper;
    }

  protected function makeflow_prologue(JobSpec $job_specification, $prefix, $path_to_result_files) {
	return '';
  }

  protected function makeflow_analysis_rules(JobSpec $job_specification, $prefix, $path_to_result_files) {
	return '';
  }

  protected function makeflow_parse_rules(JobSpec $job_specification, $prefix, $path_to_result_files) {
	return '';
  }


  private function lackey_parseJobId($exec_str) {
    $jobIds = "";
	  foreach ($exec_str as $line) {
      $matches = array();
      preg_match('/submitted to cluster (\d+)/', $line, $matches);
      if ($matches[1]) {
		    if ($jobIds) {
			    $jobIds .= ",$matches[1]";
		    } else {
			  $jobIds = $matches[1];
		    }
      }
    }

    return $jobIds;
  }

  private function lackey_StoreJobParamsInDB(JobSpec $job, $jobIds){
    //save some job params in the db
    global $user;
    $args = array();
    $args['sequence'] = $job->get_input();
    $args['user_name']=$user->name;
    $args['submitter_ip']=$_SERVER['REMOTE_ADDR'];
    $args['detailed_est']='F';
    $args['program']=$job->get_program();
    $args['date']=date("m/d/y");
    $args['time']=date("H:i:s");
    $args['rawId']=$jobIds;
    $args['description'] = $job->get_description();
    $args['condor_job_id'] = $jobIds;

    foreach($job->get_attributes() as $attrib => $value) {
      $args[$attrib] = $value;
    }

    $this->lackey_save_params($jobIds,$args);
    #database information
    foreach ($job->get_target_dbs() as $db) {
      $params['target_database']=$db;
      $this->lackey_save_params($jobIds,$params);
    }
  }

  protected function lackey_save_params($id, $args) {
    foreach($args as $param => $value){
      $fields = array('job_id' => $this->myId, 'argument' => $param, 'value' => $value);
      db_insert('xgrid_job_params')->fields($fields)->execute();
      //dpm("Inserting $param with a value of $value for job id $this->myId"); // Pretty sure this works! just don't want to execute anything just yet
    }
  }
}
