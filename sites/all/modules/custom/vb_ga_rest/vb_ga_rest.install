<?php

/**
 * Implements hook_schema.
 */
function vb_ga_rest_schema() {
  $schema['vb_google_api_configuration'] = array(
    'description' => t('Stores the key that will be used to authenticate to a specific Google API Service.'),
    'fields' => array(
      'id' => array(
        'description' => t('The primary identifier for the Google API configuration.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => t('Human readable name of the Google API configuration'),
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => t('Describes the Google API configuration'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'delta' => array(
        'description' => t('The machine name for the Google API configuration'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'api_key' => array(
        'description' => t('The fid of the API key used to authenticate'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'vb_google_api_configuration_delta' => array('delta'),
    ),
    'unique keys' => array(
      'delta' => array('delta'),
    ),
    'primary key' => array('id'),
  );

  $schema['vb_google_api_rest_endpoint_api_config'] = array(
    'description' => t('Maps the REST endpoint with the Google API configuration'),
    'fields' => array(
      'id' => array(
        'description' => t('The primary identifier for the mapping'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'endpoint_id' => array(
        'description' => t('Foreign key used to map the Google API configuration with a REST endpoint'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'api_id' => array(
        'description' => t('Foreign key to map the REST endpoint to the Google API configuration'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'endpoint_id' => array(
        'table' => 'vb_google_api_rest_endpoint',
        'columns' => array('endpoint_id' => 'id'),
      ),
      'api_id' => array(
        'table' => 'vb_google_api_configuration',
        'columns' => array('api_id' => 'id'),
      ),
    ),
    'indexes' => array(
      'rest_endpoint_id' => array('endpoint_id'),
      'google_api_id' => array('api_id'),
    ),
  );

  $schema['vb_google_api_rest_endpoint'] = array(
    'description' => t('The base table for the Google API configuration.'),
    'fields' => array(
      'id' => array(
        'description' => t('The primary identifier for a Google API configuration.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => t('Human readable name of the Google API configuration'),
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => t('Stores the description of the Google API configuration'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'delta' => array(
        'description' => t('The machine name for the Google API configuration'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'service_id' => array(
        'description' => t('Stores the ID of the service that will be connected to by the Google API'),
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE
      ),
    ),
    'indexes' => array(
      'vb_google_api_rest_endpoint_delta' => array('delta'),
    ),
    'unique keys' => array(
      'delta' => array('delta'),
    ),
    'primary key' => array('id'),
  );

  $schema['vb_google_api_rest_endpoint_allowed_parameters'] = array(
    'description' => t('Stores the allowed query parameters that will be used by the Google API REST endpoint'),
    'fields' => array(
      'id' => array(
        'description' => t('The primary identifier for the allowed query parameter for the Google API REST endpoint'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'endpoint_id' => array(
        'description' => t('Foreign key used to map the allowed parameter to a Google API configuration'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'query_parameter' => array(
        'description' => t('The query parameter that will be allowed in the URL for the Google API endpoint'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'required' => array(
        'description' => t('Flag to let the module know that this query paremter is required for the Google API endpiont'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'endpoint_id' => array(
        'table' => 'vb_google_api_rest_endpoint',
        'columns' => array('endpoint_id' => 'id'),
      ),
    ),
    'indexes' => array(
      'rest_endpoint_id' => array('endpoint_id'),
    ),
  );

  // @todo Even though we are storing this information, currently we do not
  //   check if a user of the REST endpoint entered accepted values.  We do use
  //   this information to check if we need to do further processhing to the
  //   value returned from the Google API.
  $schema['vb_google_api_rest_endpoint_allowed_parameter_values'] = array(
    'description' => t('Stores all the possible allowed values for the query parameters for the Google API REST endpoint.'),
    'fields' => array(
      'id' => array(
        'description' => t('The primary identifier for the allowed value of the Google API REST endpoing query parameter.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'allowed_parameter_id' => array(
        'description' => t('Foreign key used to map the allowed query parameter value with the query parameter'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'allowed_value' => array(
        'description' => t('The allowed value for a query parameter of a Google API REST endpoint'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'properties' => array(
        'description' => t('Flag to let module know that this specific value will set additional properties'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'allowed_parameter_id' => array(
        'table' => 'vb_google_api_rest_endpoint_allowed_parameters',
        'columns' => array('allowed_parameter_id' => 'id'),
      ),
    ),
    'indexes' => array(
      'rest_endpoint_allowed_parameter' => array('allowed_parameter_id'),
    ),
  );

  $schema['vb_google_api_rest_endpoint_allowed_parameter_value_properties'] = array(
    'description' => t('Stores the additional functions and values that will be applied to the allowed values of the query parameters'),
    'fields' => array(
      'id' => array(
        'description' => t('The primary identifier for the properties of the allowed calues of the query parameters'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'parameter_value_id' => array(
        'description' => t('Foreign key used to map the property to the allowed value of the query parameter'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'property' => array(
        'description' => t('The property that will be set for the allowed value of the query parameter'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'value' => array(
        'description' => t('The value that will be used to set the property'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'operator' => array(
        'description' => t('Stores the operator that will be applied on the Google API value'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'transform_value' => array(
        'description' => t('The value that will be used to apply the operator to the Google API value'),
        'type' => 'int',
        'not null' => FALSE,
      ),
      'round' => array(
        'description' => t('The number of decimal places to display from the transformed value'),
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'parameter_value_id' => array(
        'table' => 'vb_google_api_rest_endpoint_allowed_parameter_values',
        'columns' => array('parameter_value_id' => 'id'),
      ),
    ),
    'indexes' => array(
      'rest_endpoint_allowed_parameter_value' => array('parameter_value_id'),
    ),
  );

  $schema['vb_google_api_rest_endpoint_classes'] = array(
    'description' => t('Stores the classes that will be used so the REST endpoing can correctly do a Google API request.'),
    'fields' => array(
      'id' => array(
        'description' => t('The primary identifier for the Google API class the will be used'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'endpoint_id' => array(
        'description' => t('Foreign key used to map the class to the API that will be using it.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'class_name' => array(
        'description' => t('The class that will be created for the Google API'),
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'configurable' => array(
        'description' => t('Flag to know if this  class will need properties set'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'parameter' => array(
        'description' => t('The query paremeter that we will explode.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'delimiter' => array(
        'description' => t('Value used to separate the query parameter value into an array'),
        'type' => 'varchar',
        'length' => 16,
        'not null' => TRUE,
        'default' => '',
      ),
      'query_parameter' => array(
        'description' => t('Flag to know if the values of the properties that will be set for this class will come from query parameters.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'variable_name' => array(
        'description' => t('When class gets created, we store it in an array that can be access later.  This stores what we store it as.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'description' => t('Used to specify the order for which the classes will be created.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'endpoint_id' => array(
        'table' => 'vb_google_api_rest_endpoint',
        'columns' => array('endpoint_api' => 'id'),
      ),
    ),
    'indexes' => array(
      'vb_google_api_rest_endpoint' => array('endpoint_id'),
    ),
  );

  $schema['vb_google_api_rest_endpoint_class_properties'] = array(
    'description' => t('Stores the properties that will be set for the class and the value that will be used for it.'),
    'fields' => array(
      'id' => array(
        'description' => t('The primary identifier for the property of the Google API class.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'class_id' => array(
        'description' => t('Foreign key of the class the property belongs to.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'property' => array(
        'description' => t('The class property that will be set'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'value' => array(
        'description' => t('Stores the variable/index that will be used get the value for the property we are setting'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'class_id' => array(
        'table' => 'vb_google_api_rest_endpoint_classes',
        'columns' => array('class_id' => 'id'),
      ),
    ),
    'indexes' => array(
      'vb_google_api_rest_endpoint_class' => array('class_id'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 *
 * The foreign key attributes from the schema are only for information purposes.
 * Will use the install hook to set the foreign key constraints.
 */
function vb_ga_rest_install() {

}

function vb_ga_rest_uninstall() {
  // Unmark 
  db_delete('file_usage')
    ->condition('module', 'vb_ga_rest')
    ->execute();

  // Get all the files uploaded by the module
  $files = db_select('file_managed', 'fm')
    ->fields('fm', array('fid'))
    ->condition('uri', '%' . db_like('vb_ga_rest') . '%', 'LIKE')
    ->execute()
    ->fetchAll();

  // Go through each file and remove it from server
  foreach ($files as $fileEntry) {
    $file = file_load($fileEntry->fid);
    $file->status = 0;
    file_save($file);
    file_delete($file);
  }
}
