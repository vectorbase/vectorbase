/*
 *The vbsearch.js javascript file is used exclusively by the Advanced Search
 *form.  It is used to display the correct fields belonging to certain domains
 *and to manage the functionality of the "Add field" button.  It also makes sure
 *to clear the fields that are no longer being used to ensure the search query
 *gets constructed properly.
*/

//Create the VectorBaseSearch NamesSpace
(function (VectorBaseSearch, $, undefined) {

  if(VectorBaseSearch.data == undefined) {
    VectorBaseSearch.data = {};
  }

  VectorBaseSearch.data.visible_fields = [];
  VectorBaseSearch.data.domain_confirmation = {'confirmed':false, 'value':''};
  VectorBaseSearch.data.domain_last_value = '';

  if (VectorBaseSearch.methods == undefined) {
    VectorBaseSearch.methods = {};
  }

  VectorBaseSearch.methods.UpdateQueryString = function(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");

    if (re.test(url)) {
      if (typeof value !== 'undefined' && value !== null)
        return url.replace(re, '$1' + key + "=" + value + '$2$3');
      else {
        var hash = url.split('#');
        url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
        if (typeof hash[1] !== 'undefined' && hash[1] !== null)
          url += '#' + hash[1];
        return url;
      }
    }
    else {
      if (typeof value !== 'undefined' && value !== null) {
        var separator = url.indexOf('?') !== -1 ? '&' : '?',
        hash = url.split('#');
        url = hash[0] + separator + key + '=' + value;
        if (typeof hash[1] !== 'undefined' && hash[1] !== null)
          url += '#' + hash[1];
        return url;
      }
      else
        return url;
    }
  };

  VectorBaseSearch.methods.CreateFieldsList = function() {
    //Clear the contents of the current list
    jQuery('#field_list li').remove();
    //jQuery('#edit-field-search-site-und').change();
    var visibleFields = jQuery("#advanced-search-wrapper .form-wrapper:visible");
    jQuery('#field_list').prepend("<li><label><input type='checkbox' value='all'>Select all</label></li>");

    jQuery.each(visibleFields, function(index, value) {
      if (jQuery(value).attr('id') != 'edit-field-search-site') {
        var fieldLabel = jQuery(jQuery(value).find('label')[0]).text();
        var fieldId = jQuery(value).attr('id');
        var fieldValue = true;
        var notEmptyTextFields = jQuery(value).find(':text[value!=""]');
        jQuery('#field_list').append("<li><label><input type='checkbox' value='" + fieldId + "'>" + fieldLabel + "</label></li>");

        // console.log(notEmptyTextFields,'notEmptyTextFields');   //DKDK

        //Didn't have time to think of good condition statement so doing this
        //for now.
        if ((notEmptyTextFields.length == 0 && jQuery(value).find('select').length ==0) || (notEmptyTextFields.length == 1 && notEmptyTextFields.closest('.locentryradius-field').length == 1)) {
          fieldValue = false;
          // console.log('IM at notEmptyTextFields.length');   //DKDK
        } else if (jQuery(value).find('select').length && (jQuery(value).find('select').val() == null || jQuery(value).find('select').val() == "none" || jQuery(value).find('select').val() == 'all')) {
          fieldValue = false;
          // console.log('IM at jQuery(value)');   //DKDK
        }
        //DKDK adding new if for custom_field type - the error comes from jQuery(value).find('select').length, # of pull-down menu and its value=all
        if ((notEmptyTextFields.length != 0 && notEmptyTextFields.closest('.custom-fieldtype-minvalue-field').length == 1) || (notEmptyTextFields.length != 0 && notEmptyTextFields.closest('.custom-fieldtype-maxvalue-field').length == 1)) {
          fieldValue = true;
        }
        // console.log(jQuery(value),'jQuery(value)');   //DKDK
        // console.log(jQuery(value).find('select').length,'jQuery(value).find(select).length ')   //DKDK this is the number of pull-down menu. e.g., 1 for Exp. factors at Exp. Stat.; 2 for custom_fieldtype widgets
        // console.log(jQuery(value).find('select').val(),'jQuery(value).find(select).val()');   //DKDK
        // console.log(fieldValue,'fieldValue');   //DKDK - at custom_fieldtype, minvalue & GO -> True, maxvalue & GO -> still False
        // console.log(notEmptyTextFields.closest('.custom-fieldtype-maxvalue-field').length,'notEmptyTextFields.closest(.custom-fieldtype-maxvalue-field).length'); //DKDK checking purpose
        //if (jQuery(value).find('input').val() == '' || jQuery(value).find('select').val() == null || jQuery(value).find('select').val() == "none") {
        if (fieldValue == false) {
          jQuery('#' + fieldId + ' .form-item').hide();
          jQuery('#' + fieldId + ' .form-item').siblings('.close_field').hide();
        } else {
          jQuery('#field_list').find(':checkbox[value=' + fieldId + ']').attr('checked', true);
          jQuery('#' + fieldId + ' .form-item').siblings('.close_field').show();
        }
      };
    });
  };

  VectorBaseSearch.init = function() {

    //Go through each popup field and add the click event for the done button
    jQuery("[id$=popup-0]").each( function() {
     jQuery('#'+ jQuery(this).attr('id')).focus(function() {
       var thisCalendar = $(this);
       jQuery('.ui-datepicker-calendar').detach();
       jQuery('.ui-datepicker-close').click(function() {
         var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
         var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
         thisCalendar.datepicker('setDate', new Date(year, month, 1));
       });
     });
    });

    jQuery('.fieldset-wrapper').attr('id','advanced-search-wrapper');

    VectorBaseSearch.methods.CreateFieldsList();
    VectorBaseSearch.data.domain_last_value = jQuery('#edit-field-search-site-und option:selected');

    jQuery('#edit-field-search-site-und').click(function(event) {
      //VectorBaseSearch.data.domain_last_value = jQuery('#edit-field-search-site-und option:selected');
    });

    jQuery('.domain-confirmation button').click(function(event) {
      if (jQuery(this).val() == 'yes') {
        //VectorBaseSearch.data.domain_last_value.setAttribute('selected', true);
        jQuery('#edit-field-search-site-und').val(VectorBaseSearch.data.domain_last_value.val())
        VectorBaseSearch.data.domain_confirmation.confirmed = true;
        jQuery('#edit-field-search-site-und').change();
      } else {
        VectorBaseSearch.data.domain_last_value = jQuery('#edit-field-search-site-und option:selected');
      }

      jQuery('.domain-confirmation').hide();
    });

    jQuery('#edit-field-search-site-und').change(function(event) {
      var hiddenTextfields = jQuery.merge(jQuery('select', ':hidden'), jQuery(':text', ':hidden'));
      var length = hiddenTextfields.length;
      var currentSelectedValue = jQuery('#edit-field-search-site-und option:selected');

      for (var i = 0; i < length; ++i) {
        if (jQuery('#' + hiddenTextfields[i].id).closest('.locentryradius-field').length == 0) {
          var fieldValue = jQuery('#' + hiddenTextfields[i].id).val();

          if (VectorBaseSearch.data.domain_confirmation.confirmed == false && fieldValue != null && fieldValue != '' && fieldValue != 'none' && fieldValue != 'all')  {
            jQuery('.confirmation-message').empty();
            jQuery('.confirmation-message').append('Switching domains will cause one or more populated fields to be removed, continue?');
            //VectorBaseSearch.data.domain_last_value.setAttribute('selected', true);
            jQuery('#edit-field-search-site-und').val(VectorBaseSearch.data.domain_last_value.val())
            //Temporarily confirm the change to prevent infinite loop from
            //occurring when doing the change of select list to bring back
            //fields that were hidden buy drupal's state.js
            VectorBaseSearch.data.domain_confirmation.confirmed = true;
            jQuery('#edit-field-search-site-und').change();
            VectorBaseSearch.data.domain_last_value = currentSelectedValue;
            jQuery('.domain-confirmation').show();
            return;
          } else {
            jQuery('#' + hiddenTextfields[i].id).val('');
            jQuery('#' + hiddenTextfields[i].id + ' option:selected').removeAttr('selected');
          };
        }
      }

      //jQuery('#edit-field-search-expt-factors option:selected').removeAttr('selected')
      //jQuery("#edit-field-search-has-geodata option:selected").removeAttr('selected');

      VectorBaseSearch.data.domain_confirmation.confirmed = false;
      jQuery('.domain-confirmation').hide();

      VectorBaseSearch.data.domain_last_value = jQuery('#edit-field-search-site-und option:selected');
      VectorBaseSearch.methods.CreateFieldsList();
    });

    //This event will no longer be used for the near future since the subdomains
    //are going to be placed in the domain list as well
    jQuery('#edit-field-search-popbio-subdomain-und').change(function(event) {
      var hiddenTextfields = jQuery(':text',':hidden');
      var length = hiddenTextfields.length;
      for (var i = 0; i < length; ++i) {
        if (jQuery('#' + hiddenTextfields[i].id).closest('.locentryradius-field').length == 0) {
          $('#' + hiddenTextfields[i].id).val('');
        }
      }

      jQuery('#edit-field-search-date option:selected').removeAttr('selected')
      jQuery("#edit-field-search-has-geodata").val("all").trigger("change");
    });

    jQuery('#edit-field-search-exp-subdomain-und').change(function(event) {
      var hiddenTextfields = jQuery(':text',':hidden');
      var length = hiddenTextfields.length;
      for (var i = 0; i < length; ++i) {
        if (jQuery('#' + hiddenTextfields[i].id).closest('.locentryradius-field').length == 0) {
          $('#' + hiddenTextfields[i].id).val('');
        }
      }

      if(!jQuery('#edit-field-search-exp-subdomain-und').is(':visible')) {
        jQuery("#edit-field-search-popbio-subdomain-und").val("All").trigger("change");
      };

      jQuery('#edit-field-search-expt-factors option:selected').removeAttr('selected')
    });

    jQuery("#edit-advanced-search legend a").click(function() {
        var url = window.location.href;
        if(url.indexOf('?as=True') != -1) {
          $("#search-form").attr("action", VectorBaseSearch.methods.UpdateQueryString('as'));
        } else if (url.indexOf('&as=True') != -1) {
          $("#search-form").attr("action", VectorBaseSearch.methods.UpdateQueryString('as'));
        } else {
          $("#search-form").attr("action", VectorBaseSearch.methods.UpdateQueryString('as', 'True'));
        }

        VectorBaseSearch.methods.CreateFieldsList();
    });

    jQuery(".ui-datepicker-close").click(function() {
      var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
      $(this).datepicker("setDate", new Date(year, month, 1));
    });

    jQuery("#add_field").click(function(e) {
      e.preventDefault();
      if (jQuery("#field_list_div").is(":visible")) {
        jQuery("#field_list_div").hide();
        jQuery(".tip").hide();
      } else {
        jQuery("#field_list_div").show();
        jQuery(".tip").show();
      }
    });

   /* jQuery('#edit-advanced-search .fieldset-legend  a').click(function(e) {
      //Clear the contents of the current list
      jQuery('#field_list li').remove();
      var visibleFields = jQuery("#advanced-search-wrapper .form-wrapper:visible");

      jQuery.each(visibleFields, function(index, value) {
        if (jQuery(value).attr('id') != 'edit-field-search-site') {
          var fieldLabel = jQuery(value).find('label').text();
          var fieldId = jQuery(value).attr('id');

          jQuery('#field_list').append("<li><a><label><input type='checkbox' value='" + fieldId + "'>" + fieldLabel + "</label></a></li>");
          jQuery('#' + fieldId + ' .form-item').hide();
        };
      });
    });*/

    //This event will no longer be used for the near future because the
    //subdomains are going to be placed in the domain list as well
    //NOTE: I still need ot update this to the same behavior as the popbio
    //sub-domain if the domain and sub-domain ever get their own list again
    jQuery('#edit-field-search-exp-subdomain-und').change(function() {
      jQuery('#field_list li').remove();
      var visibleFields = jQuery("#advanced-search-wrapper .form-wrapper:visible");

      jQuery.each(visibleFields, function(index, value) {
        if (jQuery(value).attr('id') != 'edit-field-search-site' && jQuery(value).attr('id') != 'edit-field-search-exp-subdomain') {
          var fieldLabel = jQuery(jQuery(value).find('label')[0]).text();
          var fieldId = jQuery(value).attr('id');

          jQuery('#field_list').append("<li><div data-value='" + fieldId + "'><a href='#'>" + fieldLabel + "</a></div></li>");
          jQuery('#' + fieldId + ' .form-item').hide();
        };
      });
    });

    jQuery(document).mouseup(function (e) {
      var container = jQuery("#field_list_div");

      if (container.attr('id') != jQuery(e.target).attr('id') && container.has(e.target).length === 0) {
        container.hide();
        jQuery('.tip').hide();
      }
    });

    //Not used at the moment, but could change in the future
    /*jQuery('#field_list').delegate('div', 'click', function(e) {
      var selector = jQuery(this).data('value');
      jQuery('#' + selector + ' .form-item').show();
      jQuery('#' + selector + ' .form-item').siblings('.close_field').show();
      jQuery('#' + selector).css('display', 'table');
      jQuery('.tip').hide();
      jQuery('#field_list_div').hide();
    });*/

    //Do a change event when checbox is checked.
    jQuery('#field_list').delegate(':checkbox', 'change', function(e) {
      var checkbox = jQuery(this);
      var selector = checkbox.val();
      // console.log(selector);
      //Go through the field list and show all fields if clicked "Select All"
      //If "Select All" not checked, go through normal process of displaying
      //the field
      if (selector == 'all') {
        var $field_list = jQuery('#field_list :checkbox');

        //Go through each field in the list
        jQuery.each($field_list, function(index, field) {
          //Skip the "Slect All" field
          if (jQuery(field).val() != 'all') {
            //Trigger change event, click event would check and uncheck the
            //checkbox at the end of the event
            if (checkbox.attr('checked') == false) {
              jQuery(field).attr('checked', false);
              jQuery(field).change();
            } else {
              jQuery(field).attr('checked', true);
              jQuery(field).change();
            }
          }
        });
      } else {
        //Display/hide the field if checkbox is checked/unchecked
        if (checkbox.attr('checked') == false) {
          jQuery('#' + selector + ' .form-item').hide();
          jQuery('#' + selector + ' .close_field').hide()
          jQuery('#' + selector + ' .form-item').find(':text').val('');
          jQuery('#' + selector + ' .form-item').find('select option:selected').removeAttr('selected');
        } else {
          jQuery('#' + selector + ' .form-item').show();
          jQuery('#' + selector + ' .close_field').show();
          jQuery('#' + selector + ' .form-item').find(':text').closest('.locentryradius-field').find(':text').val('10');
          jQuery('#' + selector).css('display', 'table');

          jQuery(".form-item label a.tooltipClick").click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            jQuery("div.tooltipDesc").show();
          });

          // DKDK enable to close tooltip when pressing out of link          
          jQuery(document).click(function() {
            jQuery('div.tooltipDesc').hide();
          });
          // DKDK add help for specific field VB-6887 - end

       } // end of else (L324)
      }
    });

    //DKDK VB-7910 make these independent of above click event to solve disappearance of help icons
    jQuery('#edit-field-search-expt-factors .form-item label').replaceWith('<label for="edit-field-search-expt-factors-und">Experimental factors <a href="/documentation/expression-browser-user-guide#Experimental_factor_icons" target="_blank"><img src="/sites/default/files/ftp/help_question_blue.png" width="15" height="15" align="right"></a></label>');
    jQuery('#edit-field-search-p-value .form-item label').replaceWith('<label>p-value <a href="/documentation/expression-browser-user-guide#P-value_colour_code" target="_blank"><img src="/sites/default/files/ftp/help_question_blue.png" width="15" height="15" align="right"></a></label>');
    jQuery('#edit-field-search-transcript-id .form-item label').replaceWith('<label for="edit-field-search-transcript-id-und-0-value">ID <a href="#" class="tooltipClick"> <img src="/sites/default/files/ftp/help_question_blue.png" width="15" height="15" align="right"></a></label><div class="tooltipDesc" style="display:none">This field could be either of the following:<br>identifier, accession, history, id, and gene_ids</div>');
    jQuery('#edit-field-search-name .form-item label').replaceWith('<label for="edit-field-search-name-und-0-value">Name <a href="#" class="tooltipClick"> <img src="/sites/default/files/ftp/help_question_blue.png" width="15" height="15" align="right"></a></label><div class="tooltipDesc" style="display:none">This field could be either of the following:<br>synonym, label, and cvterms</div>');
    // VB-7618 Exact Gene ID field
    jQuery('#edit-field-search-exact-genome-id .form-item label').replaceWith('<label for="edit-field-search-exact-genome-id-und-0-value">Exact gene ID <a href="#" class="tooltipClick"> <img src="/sites/default/files/ftp/help_question_blue.png" width="15" height="15" align="right"></a></label><div class="tooltipDesc" style="display:none">This field does not retrieve history IDs</div>');
    // VB-7796 range search helps for seq_region_start/end 
    jQuery('#edit-field-search-vari-start .form-item label').replaceWith('<label for="edit-field-search-vari-start">Sequence region start <a href="/tutorials/tools-and-resources-tutorials/variation" target="_blank"><img src="/sites/default/files/ftp/help_question_blue.png" width="15" height="15" align="right"></a></label>');
    jQuery('#edit-field-search-vari-end .form-item label').replaceWith('<label for="edit-field-search-vari-end">Sequence region end <a href="/tutorials/tools-and-resources-tutorials/variation" target="_blank"><img src="/sites/default/files/ftp/help_question_blue.png" width="15" height="15" align="right"></a></label>');

    jQuery('.close_field').click(function() {
      var fieldId = jQuery(this).parents('.form-wrapper').attr('id');
      jQuery(this).hide();
      jQuery('#' + fieldId + ' .form-item').hide();
      if (jQuery('#' + fieldId).hasClass('field-type-locentry')) {
        jQuery('#' + fieldId + ' .form-item').find(':text').closest('.locentryradius-field').find(':text').val('10');
        jQuery('#' + fieldId + ' .form-item').find(':text').closest('.locentrycoords-field').find(':text').val('');
        jQuery('#' + fieldId + ' .form-item').find(':text').closest('.locentrycoordsbox-field').find(':text').val(''); //DKDK
      } else {
        jQuery('#' + fieldId + ' .form-item').find(':text').val('');
      }
      jQuery('#' + fieldId + ' .form-item').find('select option:selected').removeAttr('selected');
      jQuery('#field_list').find(':checkbox[value=' + fieldId + ']').attr('checked', false);
    });

    jQuery('#edit-toggle-search-type').click(function() {
      if (jQuery('#edit-field-file-search').css('display') == "none") {
        jQuery('#edit-field-file-search').show();
        jQuery('.form-item-keys').hide();
      } else {
        jQuery('#edit-field-file-search').hide();
        jQuery('.form-item-keys').show();

        //Trying to remove uploaded button
        var remove_button = jQuery(':input[value="Remove"]').css('display');
        if (remove_button != "none" && remove_button != undefined) {
          jQuery(':input[value="Remove"]').mousedown();
        }
      }
    });

    //Hide search box that is not needed
    if (jQuery('#edit-field-file-search').hasClass('file-search-hide') == false) {
      jQuery('.form-item-keys').hide();
      //Adding this here because by default this field is hidden
      jQuery('#edit-field-file-search').show();
    }

  };

  $(document).ready(function () {
    VectorBaseSearch.init();
    //In CSS, we made the advanced search be hidden first to prevent
    //Users from seeing the Raw Form (i.e All fields displaying)
    //So we display the Form was the fields have been formatted
    //And then we recreate the list again to hide blank fields
    jQuery('#edit-advanced-search').show();
    VectorBaseSearch.methods.CreateFieldsList();
//Notes: jQuery('#advanced-search-wrapper .form-wrapper :visible select')
//Notes: jQuery('label[for="edit-field-search-site-und"]').text()
  });
})(window.VectorBaseSearch = window.VectorBaseSearch || {}, jQuery);

