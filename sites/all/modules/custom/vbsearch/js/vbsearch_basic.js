/*
 *The vbsearch_basic.js javascript file is used by the top-right search block
 *form.  It is currently used to toggle between regular and file search as well
 *as do a work around for submitting the search when pressing enter.
*/

//Create the VectorBaseSearch NamesSpace
(function (VectorBaseSearch, $, undefined) {

  if(VectorBaseSearch.data == undefined) {
    VectorBaseSearch.data = {};
  }

  if (VectorBaseSearch.methods == undefined) {
    VectorBaseSearch.methods = {};
  }

  VectorBaseSearch.basic_init = function() {
    jQuery('#edit-toggle-basic-search-type').click(function() {
      if (jQuery('#search .field-type-file').css('display') == "none") {
        jQuery('#search .field-type-file').show();
        jQuery('input[name=search_block_form').hide();
        jQuery('input[name=search_block_form').val('*');
      } else {
        jQuery('#search .field-type-file').hide();
        jQuery('input[name=search_block_form').show();
        jQuery('input[name=search_block_form').val('');

      }
    });

    //Adding the file search caused the form not to be submitted when pressing
    //enter.  This js fixes that while a better solution is found in the future
    //if needed
    $("#search").keypress(function(event) {
      if ( event.which == 13 ) {
        event.preventDefault();
        $('#edit-actions .form-submit').click();
      }
    });

    $("#search .field-type-file").delegate('.file-widget', 'DOMNodeInserted', function(e) {
      jQuery(e.target).detach().prependTo('#content-group-inner');
    });
  };

  $(document).ready(function () {
    VectorBaseSearch.basic_init();
  });
})(window.VectorBaseSearch = window.VectorBaseSearch || {}, jQuery);

