<?php

/**
 * Implements hook_menu()
 *
 * This function allows us to create the URL that will be used to to give
 * autcomplete suggestions to our VectorBase Search page.
 * @return array
 */

function vbsearch_autocomplete_menu() {
  $items = array();

  //Generates the URL that we will use get our autcomplete suggestions.
  $items['vbsearch/autocomplete/%'] = array(
    'title' => 'VectorBase Search Autocomplete',
    'page callback' => 'vbsearch_autocomplete',
    'page arguments' => array(2,3),
    'access callback' => TRUE,
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * This function is what does a SOLR query, parses the response, and sends the
 * parsed suggestions back to search.
 *
 */

function vbsearch_autocomplete($query, $field='') {
  $query = preg_replace('/\s+/', ' ', $query);
  $searchQuery = escapeColon($query) . '*';
  $queryTerms = explode(' ', $query);
  $lastTerm = end($queryTerms);
  $patternString = '';

  //If getting suggestions from the main search box, use all the searchable
  //fields to look for suggestions, otherwise only search in the fields
  //used by the field to look for suggestions
  if($field == '') {
    //Adding the different solr parameters that were configured to the $query variable
    //Might need to change this in the future if we decide the modify what parameters can get boosts 
    $queryParameters = array(
      'fl' => array('text', 'species'),
      'qf' => array('text', 'species'),
    );
  } else {
    //Get configuration of the field (includes the solr mappings)
    //NOTE: It might be necessary in the future to set the VBSearch type through
    //a dynamic way
    $field_config = vbsearch_type_field_load($field, 'advanced_search');
    $queryParameters = array(
      'fl' => array(),
      'qf' => array(),
      'fq' => array()
    );

    if (!empty($field_config['solr_mapping'])) {
      foreach (explode(",", $field_config['solr_mapping']) as $solr_field) {
        array_push($queryParameters['qf'], $solr_field);
        array_push($queryParameters['fl'] , $solr_field);
      }
    }

    //Add chosen Domain/Sub-Domain to only get subset of suggestions
    $siteMappings = array('site' => '', 'bundle_name' => '');
    list($siteMappings['site'], $siteMappings['bundle_name']) = explode('-', $_GET['field_search_site']);
    foreach ($siteMappings as $key => $value) {
      //Do not filter if the All value is selected for the Domain/Sub-Domain
      if ($value && $value !== 'all') {
        $queryParameters['fq'][$key] = $value;
      }
    }
  }

  //DKDK VB-8046 autocomplete suggestion
  if ($field_config['vbsearch_field_autocomplete_option'] == 'disable') {
    $searchSuggestions = '';
  } 
  else {
    $searchSuggestions = vbsearch_autocomplete_get_suggestions($searchQuery, $queryParameters, $queryTerms, $lastTerm, $field);    
  }

  $query = strtolower($query);
  foreach($searchSuggestions as $key => $searchSuggestion) {
    //Fixing issue where all terms would get capitalized when only one term had
    //a ":" character
    $searchSuggestions[$key] = preg_replace_callback('/\w+:\w+/',
      function ($matches) {
        return strtoupper($matches[0]);
      }, $searchSuggestion);
  }

  return drupal_json_output($searchSuggestions);
  //return "here";
}

function vbsearch_autocomplete_get_suggestions($searchQuery, $queryParameters, $queryTerms, $lastTerm, $field) {
  $solr_url = vbsearch_find_solr_url();
  $newUrl = dirname($solr_url) . '/autocomplete';
  $solrQuery = vbsearch_createSolrQuery($searchQuery, $queryParameters['qf'], $queryParameters['fl'], count($queryTerms), $newUrl);

  //Add Domain/Sub-dimain as filter
  if (isset($queryParameters['fq'])) {
    foreach ($queryParameters['fq'] as $key => $value) {
      $solrQuery->addFilter($key, $value);
    }
  }

  $response = $solrQuery->search();
  $patternString = '';

  //Generate the pattern string to search for in the response
  foreach($queryTerms as $queryTerm) {
    if($queryTerm == $lastTerm) {
      $queryTerm = str_replace(array('*', '\\'), '', $queryTerm);
      $patternString = $patternString . "$queryTerm" . "\S*[^:;\.\)\],\s]";
      //$patternString = $patterString . $queryTerm . "\S*";
    } else {
      //pay attention to the space in the end of the string, it was cuasing me
      //problems when matching
      $patternString = $patternString . "$queryTerm\s";
    }
  }
  $queryTerm = implode(' ', $queryTerms);

  //We go through the response and parse the results to find
  //text that matches what the user typed in a field
  $results = $response->response->docs;

  if ($field == '' || $field == 'field_search_description') {
    $searchSuggestions = vbsearch_autocomplete_get_matches($patternString, $results);
  } else {
    //This has repetive code found in vbsearch_autocomplete_get_matches, could
    //probably improve later, NO TIME THOUGHT FOR THIS RELEASE!! :'(
    $limit = 4;
    $searchSuggestions = array();
    foreach($results as $result) {
      foreach($result as $result_fields) {
        if(is_array($result_fields)) {
          foreach($result_fields as $field_value) {
            if(mb_stripos($field_value, $queryTerm) !== false) {
              $searchSuggestions[] = $field_value;
              $searchSuggestions = array_values(array_unique($searchSuggestions));
              if (count($searchSuggestions) == $limit) {
                break 3;
              }
            }
          }
        } else {
          $field_value = $result_fields;
          if(mb_stripos($field_value, $queryTerm) !== false) {
            $searchSuggestions[] = $field_value;
            $searchSuggestions = array_values(array_unique($searchSuggestions));
            if (count($searchSuggestions) == $limit) {
              break 2;
            }
          }
        }
      }
    }
  }

  //Check to see if we have enough suggestions, if not then get suggestions from
  //the last term
  if (count($queryTerms) > 1 && count($searchSuggestions) < 4 && $field == '') {
    //We are only returning a max of 4 suggestions so need to get only the
    //suggestions that we need
    $patternString = $lastTerm . "\S*[^:;\.\)\],\s]";
    //Removing the escaped "colon" from searchQuery so that the prefix can be
    //created correctly
    $searchQuery = str_replace('\:', ':', $searchQuery);
    $prefix = str_replace($lastTerm . '*', '', $searchQuery);
    $searchSuggestions = vbsearch_autocomplete_get_matches($patternString, $results, $searchSuggestions, $prefix);

  }

  //This returns the term to be autocompleted as a suggestion since the regex
  //that I currently use does not return the exact match.  We assume if we get
  //results, then a match should be there
  if (empty($searchSuggestions) && !empty($results)) {
    $searchSuggestions[] = $queryTerm;
  }

  return $searchSuggestions;
}

function vbsearch_autocomplete_get_matches($patternString, $results, $searchSuggestions = array(), $prefix = '') {
  $limit = 4;
  foreach($results as $result) {
    foreach($result as $result_fields) {

      //Docs fields do not always return an array.
      //Check if it is an array to loop through otherwise
      //get the value to see if we can get suggestions
      //Note: There is repeated code here so will have to simplify when I have
      //more time
      if (is_array($result_fields)) {
        foreach($result_fields as $field_value) {
          preg_match('/' . $patternString . '/i', $field_value, $matches);
          $matches = array_map('strtolower', $matches);
          //Check if we are gong an independent term autocomplete
          if ($prefix != '') {
            foreach($matches as $key => $value) {
              //Do not add the term as a suggestion if the match is already part
              //of the suggestion
              if (stripos($prefix, $value) === false) {
                $matches[$key] = $prefix . $value;
              } else {
                unset($matches[$key]);
              }
            }
          }
          //Strip punctuation from beginning and end of string
          foreach($matches as $key => $value) {
            $matches[$key] = trim(strip_tags($value), ",}.");
          }
          $searchSuggestions = array_unique(array_merge($searchSuggestions, $matches));
          if (count($searchSuggestions) == $limit) {
            break 3;
          }
        }
      } else {
        $field_value = $result_fields;
        preg_match('/' . $patternString . '/i', $field_value, $matches);
        $matches = array_map('strtolower', $matches);
        //Check if we are doing an independent term autocomplete
        if ($prefix != '') {
          foreach($matches as $key => $value) {
            //Do not add the term as a suggestion if the match is already part
            //of the suggestion
            if (stripos($prefix, $value) === false) {
              $matches[$key] = $prefix . $value;
            } else {
              unset($matches[$key]);
            }
          }
        }
        //Strip punctuation from beginning and end of string
        foreach($matches as $key => $value) {
          $matches[$key] = trim(strip_tags($value), ",}.");
        }
        $searchSuggestions = array_unique(array_merge($searchSuggestions, $matches));
        if (count($searchSuggestions) == $limit) {
          break 2;
        }
      }
    }
  }

  return $searchSuggestions;
}

function vbsearch_autocomplete_form_alter(&$form, &$from_state, $form_id) {
  if ($form_id == 'search_block_form') {
    //Adding the JS that wil lbe used for AutoComplete
    // Add the jQuery UI autocomplete library.
    drupal_add_library('system', 'ui.autocomplete');
    $form['#attached']['js'][] = array(
        'data' => drupal_get_path('module', 'vbsearch_autocomplete') . '/vbsearch_autocomplete.js',
    );
    $form['#attached']['css'][] = array(
      'data' => drupal_get_path('module', 'vbsearch_autocomplete'). '/vbsearch_autocomplete.css',
    );
  }
}
