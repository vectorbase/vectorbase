(function (VectorBaseSearch, $, undefined) {

  if (VectorBaseSearch.data == undefined) {
    VectorBaseSearch.data = {};
  }

  var filter_results_checked = false;

  function cancelSearchSubmit(e) {
    $("#facet-not-applied").dialog("close");
    filter_results_checked = false;
  }

  function continueSearchSubmit(e) {
    $("#edit-submit").click();
  }

  VectorBaseSearch.init_filters = function() {
    //$(".facetsort").tablesorter();
    $(".search_filter_box table").each(function() {
      var table_id = $(this).attr('id');
      //$(this).tablesorterPager({container: $("#" + table_id + "-pager")});
      //Drupal adds sticky headers, need to take them into account
      if ($(this).hasClass("sticky-header") == false) {
        $(this).tablesorter({sortList: [[2,1]], headers: {0: {sorter: false}}}).tablesorterPager({container: $("#" + table_id + "-pager"), size: 20});
      }
    });

    $("th a").click( function() {
      window.location = this.href;
    });

    $("a.remove-row").click(function() {
      $(this).closest("tr").fadeOut(function() {
        $(this).find("input.form-autocomplete").val('').closest("form").submit();
      });
    });

    $('#add-facet-category').click(function(e) {
      e.preventDefault();
      if (jQuery("#facet_category_list_div").is(":visible")) {
        jQuery("#facet_category_list_div").hide();
        jQuery(".facet_tip").hide();
      } else  {
        jQuery("#facet_category_list_div").show();
        jQuery(".facet_tip").show();
      }
    });


    jQuery(document).mouseup(function (e) {
      var container = jQuery("#facet_category_list_div");
      if (container.attr('id') != jQuery(e.target).attr('id') && container.has(e.target).length === 0) {
        container.hide();
        jQuery('.facet_tip').hide();
      }
    });

    jQuery('#facet_category_list').delegate(':checkbox', 'click', function(e) {
      var checkbox = jQuery(this);
      var selector = checkbox.val();
      //Check if we need to display the apply button
      var checked_checkboxes = $('#' + selector).find('input:checked').length;

      if (checkbox.attr('checked') == false) {
        jQuery('#' + selector).fadeOut();
        jQuery('#' + selector + '-pager').fadeOut();
        jQuery('#' + selector + '-submit').fadeOut();
      } else {
        jQuery('#' + selector).fadeIn();
        jQuery('#' + selector + '-pager').fadeIn();

        //Only display the submit button if there are facets checked
        if (checked_checkboxes) {
          jQuery('#' + selector + '-submit').fadeIn();
        }
      }
    });

    // Click handlers for the multi-select filters
    $('.search_filters').delegate('.tableselect tr', 'click', function(e) {
      //For some reason needed to move up to tr element using closest
      var links = $(e.target).closest('tr').find('a');

      //Only fire events of not clicking the checkbox and the links are disabled
      if (e.target.type !== 'checkbox' && links.hasClass('disabled')) {
        //For some reason, needed to do a click to check the box and then a
        //change event to check if it was checked or not
        $(':checkbox', this).click();
        $(':checkbox', this).change();
      }
    });

    // Prevents the link from working, used when facet category is in
    // multi-select mode so that seleting the link will toggle the checkbox
    // instead of redirecting user
    $('.search_filters').delegate('.tableselect a', 'click', function(e) {
      if ($(e.target).hasClass('disabled')) {
        e.preventDefault();
      }
    });

    //Display or hide the apply button if a checkbox state changes
    $('.search_filters').delegate(':checkbox', 'change', function(e) {
      var apply_button = $(e.target).closest('form').find('.form-submit');
      var checked_checkboxes = $(e.target).closest('table').find('input:checked').length;

      if (checked_checkboxes && apply_button.css('display') == 'none') {
        apply_button.fadeIn().css('display', 'block');;
      } else if (!checked_checkboxes && apply_button.css('display') != 'none') {
        apply_button.fadeOut();
      }
    });

    // Toggling between single and multi-select facets
    $('.toggle-select').click(function (e) {
      var button = $(e.target);
      var checkboxes = $(e.target).closest('form').find('input[type="checkbox"]');
      var apply_button = $(e.target).closest('form').find('.form-submit');
      var checked_checkboxes = $(e.target).closest('form').find('input:checked').length;
      var links = $(e.target).closest('form').find('a');
      var checkboxes_displayed = $(checkboxes).css("display");

      //Display or hide the checkbox and update the button with correct text
      if (checkboxes_displayed === "none") {
        button.text("Single Select");
        links.addClass('disabled');
        checkboxes.each(function () {
          //For some reason I can't see the effect, but it is the same on Live
          //so not worrying about it
          $(this).fadeIn();
        });

        if (checked_checkboxes) {
          apply_button.fadeIn();
        }
      } else {
        button.text("Multi Select");
        apply_button.fadeOut();
        links.removeClass('disabled');
        checkboxes.each(function () {
          //Taking into consideration checkboxes that are not in page being
          //viewed on pager.  fadeOut does not do anything for checkboxes that
          //are not the page being viewed in the facet category
          if ($(this).closest('tr').is(":hidden")) {
            $(this).hide();
          } else {
            $(this).fadeOut('slow');
          }
        });
      }
    });

    $(".facetsort td:nth-child(2) a").mouseover(function (e) {
      var delta = jQuery(this).closest('table').attr('data-delta');
      if (jQuery(this).attr('title') == "Loading description..." && delta != "") {
        var query = jQuery(this).text();
        var hit_description_url = "/vbsearch/facets/hit/description/" + query + "/" + delta;
        VectorBaseSearch.data.link = this;
        jQuery.get(hit_description_url, function(data) {
          if (data != null ) {
            jQuery(VectorBaseSearch.data.link).attr('title', data);
          } else {
            jQuery(VectorBaseSearch.data.link).attr('title', "Description not found"); 
          }
        });
      }
    });

    $("#edit-submit").click(function (e) {

      if (!filter_results_checked) {
        filter_results_checked = true;
        var checked_checkboxes = $(".search_filter_box").find('input:checked').length;

        //Display dialog box if there are checked facets that were not applied
        if (checked_checkboxes) {
          e.preventDefault();
          $("#facet-not-applied").dialog({
            autoOpen: true,
            show: "scale",
            hide: "scale",
            draggable: false,
            resizable: false,
            modal: true,
            dialogClass: "no-close",
            buttons: [{
              id:"cancel-search-submit",
              text: "Cancel Search",
              click: cancelSearchSubmit
            },
            {
              id:"continue-search-submit",
              text:"Continue Search",
              click: continueSearchSubmit
            }]
          });

          $("#facet-not-applied").html("Filters in one or more facet categories was not applied for this Search.  Continue search without applying filters?");
        }
      } else {
        filter_results_checked = false;
      }

    });
  };

  //Wait until the page has completely loaded before display the facet list
  $(window).load(function () {
    $('.facet-loader-div').show();
    VectorBaseSearch.init_filters();
    //Hide the loader gif and display the facets on page
    $('.facet-loader-div').fadeOut();
    $('.search_filters').fadeIn();
  });
})(window.VectorBaseSearch = window.VectorBaseSearch || {}, jQuery);
