(function (VectorBaseSearch, $, undefined) {

  if (VectorBaseSearch.data == undefined) {
    VectorBaseSearch.data = {};
  }

  VectorBaseSearch.init_actions = function() {
    VectorBaseSearch.data.blast_job = {};

    var reference_species = $("tr:contains('Reference species') td:eq(1)").text();
    var pinwheel = '<br><img src="/' + Drupal.settings.blast.blastPath + '/ajax-loader.gif">';

    //Use reference species field to blast against if it is set, otherwise use
    //the species field to blast against
    if (reference_species != '') {
      VectorBaseSearch.data.blast_job.dbs = reference_species;
    } else {
      VectorBaseSearch.data.blast_job.dbs = $('#result-details-box i').text();
    }

    if ($('#sequence_s').html() != null) {
      VectorBaseSearch.data.blast_job.sequence = $('#sequence_s').html().replace(/<br>/g, '\n').replace('&gt;', '>');
    }

    if ($('#accession').text() != '') {
      VectorBaseSearch.data.blast_job.description = $('#accession').text();
    }

    $("#submissionDialog").dialog({
      autoOpen: false,
      show: "scale",
      hide: "scale",
      width: 270,
      height: 100,
      draggable: false,
      modal: true,
      title: "Blast Job Status"
    });

    $("#action-buttons button").click(function() {
      $("#submissionDialog").dialog("open");
      $("#submissionDialog").html("Blast job is running" + pinwheel);
      VectorBaseSearch.data.blast_job.program = $(this).text().toLowerCase().trim();

      $.ajax({
        type: "POST",
        timeout: 3200000,
        url: "/blast/rest/submit",
        data: VectorBaseSearch.data.blast_job,
        success: function(blast_job) {
          $("#submissionDialog").dialog("close");

          if (blast_job.status == 'done') {
            $("#action-result").html('Job <a target="_blank" href="/blast?job_id=' + blast_job.id + '">' + blast_job.id + '</a> ' + blast_job.status);
          } else {
            $("#action-result").html('Job ' + blast_job.id + ': ' + blast_job.status);
          }
        },
        error: function(msg) {
          $("#submissionDialog").dialog("open");
          $("#submissionDialog").html("Error: " + msg);
        }
      });
    });

    //Check if the copy command is supported by the browser
    if (!document.queryCommandSupported('copy')) {
      $('#copy-sequence').hide();
    }

    $('#copy-sequence').click(function (event) {
      var sequence = document.querySelector('#sequence_s');
      var range = document.createRange();
      range.selectNode(sequence);
      window.getSelection().addRange(range)

      //Execute the copy command, using try catch in case the if statement that
      //checks if the copy command is supported did not work as expected
      try {
        var successful = document.execCommand('copy');
      } catch(err) {
        windogs.alert("Copy command not supported by your browser");
      }

      window.getSelection().removeAllRanges();
    });

  };

  $(document).ready(function () {
    VectorBaseSearch.init_actions();
  });
})(window.VectorBaseSearch = window.VectorBaseSearch || {}, jQuery);
