<?php

#function vbsearch_indexing_apachesolr_field_mappings() {
  #drupal_set_message("field_mappings");
#  drupal_set_message(print_r($mappings, true));
#}

#function vbsearch_indexing_apachesolr_field_mappings_alter(&$mappings, $entity_type) {
#  drupal_set_message("field_mappings_alter");
#  foreach($mappings as $map=>$info) {
#    drupal_set_message($map . ' => ' . print_r($info, true));
#  }
#}

function vbsearch_indexing_apachesolr_index_documents_alter(array &$documents, $entity, $entity_type, $env_id) {
  //Just use this function to create the index files
  //Get all of the solr_fields used to generate the document to index
  $solr_fields_config = db_query("SELECT * FROM {vbsearch_solr_field_config} WHERE content_field_solr_mappings != :content_field_solr_mappings", array(
    ':content_field_solr_mappings' => '',
  ));

  $documents[0]->clear();

  //Setting the value of the solr field the content type field maps to
  foreach($solr_fields_config as $solr_field_config) {
    $solr_field = $solr_field_config->solr_field_name;
    $content_field_solr_mappings = unserialize($solr_field_config->content_field_solr_mappings);
    foreach ($content_field_solr_mappings as $content_type => $field) {
      $field = preg_replace('/\s+/', '', $field);
      if ($content_type == 'All' || $content_type == $entity->type){
        //Checking for specific cases to index the node fields correctly
        if (preg_match('/^(["\']).*\1$/m', $field)) {
          $documents[0]->setField($solr_field, str_replace('"', "", $field));
        } else if($solr_field == 'content') {
          //Check first entity has field defined so an error does not come up in
          //the logs
          if (isset($entity->$field)) {
            $body = $entity->$field;
            if (isset($body['und'][0]['value'])) {
              $documents[0]->setField($solr_field, strip_tags($body['und'][0]['value']));
            }
          }
        } else if($solr_field == 'timestamp') {
          $documents[0]->setField($solr_field, date('Y-m-d\TH:i:s\Z', $entity->$field));
        } else if ($solr_field == 'url' || $solr_field == 'path') {
          $drupal_url = "node/" . $entity->$field;
          $documents[0]->setField($solr_field, "/" . drupal_get_path_alias($drupal_url));
        } else if ($solr_field == 'bundle_name') {
          //$bundle_name = $entity->$field;
          $documents[0]->setField($solr_field, nameCleaner($entity->$field));
        } else {
          $documents[0]->setField($solr_field, $entity->$field);
        }
      }
    }
  }

  //Might want to use GoogleAnalytics API
  $nodevisit = statistics_get($entity->nid);
  //Some nodes return no statistics, check because causes error if field is set
  //with empty string
  if (isset($nodevisit['totalcount'])) {
    $documents[0]->setField('nodecount_i', $nodevisit['totalcount']);
  }

  //Adding this code here for the moment while I figure out how to better add it
  // add fields to be included in search (if available)
  if (isset($entity->field_population_biology_sample_['und'][0]['value'])){
    //getField() retrieves an array, need to get the actual value
    $content = $documents[0]->getField('content');
    $content = $content['value'];
    $documents[0]->setField('content', $content . " " . strip_tags($entity->field_population_biology_sample_['und'][0]['value']) . " ");
  }

  $content = $documents[0]->getField('content');
  if( $content['value'] != "" ) {
    $documents[0]->description = substr($content['value'], 0, 125) . "...";
  }

  // downloadable files
  // this serves to pull associate a downloadable file with a species
  if (strcmp($entity->type, "downloadable_file") == 0) {
    if (!empty($entity->field_organism_taxonomy)) {
      $tid = $entity->field_organism_taxonomy['und'][0]['tid'];   // the tid as it exists in the node
      $nameResult = db_query("select name from taxonomy_term_data where tid = :tid", array(':tid' => $tid));
      $name = $nameResult->fetchField(0);
      if ($name) {
        $documents[0]->setField('species', $name);
      }
    }
    $documents[0]->setField('description', $entity->field_description['und'][0]['value']);
  }

  //Adds the reference_genome field to be set for organism
  if ($entity->type == 'organism') {
    $query = new EntityFieldQuery();

    $entities = $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', 'strain')
      ->fieldCondition('field_organism', 'target_id', $entity->nid)
      ->execute();

    if (isset($entities['node'])) {
      $nids = array_keys($entities['node']);
      $nodes = node_load_multiple($nids);
    } else {
      $nodes = array();
    }

    $ensembl_names = array();
    foreach ($nodes as $node) {
      if (!empty($node->field_ensembl_name['und'][0]['value'])) {
        $ensembl_name = $node->field_ensembl_name['und'][0]['value'];

        if ($ensembl_name == 'Anopheles_gambiaeS') {
          $ensembl_name = 'Anopheles_gambiae_pimperena';
        } else if ($ensembl_name == 'Anopheles_stephensiI') {
          $ensembl_name = 'Anopheles_stephensi_indian';
        } else if ($ensembl_name == 'Anopheles_sinensisC') {
          $ensembl_name = 'Anopheles_sinensis_china';
        }

        //DKDK VB-8100 VB Drupal indexing error
        // $documents[0]->setField('reference_genome_s', $ensembl_name);
        $ensembl_names[] = $ensembl_name;
      }
    }
    //DKDK VB-7917 changed from _ss to _s as unified; VB-8100 go to ss
    $documents[0]->setField('reference_genome_ss', $ensembl_names);
  }
}

#function vbsearch_indexing_apachesolr_index_document_build(ApacheSolrDocument $document, $entity, $entity_type, $env_id) {
#  drupal_set_message("index_document_build");
#  drupal_set_message(print_r($document, true));
#}

function nameCleaner($name) {
  $name = ucfirst($name);
  $name = str_replace("_", " ", $name);
  return $name;
}

function vbsearch_indexing_apachesolr_exclude($entity_id, $entity_type, $row, $env_id) {
  // Never index deprecated or archived nodes
  //$entity->field_status[und][0][value];
   //Getting the correct configuration values is kind of cryptic right now, but
  //it should be better once the configuration stuff is stored in the database
  $node = node_load($entity_id);

  if (isset($node->field_status['und'][0]['value'])) {
    $fieldStatus = $node->field_status['und'][0]['value'];
  } else {
    $fieldStatus = '';
  }

  //Exclude the Resource Skinning Template from getting indexed
  if ($entity_id == '9765424') {
    return TRUE;
  }

  if ($node->status !=0 && strcmp($fieldStatus, "Archived") != 0 && strcmp($fieldStatus, "Deprecated") != 0 ) {
    return FALSE;
  }

  return TRUE;
}
