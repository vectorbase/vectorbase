// $(document).ready(function() {
(function ($) {

    Drupal.behaviors.vbsearch_md = {
        attach: function (context, settings){
            $('#myBtn_all').click(function(e){
                e.preventDefault();
                // window.open('/vbsearch/download_seq');
                // showAdditionalDialog();
                submitExport();
                return false;
            });

            // declare progressing bar - but this may not be used though
            // var pinwheel = '<br/><center><div id="spinner"><img src="' + Drupal.settings.baseUrl + '/' + Drupal.settings.blast.blastPath + '/ajax-loader.gif" width="220" height="19"></div></center>';
            // var pinwheel = '<br/><center><div id="spinner"><img src="' + Drupal.settings.baseUrl + '/' + Drupal.settings.blast.blastPath + '/ajax-loader.gif" width="220" height="19" style="position: relative; z-index: 1"></div></center>';
            // var pinwheel = '<br/><center><img src="' + Drupal.settings.baseUrl + '/' + Drupal.settings.blast.blastPath + '/ajax-loader.gif" width="220" height="19" style="position: relative; z-index: 1"></center>';
            // var pinwheel = '<br/><center><img src="' + Drupal.settings.baseUrl + '/' + Drupal.settings.blast.blastPath + '/running5.gif" width="64" height="64" style="position: relative; z-index: 2000"></center>';            

            // var pinwheel = '<br/><center><img src="' + Drupal.settings.baseUrl + '/' + Drupal.settings.blast.blastPath + '/ajax-loader.gif"></center>';
            // var pinwheel = '<br/><center><img src="' + Drupal.settings.baseUrl + '/' + Drupal.settings.vbsearch_md.basePath + '/ajax-loader.gif" width="220" height="19"></center>';            

            // global variable?                        
            // window.globalpinwheel = '<br/><img src="' + Drupal.settings.baseUrl + '/' + Drupal.settings.blast.blastPath + '/ajax-loader.gif">';                                    

            function submitExport() {
                // create dialog popup
                // $("#mdSubmissionDialog").dialog({
                $("#mdSubmissionDialog").dialog({                
                    autoOpen: true,
                    show: "scale",
                    hide: "scale",
                    width: 485,
                    height: 440,
                    draggable: false,
                    resizable: false,                    
                    // modal: true,
                    modal: true,                    
                    title: "Export",
                    // stack: true,

                    // make three buttons
                    buttons: [{
                        id:"btn-export-download",
                        text: "Export Download",
                        click: function(e) {
                            e.preventDefault();
                            // for the case of Anonymous user
                            var that = this;
                            if (typeof returnValueArray[1] == 'undefined') {
                                returnValueArray[1] = '';
                            }

                            // declare cookie variable
                            var fileDownloadCheckTimer;
                            //use the current timestamp as the token value
                            var tokenValue = new Date().getTime(); 

                            // change dialog message depending on background or not
                            if (returnValueArray[3] == 'on') {
                                // pass user_name
                                showAdditionalDialog(returnValueArray[2],returnValueArray[3]);
                                $.ajax({
                                    async   : false,
                                    url     : Drupal.settings.baseUrl + '/' + Drupal.settings.vbsearch_md.basePath + '/vbsearch_md_exportDownload.php',
                                    // type    : 'POST',
                                    type    : 'GET',
                                    data    : { value0: returnValueArray[0], value1: returnValueArray[1], value2: tokenValue },
                                    success : function(resp) {
                                        // alert("Submitted !!!");
                                        // console.log(resp)

                                    },
                                    // error   : function(resp){
                                    //     //alert(JSON.stringify(resp));
                                    // }
                                });
                            } else {
                                showAdditionalDialog(returnValueArray[2],returnValueArray[3]);
                                window.location = Drupal.settings.baseUrl + '/' + Drupal.settings.vbsearch_md.basePath + 
                                                  '/vbsearch_md_exportDownload.php?value0=' + returnValueArray[0] + '&value1=' + returnValueArray[1] + '&value2=' + tokenValue;

                                // checking Cookie                  
                                fileDownloadCheckTimer = window.setInterval(function () {
                                var cookieValue = $.cookie('fileDownloadToken');
                                // console.log(cookieValue);
                                    if (cookieValue == tokenValue) {
                                    // finishDownload();
                                    window.clearInterval(fileDownloadCheckTimer);
                                    // $.removeCookie('fileDownloadToken'); //clears this cookie value
                                    $(that).dialog('close');
                                    // $(this).dialog('close');
                                    }                                    
                                }, 1000);

                            }
                        }
                    }, 
                    {
                        id:"btn-export-sequence",
                        text: "Export Sequence",
                        click: function(e) {
                            e.preventDefault();
                            // for the case of Anonymous user
                            var that = this;
                            if (typeof returnValueArray[1] == 'undefined') {
                                returnValueArray[1] = '';
                            }

                            // declare cookie variable
                            var fileDownloadCheckTimer;
                            //use the current timestamp as the token value
                            var tokenValue = new Date().getTime(); 

                            // change dialog message depending on background or not
                            if (returnValueArray[3] == 'on') {
                                // pass user_name
                                showAdditionalDialog(returnValueArray[2],returnValueArray[3]);
                                $.ajax({
                                    async   : false,
                                    url     : Drupal.settings.baseUrl + '/' + Drupal.settings.vbsearch_md.basePath + '/vbsearch_md_exportSequence.php',
                                    // type    : 'POST',
                                    type    : 'GET',
                                    data    : { value0: returnValueArray[0], value1: returnValueArray[1], value2: tokenValue },
                                    success : function(resp) {
                                        // alert("Submitted !!!");
                                        // console.log(resp)

                                    },
                                    // error   : function(resp){
                                    //     //alert(JSON.stringify(resp));
                                    // }
                                });
                            } else {
                                showAdditionalDialog(returnValueArray[2],returnValueArray[3]);
                                window.location = Drupal.settings.baseUrl + '/' + Drupal.settings.vbsearch_md.basePath + 
                                                  '/vbsearch_md_exportSequence.php?value0=' + returnValueArray[0] + '&value1=' + returnValueArray[1] + '&value2=' + tokenValue;

                                // checking Cookie                  
                                fileDownloadCheckTimer = window.setInterval(function () {
                                var cookieValue = $.cookie('fileSequenceToken');
                                // console.log(cookieValue);
                                    if (cookieValue == tokenValue) {
                                    // finishDownload();
                                    window.clearInterval(fileDownloadCheckTimer);
                                    // $.removeCookie('fileDownloadToken'); //clears this cookie value
                                    $(that).dialog('close');
                                    // $(this).dialog('close');
                                    }                                    
                                }, 1000);

                            }
                        }
                    },
                    {
                        id:"btn-export-structure",
                        text: "Export STRUCTURE",
                        click: function(e) {
                            e.preventDefault();
                            // for the case of Anonymous user
                            var that = this;
                            if (typeof returnValueArray[1] == 'undefined') {
                                returnValueArray[1] = '';
                            }

                            // declare cookie variable
                            var fileDownloadCheckTimer;
                            //use the current timestamp as the token value
                            var tokenValue = new Date().getTime(); 

                            // change dialog message depending on background or not
                            if (returnValueArray[3] == 'on') {
                                // pass user_name
                                showAdditionalDialog(returnValueArray[2],returnValueArray[3]);
                                $.ajax({
                                    async   : false,
                                    url     : Drupal.settings.baseUrl + '/' + Drupal.settings.vbsearch_md.basePath + '/vbsearch_md_exportStructure.php',
                                    // type    : 'POST',
                                    type    : 'GET',
                                    data    : { value0: returnValueArray[0], value1: returnValueArray[1], value2: tokenValue },
                                    success : function(resp) {
                                        // alert("Submitted !!!");
                                        // console.log(resp)

                                    },
                                    // error   : function(resp){
                                    //     //alert(JSON.stringify(resp));
                                    // }
                                });
                            } else {
                                showAdditionalDialog(returnValueArray[2],returnValueArray[3]);
                                window.location = Drupal.settings.baseUrl + '/' + Drupal.settings.vbsearch_md.basePath + 
                                                 '/vbsearch_md_exportStructure.php?value0=' + returnValueArray[0] + '&value1=' + returnValueArray[1] + '&value2=' + tokenValue;

                                // checking Cookie                  
                                fileDownloadCheckTimer = window.setInterval(function () {
                                var cookieValue = $.cookie('fileStructureToken');
                                // console.log(cookieValue);
                                    if (cookieValue == tokenValue) {
                                    // finishDownload();
                                    window.clearInterval(fileDownloadCheckTimer);
                                    // $.removeCookie('fileDownloadToken'); //clears this cookie value
                                    $(that).dialog('close');
                                    // $(this).dialog('close');
                                    }                                    
                                }, 1000);

                            }

                        }
                    },
                    ]
                });

                $("#mdSubmissionDialog").html(`
                    Currently, this Export functionality offers Download (.csv), Sequence (.fa), and STRUCTURE format.
                    <br><br># Please note that Export buttons in the bottom are automatically activated/deactivated under the following criteria.
                    <ul>
                        <li>Single Domain and single Sub-domain should be selected.</li>
                        <li>List of Domains/Sub-domains for Sequence: Genome/Translation, Genome/Transcript, Genome/Gene, Proteome, Transcriptome</li>
                        <li>For STRUCTURE format, only Population Biology/Sample genotype is of interest with Genotype class = microsatellite</li>
                        <li>Login users: a) up to 300,000 results on the fly; b) background job over 300,000 results.</li>
                        <li>Anonymous users: only up to 5,000 results. Please login or <a href="/user/register" target="_blank">request a VectorBase account</a> to get more results of your query.</li>
                    </ul> 
                    `);

                // check the activation/deactivation of buttons and get the number of limits and uid in export
                var returnValueArray = Check_md_Status();

            } // end of function submitExport()

            function showAdditionalDialog(usernamevalue,background_process) {
                $('#mdSubmissionDialog').dialog({
                        autoOpen: false,
                        height: 320,
                        width: 485,
                        resizable: false,
                        modal: true,
                        title: "Exporting...",
                        // stack: true,

                    });

                //diable all buttons while exporting
                $('#btn-export-download').button('disable');
                $('#btn-export-sequence').button('disable');
                $('#btn-export-structure').button('disable');

                // change message in the dialog
                // $("#mdSubmissionDialog").html('<br><br><b>Please wait for a download dialog and then close this popup window after finishing the download</b><br><br>' + pinwheel);
                if( background_process == 'on') {
                    // $("#mdSubmissionDialog").html('<br><b>Export request was sent to a Background Job. <br><br>In general, output file will be ready within an hour and its Download link will be provided at "Export" under "Your jobs" in your Profile page, which the link is: <a href="' + Drupal.settings.baseUrl + '/users/' + usernamevalue + '/#collapse-Export" target="_blank">' + usernamevalue + ' Profile</a><br><br>Please close this popup window to go back to Search.</b><br><br>');
                    $("#mdSubmissionDialog").html('<br><b>Export request was sent to a Background Job. <br><br>' +
                        'In general, output file will be ready within a few minutes and its Download link will be provided at "Export" under "Your jobs" in your Profile page, which the link is: ' + 
                        '<i><a href="' + Drupal.settings.baseUrl + '/users/' + usernamevalue + '/#collapse-Export" target="_blank">' + usernamevalue + ' Profile</a></i><br>' +
                        '(revisit/reload the page once in a while to check the job status) <br><br>' +
                        'Please close this popup window to go back to Search.</b><br><br>');
                } else {
                    $("#mdSubmissionDialog").html('<br><br><b>This popup dialog will be automatically closed when the file downloading is ready</b><br><br>');
                }
                // $("#mdSubmissionDialog").html('<br><br><b>Please wait for a download dialog and then close this popup window after finishing the download</b><br><br>' + $('.imghideDK').show());            
                // $('.imghideDK').show();

            } // end of function showAdditionalDialog()

            function Check_md_Status() {

                //
                // Export Download
                //

                var results_count = document.getElementById('total_items').innerHTML.trim();
                // count rows of sub-domain table
                // domain-facet-left, sub-domain-facet-left -> Antelmo changed them to facet-domain, facet-sub-domain
                if (document.getElementById('facet-sub-domain')) {
                    var subdomain_count = document.getElementById('facet-sub-domain').rows.length;
                } else {
                    var subdomain_count = 0;
                }

                // console.log(subdomain_count);
                var thisElement = document.getElementById('myBtn_all');

                // retrieve information
                var valueSplit = document.getElementById('myBtn_all').value.split(';');
                // console.log(valueSplit);

                // if (document.getElementById('myBtn_all').value == "login") {
                if (valueSplit[0] == "login") {
                    var numlimit = 300000;
                } else {
                    var numlimit = 5000;
                }

                // check if back_process is done
                if (results_count > 300000) {
                    back_process = 'on';
                } else {
                    back_process = 'off';
                }

                // add additional condition to consider number of subdomain
                // note that it should be 2 if one subdomain exists only (additional <tr> exists for naming the "Sub-domain")
                if((results_count <= numlimit || numlimit == 300000) && subdomain_count == 2) {
                    //Enable the export download button - do nothing
                } else {
                    //Disable the export download button
                    $('#btn-export-download').button('disable');

                }

                //
                // Export Sequence & Structure
                //
                // var thisElement_seq = document.getElementById('myBtn_seq');
                var domain_count = document.getElementById('facet-domain').rows.length;    //# of <tr>
                // console.log(domain_count);   //10 because it includes <tr> within <thead>
                    // if((results_count <= numlimit_seq || numlimit_seq == -1) && domain_count == 2 && subdomain_count == 2){     //DKDK bug fix (was numlimit)
                    if((results_count <= numlimit || numlimit == 300000) && domain_count == 2 && subdomain_count == 2) {     //DKDK bug fix (was numlimit)                    
                        var domain_table = document.getElementById('facet-domain');
                        // changed from [0] to [1] as filter results changed
                        var domain_name = domain_table.getElementsByTagName('td')[1].innerHTML.trim(); //if domain_count==2, no <a> exists
                        // console.log(domain_name);
                        var subdomain_table = document.getElementById('facet-sub-domain');
                        // changed from [0] to [1] as filter results changed
                        var subdomain_name = subdomain_table.getElementsByTagName('td')[1].innerHTML.trim();
                        // console.log(subdomain_name);
                        // Export Sequence
                        if(domain_name == "Transcriptome" || domain_name == "Proteome") {
                            // Enable the export sequence button - do nothing
                        } else if (domain_name == "Genome" && (subdomain_name == "Transcript" || subdomain_name == "Gene" || subdomain_name == "Translation")) {
                            // Enable the export sequence button - do nothing
                        } else {
                            // Disable the export sequence button
                            $('#btn-export-sequence').button('disable');
                        }

                        // Export Structure
                        if(domain_name == "Population Biology" && subdomain_name == "Sample genotype") {
                            // Enable the export structure button - do nothing
                            // // change background process setting here for structure: 100000 not 300000
                            // if (results_count > 100000) {
                            //     back_process = 'off';
                            // } else {
                            //     back_process = 'off';
                            // }
                        } else {
                            // Disable the export structure button
                            $('#btn-export-structure').button('disable');
                        }
                    } else {
                        // Disable the export sequence and structure buttons
                        $('#btn-export-sequence').button('disable');
                        $('#btn-export-structure').button('disable');                        
                    }

                var returnValueArray = [];
                returnValueArray.push(numlimit);
                // user_id
                returnValueArray.push(valueSplit[1]);
                // user_name
                returnValueArray.push(valueSplit[2]);
                // back_process
                returnValueArray.push(back_process);  
                return returnValueArray;    

            }   // end of function Check_md_Status()

        }
    };

})(jQuery);