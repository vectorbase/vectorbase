<?php

    //
    // Step 2: Increase PHP time limits to 5 minutes.
    //

    // this may be required?
    // **** load drupal enviornment ****
    define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
    require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    chdir(DRUPAL_ROOT);

    //set time - unit in second
    $expire_time = 60 * 120;
    //server values: socket (60), execution (90), input (60), memory (2048M)

    // drupal_set_time_limit(300);
    // $oldTO = ini_get('default_socket_timeout');
    // ini_set('default_socket_timeout', $expire_time);
    $oldExTime = ini_get('max_execution_time');
    ini_set('max_execution_time', $expire_time);
    // $oldMemory = ini_get('memory_limit');
    // ini_set('memory_limit', '1024M');
    //for input only (e.g., upload video) thus do not need in this script
    // $oldInputTime = ini_get('max_input_time');
    // ini_set('max_input_time', 0);       // 0 = unlimited time

    // $temp_var1=ini_get('default_socket_timeout');
    $temp_var2=ini_get('max_execution_time');

    //get php argument through drush: md5 hash
    $php_arg = drush_shift();

    //retrieve fieldlist and solrQuery from db
    //fetchField (value), fetchAll (array object), fetchObject (object)
    $rowcount = db_query("SELECT md_totalnumber from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    $fieldlist = db_query("SELECT md_fieldlist from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    $solrQuery = db_query("SELECT md_solrquery from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();

    //get domain name and subdomain name
    $domainsubdomain = db_query("SELECT md_description from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    $domainsubdomain = explode(',', $domainsubdomain);
    $domain_name = str_replace(' ', '', $domainsubdomain[0]);
    $subdomain_name = str_replace(' ', '', $domainsubdomain[1]);

    //this actually means the total number of search results
    $rowcount = (int)$rowcount;

    // //unserialize
    $fieldlist = unserialize($fieldlist);
    $solrQuery = unserialize(base64_decode($solrQuery));

    // //open file - possibly use DRUPAL_ROOT here?
    // $savePath = '/home/testdown/temp1/';
    $savePath = DRUPAL_ROOT . '/data/job_results/export/';
    $myfile = fopen("$savePath$php_arg.csv", "w");

    // Holds the CSV for each data line
    $solrCSV = "";
    // Holds the CSV header line
    $header = "";
    // Temporary storage for the key values pairs returned by Solr.
    // Required to preserve the order of the columns, as Solr does not
    // maintain column order from row to row for some reason.
    $dataArray = array();

    //DKDK header fix?
    $header_new = "";
    foreach ($fieldlist as $fieldvalue){
        $header_new = $header_new . $fieldvalue . ",";
    }
    $solrCSV = $header_new . "\r\n";   //DKDK
    // $solrCSV = $header . "\n";   //DKDK
    ob_clean();    //DKDK delete output buffer before starting to record
    //write header of the csv file
    fwrite($myfile, $solrCSV);
    // print($solrCSV);
    $solrCSV = '';

    //set initial counts - Genome/Gene somehow requires less number than normal
    $startcount = 0;
    if ($domain_name == 'Genome' && $subdomain_name == 'Gene') {
        $rowcount_once = 27000;
    } else {
        $rowcount_once = 50000;
    }

    //set the number of loops
    $loop_count = ceil($rowcount/$rowcount_once);

    // if($rowcount_once > $rowcount)  {
    //     $loop_count = intval($rowcount/$rowcount_once);
    // } else {
    //     $loop_count = intval($rowcount/$rowcount_once) + 1;
    // }
    //below is original
    // if($rowcount_once > $rowcount)  {
    //     $loop_count = intval($rowcount/$rowcount_once) + 1;
    // } else {
    //     $loop_count = intval($rowcount/$rowcount_once);
    // }
    //Execute the query
    try {
        //Make a loop for dealing with massive download?
        for ($iii = 0; $iii < $loop_count; $iii++) {
            $solrQuery->replaceParam('start', $startcount);
            $solrQuery->replaceParam('rows', $rowcount_once);
            $solrResults = $solrQuery->search();
            $docs = $solrResults->response->docs;      //DKDK this remains the same with previous one
            if (empty($docs) != True) {
                // Loop through each document and its fields, printing each line out to http.
                foreach ($docs as $doc){
                    foreach ($doc as $key => $value){
                        if (!is_array($value)){
                            // If not an array, assign the value to the corresponding element of dataArray
                            // DKDK clean function removes non-ascii characters; VB-7801 Comma case - fix a bug for mb_check_encoding() with !
                            if (!mb_check_encoding($value,'ASCII')) {
                                $value = str_replace(array("\n", "\r"), ' ', $value);
                                $dataArray[$key] = clean($value);
                            } else {
                                // $value = chr(255) . chr(254) . mb_convert_encoding($value, 'UTF-16LE', 'UTF-8');    //DKDK for Excel (Win & OSX) mmm does not work properly
                                // $dataArray[$key] = $value;
                                $value = str_replace(array("\n", "\r"), ' ', $value);
                                // DKDK VB-7801 Comma case
                                if (strpos($value, ',') !== false) {
                                    $dataArray[$key] = '"' . $value . '"';
                                } else {
                                    $dataArray[$key] = $value;
                                }
                                // DKDK need to remove comma for single value
                                // $dataArray[$key] = str_replace(',', '' , $value);
                            }
                        } else {
                            // Loop through the array and build a string in the corresponding element of dataArray
                            foreach ($value as $element) {
                                // Concatenates the array element to the dataArray value
                                // DKDK clean function removes non-ascii characters; VB-7801 Comma case - fix a bug for mb_check_encoding() with !
                                if (!mb_check_encoding($element,'ASCII')) {
                                    $element = str_replace(array("\n", "\r"), ' ', $element);
                                    $dataArray[$key] = $dataArray[$key] . clean($element) . ";";
                                } else {
                                    // $element = chr(255) . chr(254) . mb_convert_encoding($element, 'UTF-16LE', 'UTF-8');    //DKDK for Excel (Win & OSX) mmm does not work properly
                                    // $dataArray[$key] = $dataArray[$key] . $element . ";";
                                    $element = str_replace(array("\n", "\r"), ' ', $element);
                                    // DKDK need to remove comma for single value
                                    $dataArray[$key] = $dataArray[$key] . str_replace(',', '' , $element) . ";";
                                }
                            }
                            // DKDK VB-7801 remove trailing semi-colon for multiple values
                            $dataArray[$key] = rtrim($dataArray[$key], ';');
                        }
                    }

                    // Loop through dataArray and build the CSV string. Clear dataArray elements as they are used.
                    // This preserves column order from row to row.
                    for ($i = 0; $i < count($fieldlist); $i++) {
                        $solrCSV = $solrCSV . $dataArray[$fieldlist[$i]] . ",";
                        $dataArray[$fieldlist[$i]] = '';
                    }

                    // Add carriage return and line feed.
                    $solrCSV = $solrCSV . "\r\n";
                    // Print resulting CSV line
                    // // DKDK for checking/printing variables
                    // $solrCSV = '';  //DKDK
                    // $solrCSV = var_dump($solrQueryTemp);     //DKDK for checking $solrQuery
                    // $solrCSV = var_dump($domain_subdomain);     //DKDK for checking $solrQuery
                    // $solrCSV = var_dump($solrQuery);     //DKDK for checking $solrQuery
                    print($solrCSV); //DKDK
                    fwrite($myfile, $solrCSV);
                    // print(chr(255) . chr(254) . mb_convert_encoding($solrCSV, 'UTF-16LE', 'UTF-8')); //DKDK mmm does not work properly
                    // Clear the CSV line for the next set of data
                    $solrCSV = '';
                }
                $startcount = $startcount + $rowcount_once;  //Increase the startcount. No need to have +1 as $startcount starts from 0, e.g., 1000 data from 0 means 0~999
                $docs='';
                // //DKDK for checking purpose - temporary
                //     fwrite($myfile, $loop_count);
            } else {
                break;
            }
        }
        fclose($myfile);
        //change md_status at db table to be 1 after finishing this work
        db_update('vbsearch_md_background')->fields(array('md_status' => 1))->condition('md_md5hash', $php_arg, '=')->execute();
    }
    catch (Exception $e) {
        fclose($myfile);
        //change md_status at db table to be 999 if something wrong
        db_update('vbsearch_md_background')->fields(array('md_status' => 999))->condition('md_md5hash', $php_arg, '=')->execute();
        if($e->getCode() == 0) {
            drupal_goto('vbsearch/download/timeout');
            return;
        }
    }

    // Reset timeout to previous value
    // ini_set('default_socket_timeout', $oldTO);
    ini_set('max_execution_time', $oldExTime);
    // ini_set('memory_limit', $oldMemory);
    // ini_set('max_input_time', $oldInputTime);

    // fclose($myfile);

    // //change md_status at db table to be 1 after finishing this work
    // db_update('vbsearch_md_background')->fields(array('md_status' => 1))->condition('md_md5hash', $php_arg, '=')->execute();

function clean($string){
    return preg_replace('/[^A-Za-z0-9\-\." "\/\=\?\_\:\%]/', ' ', $string); // Removes special chars.
}


?>