<?php

    //
    // Step 2: Increase PHP time limits to 5 minutes.
    //

    // this may be required?
    // **** load drupal enviornment ****
    define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
    require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    chdir(DRUPAL_ROOT);

    //set time - unit in second
    $expire_time = 60 * 120;
    //server values: socket (60), execution (90), input (60), memory (2048M)

    // drupal_set_time_limit(300);
    // $oldTO = ini_get('default_socket_timeout');
    // ini_set('default_socket_timeout', $expire_time);
    $oldExTime = ini_get('max_execution_time');
    ini_set('max_execution_time', $expire_time);
    // $oldMemory = ini_get('memory_limit');
    // ini_set('memory_limit', '1024M');
    //for input only (e.g., upload video) thus do not need in this script
    // $oldInputTime = ini_get('max_input_time');
    // ini_set('max_input_time', 0);       // 0 = unlimited time

    // $temp_var1=ini_get('default_socket_timeout');
    $temp_var2=ini_get('max_execution_time');

    //get php argument through drush: md5 hash
    $php_arg = drush_shift();

    //retrieve fieldlist and solrQuery from db
    //fetchField (value), fetchAll (array object), fetchObject (object)
    $rowcount = db_query("SELECT md_totalnumber from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    // $fieldlist = db_query("SELECT md_fieldlist from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    $solrQuery = db_query("SELECT md_solrquery from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    //retrieve domain and subdomain for if-else below
    $domainsubdomain = db_query("SELECT md_description from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    $domainsubdomain = explode(',', $domainsubdomain);
    $domain_name = str_replace(' ', '', $domainsubdomain[0]);
    $subdomain_name = str_replace(' ', '', $domainsubdomain[1]);


    //this actually means the total number of search results
    $rowcount = (int)$rowcount;

    // //unserialize
    // $fieldlist = unserialize($fieldlist);
    $solrQuery = unserialize(base64_decode($solrQuery));

    // //open file
    // $savePath = '/home/testdown/temp1/';
    $savePath = DRUPAL_ROOT . '/data/job_results/export/';
    $myfile = fopen("$savePath$php_arg.fa", "w");

    // //move to here
    // $solrQuery->replaceParam('rows', $rowcount);

    // Step 5: Process the results, creating text file (.seq)
    // Holds the file for each data line
    $solrCSV = "";
    // Holds the file header line
    $header = "";
    // Temporary storage for the key values pairs returned by Solr.
    // Required to preserve the order of the columns, as Solr does not
    // maintain column order from row to row for some reason.
    $dataArray = array();
    // Get the array of docs
    // $docs = $solrResults->response->docs;

    ob_clean();    //DKDK delete output buffer before starting to record
    //set initial counts
    $startcount = 0;
    $rowcount_once = 50000;

    //set the number of loops
    $loop_count = ceil($rowcount/$rowcount_once);

    // //set the number of loops
    // if($rowcount_once > $rowcount)  {
    //     $loop_count = intval($rowcount/$rowcount_once) + 1;
    // } else {
    //     $loop_count = intval($rowcount/$rowcount_once);
    // }

    //Execute the query
    try {
        //Make a loop for dealing with massive download?
        for ($iii = 0; $iii < $loop_count; $iii++) {
            $solrQuery->replaceParam('start', $startcount);
            $solrQuery->replaceParam('rows', $rowcount_once);
            $solrResults = $solrQuery->search();
            $docs = $solrResults->response->docs;      //DKDK this remains the same with previous one
            if (empty($docs) != True) {
                foreach ($docs as $doc){
                    //DKDK below works but two issues considered
                    // a) Genome does not always have data - checking subdomain and implemented at md.js
                    // b) depending on sub-domain, several cases exist: sequence_cds or sequence_aa, both, or none
                    if ($domain_name == "Genome") {
                        if( (($subdomain_name == "Translation") || ($subdomain_name == "Gene") && ($doc->sequence_aa != "")) ) {
                            $solrCSV = ">" . $doc->accession . "\r\n" . $doc->sequence_aa . "\r\n\r\n";
                            // print($solrCSV); //DKDK
                            // fwrite($myfile, $solrCSV);
                            fwrite($myfile, $solrCSV);
                        } elseif ($subdomain_name == "Transcript" && ($doc->sequence_cds != "")) {
                            $solrCSV = ">" . $doc->accession . "\r\n" . $doc->sequence_cds . "\r\n\r\n";
                            // print($solrCSV); //DKDK
                            // fwrite($myfile, $solrCSV);
                            fwrite($myfile, $solrCSV);
                        }
                    } elseif ($doc->sequence_s != "") {
                        $solrCSV = ">" . $doc->accession . "\r\n" . $doc->sequence_s . "\r\n\r\n";
                        // print($solrCSV); //DKDK
                        fwrite($myfile, $solrCSV);
                    }
                }
                $startcount = $startcount + $rowcount_once;  //Increase the startcount. No need to have +1 as $startcount starts from 0, e.g., 1000 data from 0 means 0~999
                $docs='';   // foreach ($docs as $doc)
            } else {
                break;
            }
        }
        fclose($myfile);
        //change md_status at db table to be 1 after finishing this work
        db_update('vbsearch_md_background')->fields(array('md_status' => 1))->condition('md_md5hash', $php_arg, '=')->execute();
    }
    catch (Exception $e) {
        fclose($myfile);
        //change md_status at db table to be 999 if something wrong
        db_update('vbsearch_md_background')->fields(array('md_status' => 999))->condition('md_md5hash', $php_arg, '=')->execute();

        if($e->getCode() == 0) {
            drupal_goto('vbsearch/download/timeout');
            return;
        }
    }

    // Reset timeout to previous value
    // ini_set('default_socket_timeout', $oldTO);
    ini_set('max_execution_time', $oldExTime);
    // ini_set('memory_limit', $oldMemory);
    // ini_set('max_input_time', $oldInputTime);

    // fclose($myfile);

    // //change md_status at db table to be 1 after finishing this work
    // db_update('vbsearch_md_background')->fields(array('md_status' => 1))->condition('md_md5hash', $php_arg, '=')->execute();

// function clean($string){
//     return preg_replace('/[^A-Za-z0-9\-\." "\/\=\?\_\:\%]/', ' ', $string); // Removes special chars.
// }

?>