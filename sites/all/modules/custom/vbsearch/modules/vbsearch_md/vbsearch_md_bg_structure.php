<?php

    //
    // Step 2: Increase PHP time limits to 5 minutes.
    //

    // this may be required?
    // **** load drupal enviornment ****
    define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
    require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    chdir(DRUPAL_ROOT);

    //set time - unit in second
    $expire_time = 60 * 120;
    //server values: socket (60), execution (90), input (60), memory (2048M)

    // these do not work
    // $oldHttp = variable_get('http_request_timeout');
    // variable_set('http_request_timeout', $expire_time);
    // $newHttp = variable_get('http_request_timeout');
    // $oldHttp = variable_get('apachesolr_connection_timeout');
    // variable_set('apachesolr_connection_timeout', 600);
    // $newHttp = variable_get('apachesolr_connection_timeout');

    drupal_set_time_limit(600);
    // $oldTO = ini_get('default_socket_timeout');
    // ini_set('default_socket_timeout', $expire_time);
    $oldExTime = ini_get('max_execution_time');
    ini_set('max_execution_time', $expire_time);
    // $oldMemory = ini_get('memory_limit');
    // ini_set('memory_limit', '1024M');
    //for input only (e.g., upload video) thus do not need in this script
    // $oldInputTime = ini_get('max_input_time');
    // ini_set('max_input_time', 0);       // 0 = unlimited time

    // $temp_var1=ini_get('default_socket_timeout');
    $temp_var2=ini_get('max_execution_time');

    //get php argument through drush: md5 hash
    $php_arg = drush_shift();

    //retrieve fieldlist and solrQuery from db
    //fetchField (value), fetchAll (array object), fetchObject (object)
    $rowcount = db_query("SELECT md_totalnumber from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    $fieldlist_STRUCTURE = db_query("SELECT md_fieldlist from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    $solrQuery = db_query("SELECT md_solrquery from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();

    //get domain name and subdomain name
    $domainsubdomain = db_query("SELECT md_description from vbsearch_md_background where md_md5hash = :md_md5hash", array(":md_md5hash" => $php_arg))->fetchField();
    $domainsubdomain = explode(',', $domainsubdomain);
    $domain_name = str_replace(' ', '', $domainsubdomain[0]);
    $subdomain_name = str_replace(' ', '', $domainsubdomain[1]);

    // //unserialize
    $fieldlist_STRUCTURE = unserialize($fieldlist_STRUCTURE);
    $solrQuery = unserialize(base64_decode($solrQuery));

    //this actually means the total number of search results
    $rowcount = (int)$rowcount;

    // // temporarily
    // $rowcount = 150000;
    // $solrQuery->replaceParam('rows', $rowcount);

    //set initial counts
    $startcount = 0;
    $rowcount_once = 50000;

    //set the number of loops and initialize $docs
    $loop_count = ceil($rowcount/$rowcount_once);
    $docs = array();

    // //open file - possibly use DRUPAL_ROOT here?
    // $savePath = '/home/testdown/temp1/';
    $savePath = DRUPAL_ROOT . '/data/job_results/export/';
    $myfile = fopen("$savePath$php_arg.csv", "w");

    try {
        // Make a loop for dealing with massive download and to avoid ApacheSolr timeout issue
        for ($iii = 0; $iii < $loop_count; $iii++) {
            $solrQuery->replaceParam('start', $startcount);
            $solrQuery->replaceParam('rows', $rowcount_once);
            $solrResults = $solrQuery->search();
            // convert PHP object to PHP Array
            $jsonfile = json_decode(json_encode($solrResults), true);
            $docs_temp = $jsonfile['response']['docs'];
            if (empty($docs_temp) != True) {
                // merge array
                $docs = array_merge($docs,$docs_temp);
            }
            $jsonfile = '';
            $docs_temp = '';
            //Increase the startcount. No need to have +1 as $startcount starts from 0, e.g., 1000 data from 0 means 0~999
            $startcount = $startcount + $rowcount_once;
        }

            // // // // checking error!
            // // // // //open file - possibly use DRUPAL_ROOT here?
            // $savePath_ct = '/home/testdown/temp1/';
            // $php_arg_ct = 'dk_error1_count';
            // $myfile_ct = fopen("$savePath_ct$php_arg_ct.log", "w");
            // // $solrCSV = $command;
            // fwrite($myfile_ct, count($docs));
            // fclose($myfile_ct);

            // // temporarily
            // $savePath_docs = '/home/testdown/temp1/';
            // $php_arg_docs = 'dk_error1_bg_docs';
            // $myfile_docs = fopen("$savePath_docs$php_arg_docs.log", "w");
            // fwrite($myfile_docs, print_r($docs,true));
            // fclose($myfile_docs);


        $fieldlist = array();
        $idlist = array();
        for ($i = 0;$i < count($docs); $i++) {
            $fieldlist[$i] = $docs[$i]['genotype_name_s'];
            $idlist[$i] = $docs[$i]['sample_id_s'];
            //DKDK VB-7133 adding new field - adding country_s & species too
            $namelist[$i] = $docs[$i]['sample_name_s'];
            $countrylist[$i] = $docs[$i]['country_s'];
            $specieslist[$i] = $docs[$i]['species'];
        }

        // get unique values
        $fieldlist = array_values(array_unique($fieldlist));
        //DKDK VB-7133 adding new field - adding country_s & species too
        $idlistUniqueIndex = array_unique($idlist);
        $idlist_unique = array_values($idlistUniqueIndex);
        $uniqueIndex = array_keys($idlistUniqueIndex);
        if (empty($docs) != True) {
            for ($i = 0;$i < count($idlistUniqueIndex); $i++) {
                $namelistUnique[$i] = $namelist[$uniqueIndex[$i]];
                $countrylistUnique[$i] = $countrylist[$uniqueIndex[$i]];
                $specieslistUnique[$i] = $specieslist[$uniqueIndex[$i]];
            }
        }

        // header
        $solrCSV = '';
        // header starts with ID,
        //DKDK VB-7133 adding new field - adding country_s & species too
        $header_new = 'ID,Species,Sample label,Country,';
        foreach ($fieldlist as $fieldvalue) {
            $header_new = $header_new . $fieldvalue . ",";
        }
        // remove trailing comma for header
        $header_new = rtrim($header_new,",");
        $solrCSV = $header_new . "\r\n";
        ob_clean();    //DKDK delete output buffer before starting to record
        fwrite($myfile, $solrCSV);
        // echo "<br><br>";
        $solrCSV = '';

        // count each idlist: output is an associative array, sample_id_s => the number of the sample_id_s.
        $key_num = array_count_values($idlist);

        // initiate $inc_num for looping through partial docs & $solrCSV_even & $solrCSV_odd
        $incr_num = 0;
        if (empty($docs) != True) {
            for ($i = 0;$i < count($idlist_unique); $i++) {
                // may need to handle single exception, VBS0104008 which has only 23 data???
                //DKDK VB-7133 adding new field - adding country_s & species too: species is array so need to indicate index!
                $solrCSV_init = $idlist_unique[$i] . ',' . $specieslistUnique[$i][0] . ',' . $namelistUnique[$i] . ',' . $countrylistUnique[$i] . ',';
                // initialize array with default values: in this manner, no need to check empty element nor assign value for those empty elements
                $solrCSV_even = array_fill(0,count($fieldlist),'-9');
                $solrCSV_odd  = array_fill(0,count($fieldlist),'-9');
                // print_r(count($solrCSV_even));

                // change for loop range so that only partial $doc is checked per each $idlist_unique
                for ($j = 0 + $incr_num;$j < $key_num[$idlist_unique[$i]] + $incr_num; $j++) {
                    if ($j % 2 == 0) {
                        // check genotype_name_s exists
                        if ($docs[$j]['genotype_name_s']) {
                            // check if the key value, genotype_name_s, exists in the fieldlist and get index (return key)
                            $indexnum = array_search($docs[$j]['genotype_name_s'],$fieldlist);
                            // if index exists - well always exists, so no need to check
                            $solrCSV_even[$indexnum] = (string)$docs[$j]['genotype_microsatellite_length_i'];
                        }
                    } else {
                        // check genotype_name_s exists
                        if ($docs[$j]['genotype_name_s']) {
                            // check if the key value, genotype_name_s, exists in the fieldlist and get index (return key)
                            $indexnum = array_search($docs[$j]['genotype_name_s'],$fieldlist);
                            // if $indexnum exists - well always exists, so no need to check
                            $solrCSV_odd[$indexnum] = (string)$docs[$j]['genotype_microsatellite_length_i'];
                        }
                    }
                    // echo $j;
                }
                // convert array to string with comma
                $solrCSV_even_out = implode(',', $solrCSV_even);
                $solrCSV_odd_out  = implode(',', $solrCSV_odd);

                // print to $solrCSV
                // ob_clean();
                $solrCSV = $solrCSV_init . $solrCSV_even_out . "\r\n";
                // echo "<br>";
                fwrite($myfile, $solrCSV);
                $solrCSV = $solrCSV_init . $solrCSV_odd_out  . "\r\n";
                // echo "<br>";
                // ob_clean();
                fwrite($myfile, $solrCSV);
                // echo "<br>";

                $solrCSV = '';

                // go to next $docs to get data
                $incr_num = $incr_num + $key_num[$idlist_unique[$i]];

                // echo "<br><br>";
                // var_dump($solrCSV_even);
                // echo "<br><br>";
                // var_dump($solrCSV_odd);
                // echo "<br><br>";
                // var_dump($solrCSV_even_out);
                // echo "<br><br>";
                // var_dump($solrCSV_odd_out);
                // echo "<br><br>";
                // var_dump($solrCSV);
                // echo "<br><br>";
            }
        } else {
            // the case of no results: actually no need but just in case
            $solrCSV = 'No results are found';
            fwrite($myfile, $solrCSV);
            $solrCSV = '';
        }
        fclose($myfile);
        //change md_status at db table to be 1 after finishing this work
        db_update('vbsearch_md_background')->fields(array('md_status' => 1))->condition('md_md5hash', $php_arg, '=')->execute();

    }
    catch (Exception $e) {
        fclose($myfile);
        // change md_status at db table to be 999 if something wrong
        db_update('vbsearch_md_background')->fields(array('md_status' => 999))->condition('md_md5hash', $php_arg, '=')->execute();

        // temporarily
        // $savePath_bg = '/home/testdown/temp1/';
        // $php_arg_bg = 'dk_error1_bgERROR';
        // $myfile_bg = fopen("$savePath_bg$php_arg_bg.log", "w");
        // fwrite($myfile_bg, $e);
        // fclose($myfile_bg);

        if($e->getCode() == 0) {
            drupal_goto('vbsearch/download/timeout');
            return;
        }
    }

    // Reset timeout to previous value
    // ini_set('default_socket_timeout', $oldTO);
    ini_set('max_execution_time', $oldExTime);
    // ini_set('memory_limit', $oldMemory);
    // ini_set('max_input_time', $oldInputTime);

?>