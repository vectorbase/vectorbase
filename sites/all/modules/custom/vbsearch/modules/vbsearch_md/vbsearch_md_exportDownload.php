<?php

// **** load drupal enviornment ****
define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
chdir(DRUPAL_ROOT);

// need to get these values via GET
$rowcount = $_GET['value0'];
$user_id  = $_GET['value1'];
//cookie
$downloadTokenValue = $_GET['value2'];

// run the main download function & cookie
vbsearch_md_return_data($rowcount,$user_id,$downloadTokenValue);

// main download function & cookie
function vbsearch_md_return_data($rowcount,$user_id,$downloadTokenValue){
    // Production code

    // test if this works - this does not work as this function will be beyond drupal module in the new export
        // global $user;
        // $user_name = $user->name;
        // $user_id = $user->uid;
    // thus, a bypass is to get the value from JS
    if (empty($user_id) != True) {
        $user_id = (int)$user_id;
        $user = user_load($user_id);
        $user_name = $user->name;
    }

    //
    //Step0: get url information, parsing
    //
    // $request_url=$_SERVER['REQUEST_URI'];    //this does not work because vbsearch_md_return_data() is running under /vbsearch/download/
    $url_referer = parse_url($_SERVER['HTTP_REFERER']);
    if ($url_referer['query'] == '') {
        $request_url = $url_referer['path'];
    } else {
        $request_url = $url_referer['path'] . '?' . $url_referer['query'];
    }

    //sometimes encoding characters are included so let's decode them: e.g., %2522PMID%253A22346749%2522
    $request_url = urldecode($request_url);
    // $request_url = str_replace('%2522', '%22', $request_url);
    // $request_url = str_replace('%253A', '%3A', $request_url);
    // $request_url = str_replace('%2520', '%20', $request_url);
    // $request_url = str_replace('%252A', '%2A', $request_url);

    //Start url parsing - sometimes, especially after export, the url do not have ?& -> it is converted to ? (before: ?&, after: ?)
    // DKDK VB-7801 add limit to 4 to handle slash in some species, e.g., Anopheles daciae/messeae group (Brugman et al.) 
    $explode_slash=explode('/',$request_url,4);
    if(strpos($explode_slash[3], '?&') !== false) {
        $explode_qand=explode('?&',$explode_slash[3]);
    } else if (strpos($explode_slash[3], '?') !== false) {
        $explode_qand=explode('?',$explode_slash[3]);
    } else {
        $explode_qand=explode('?&',$explode_slash[3]);
    }

    // //just in case replace " into %22 if exist. And, (space) to %20 - for query term like "expression browser" - for curl
    // $explode_qand[0] = str_replace('"', '%22', $explode_qand[0]);
    // $explode_qand[0] = str_replace(' ', '%20', $explode_qand[0]);

    //For $solrQuery, decode q=. $explode_qand[0] = search term, q= & $explode_qand[1] = for fq=
    $explode_qand[0] = urldecode($explode_qand[0]);

    //
    //Step1: construct $solrQuery
    //

    //construct initial $solrQuery
    // $query_init = '';
    $query_init = $explode_qand[0];     // use here instead of replacing q parameter to accept file search
    $solr_url = vbsearch_find_solr_url();
    $queryParameters = array();

    //make initial $solrQuery
    $solrQuery = vbsearch_createSolrQuery($query_init, $queryParameters['qf'], $queryParameters['fl'], 1, $solr_url);

    // functions are changed to use instance
    //using $_GET made by url parsing
    parse_str($explode_qand[1], $_GET);

    // //set $vbsearch_instance if search_id exists
    // if (isset($_GET['search_id'])) {
    //   //Get the list of fields from search so we can acceess their values
    //   $vbsearch_instance = vbsearch_load($_GET['search_id']);
    // } else {
    //   //Initializing field list so we can pass it as a variable
    //   $vbsearch_instance = array();
    // }
    // //make complete $solrQuery including qf
    // $solrQuery = vbsearch_setup_query($solrQuery,$vbsearch_instance);   //changed

    // //set basic fq - domain, sub-domain, species etc.
    // $solrQuery->removeParam('fq');
    // $solrQuery = vbsearch_add_facet_parameters($solrQuery);

    // //set advanced fq - advanced search regarding search_id
    // $solrQuery = vbsearch_add_advanced_parameters($solrQuery,$vbsearch_instance);   //changed

    //DKDK replace above to single command
    $solrQuery = vbsearch_apachesolr_query_alter($solrQuery);
    $solrQuery = vbsearch_facets_apachesolr_query_alter($solrQuery);
    $solrQueryTemp = $solrQuery;    // simply for test purpose

    //Add q= and remove unnecessary query parameters
    // $solrQuery->replaceParam('q', $explode_qand[0]); //DKDK this is not necessary to be here. Similar one is moved to $quuery_init
    $solrQuery->removeParam('bq');
    $solrQuery->removeParam('boost');

    //Replace parameters for query once (to get site and bundle_name)
    $solrQuery->replaceParam('rows', 1);    //DKDK set rows 1 to get only one result
    $solrQuery->replaceParam('hl', 'false');
    $solrQuery->replaceParam('facet', 'false');
    $solrQuery->replaceParam('fl',array('site','bundle_name'));

    //
    // Step 2: Increase PHP time limits to 5 minutes.
    //
    // drupal_set_time_limit(300);
    // $oldTO = ini_get('default_socket_timeout');
    // ini_set('default_socket_timeout', 300);
    // $oldExTime = ini_get('max_execution_time');
    // ini_set('max_execution_time', 300);
    // $oldMemory = ini_get('memory_limit');
    // ini_set('memory_limit', '1024M');
    // $oldInputTime = ini_get('max_input_time');
    // ini_set('max_input_time', 0);       // 0 = unlimited time


    //Step 3: query for single data to get domain, sub-domain information from the result
    try {
        $solrResults_once = $solrQuery->search();
    }
    catch (Exception $e) {
        if($e->getCode() == 0) {
            drupal_goto('vbsearch/download/timeout');
            return;
        }
    }

    $docs_once = $solrResults_once->response->docs;
    $domain_subdomain = array('site:"'.$docs_once[0]->site.'"', 'bundle_name:"'.$docs_once[0]->bundle_name.'"');
    //for background process
    $md_description = $docs_once[0]->site . ', ' . $docs_once[0]->bundle_name;

    //
    // Step 4: actual query for export
    //
    // Set the download count depending on login status.
    // if (user_is_logged_in()){
    //     $rowcount = 300000;
    // }else{
    //     $rowcount = 5000;
    // }

    // need to use this GET value instead of finding it via login or not
    $rowcount = (int)$rowcount;

    // check total number of available data
    $results_count = $solrResults_once->response->numFound;

    //Query all
    // $solrQuery->replaceParam('rows', $rowcount);
    $solrQuery->replaceParam('hl', 'false');
    $solrQuery->replaceParam('facet', 'false');
    // $solrQuery->replaceParam('fl', getFieldset($fqarray));  //DKDK using fl restricts results which become inconsistent labels for search cases
    $fieldlist = getFieldset($domain_subdomain);
    $solrQuery->replaceParam('fl', $fieldlist);  //DKDK use new variable to have domain & subdomain here


    // if total data <= rowcount or not logged in, use previous approach
    if ($results_count <= $rowcount || $rowcount == 5000) {

        //move below here
        $solrQuery->replaceParam('rows', $rowcount);

        // Step 5: Process the results, creating csv
        // Holds the CSV for each data line
        $solrCSV = "";
        // Holds the CSV header line
        $header = "";
        // Temporary storage for the key values pairs returned by Solr.
        // Required to preserve the order of the columns, as Solr does not
        // maintain column order from row to row for some reason.
        $dataArray = array();

        // Set filename with timestamp - Eastern Daylight Saving Time
        date_default_timezone_set('EST5EDT');
        $filename = 'download_' . date('omdHis') . '.csv';

        //Set the document headers
            header('Content-Encoding: UTF-8');  //DKDK
            header("Content-type: text/csv; charset=UTF-8");   //DKDK
            // header('Content-Disposition: attachment; filename="download.csv"');
            header('Content-Disposition: attachment; filename=' . $filename);            
            // header("Content-Transfer-Encoding: ascii");  //DKDK
            header('Expires: 0');   //DKDK
            // header("Expires: Mon, 01 Jan 1990 05:00:00 GMT");   //DKDK
            header('Pragma: no-cache');
            // cookie
            setcookie("fileDownloadToken", $downloadTokenValue, 0, '/');

        //DKDK header fix?
        $header_new = "";
        foreach ($fieldlist as $fieldvalue){
            $header_new = $header_new . $fieldvalue . ",";
        }
        $solrCSV = $header_new . "\r\n";   //DKDK
        // $solrCSV = $header . "\n";   //DKDK
        ob_clean();    //DKDK delete output buffer before starting to record
        print($solrCSV);
        $solrCSV = '';

        //set initial counts
        $startcount = 0;
        $rowcount_once = 50000;

        //set the number of loops
        $loop_count = ceil($rowcount/$rowcount_once);

        // //set the number of loops
        // if($rowcount_once > $rowcount)  {
        //     $loop_count = intval($rowcount/$rowcount_once) + 1;
        // } else {
        //     $loop_count = intval($rowcount/$rowcount_once);
        // }

        //Execute the query
        try {
            //Make a loop for dealing with massive download?
            for ($iii = 0; $iii < $loop_count; $iii++) {
                $solrQuery->replaceParam('start', $startcount);
                $solrQuery->replaceParam('rows', $rowcount_once);
                $solrResults = $solrQuery->search();
                $docs = $solrResults->response->docs;      //DKDK this remains the same with previous one
                if (empty($docs) != True) {
                    // Loop through each document and its fields, printing each line out to http.
                    foreach ($docs as $doc){
                        foreach ($doc as $key => $value){
                            if (!is_array($value)){
                                // If not an array, assign the value to the corresponding element of dataArray
                                //DKDK clean function removes non-ascii characters; VB-7801 Comma case - fix a bug for mb_check_encoding() with !
                                if (!mb_check_encoding($value,'ASCII')) {
                                    $value = str_replace(array("\n", "\r"), ' ', $value);
                                    $dataArray[$key] = clean($value);
                                } else {
                                    // $value = chr(255) . chr(254) . mb_convert_encoding($value, 'UTF-16LE', 'UTF-8');    //DKDK for Excel (Win & OSX) mmm does not work properly
                                    // $dataArray[$key] = $value;
                                    $value = str_replace(array("\n", "\r"), ' ', $value);
                                    // DKDK VB-7801 Comma case
                                    if (strpos($value, ',') !== false) {
                                        $dataArray[$key] = '"' . $value . '"';
                                    } else {
                                        $dataArray[$key] = $value;
                                    }
                                    // $dataArray[$key] = str_replace(',', '' , $value);   //DKDK need to remove comma for single value
                                }
                            } else {
                                // Loop through the array and build a string in the corresponding element of dataArray
                                foreach ($value as $element) {
                                    // Concatenates the array element to the dataArray value
                                    // DKDK clean function removes non-ascii characters; VB-7801 Comma case - fix a bug for mb_check_encoding() with !
                                    if (!mb_check_encoding($element,'ASCII')) {
                                        $element = str_replace(array("\n", "\r"), ' ', $element);
                                        $dataArray[$key] = $dataArray[$key] . clean($element) . ';';
                                    } else {
                                        // $element = chr(255) . chr(254) . mb_convert_encoding($element, 'UTF-16LE', 'UTF-8');    //DKDK for Excel (Win & OSX) mmm does not work properly
                                        // $dataArray[$key] = $dataArray[$key] . $element . ";";
                                        $element = str_replace(array("\n", "\r"), ' ', $element);
                                        // DKDK need to remove comma for single value
                                        $dataArray[$key] = $dataArray[$key] . str_replace(',', '' , $element) . ";";
                                    }
                                }
                                // DKDK VB-7801 remove trailing semi-colon for multiple values
                                $dataArray[$key] = rtrim($dataArray[$key], ';');
                            }
                        }

                        // Loop through dataArray and build the CSV string. Clear dataArray elements as they are used.
                        // This preserves column order from row to row.
                        for ($i = 0; $i < count($fieldlist); $i++) {
                            $solrCSV = $solrCSV . $dataArray[$fieldlist[$i]] . ",";
                            $dataArray[$fieldlist[$i]] = '';
                        }

                        // Add carriage return and line feed.
                        $solrCSV = $solrCSV . "\r\n";
                        // Print resulting CSV line
                        // // for checking/printing variables
                        // $solrCSV = '';  //DKDK
                        // $solrCSV = var_dump($solrQueryTemp);     //DKDK for checking $solrQuery
                        // $solrCSV = var_dump($domain_subdomain);     //DKDK for checking $solrQuery
                        // $solrCSV = var_dump($solrQuery);     //DKDK for checking $solrQuery
                        // $solrCSV = var_dump($results_count) . var_dump($rowcount) . var_dump($user_name) . var_dump($user_id);     //DKDK for checking $solrQuery
                        print($solrCSV); //DKDK
                        // print(chr(255) . chr(254) . mb_convert_encoding($solrCSV, 'UTF-16LE', 'UTF-8')); //DKDK mmm does not work properly
                        // Clear the CSV line for the next set of data
                        $solrCSV = '';
                    }
                    $startcount = $startcount + $rowcount_once;  //Increase the startcount. No need to have +1 as $startcount starts from 0, e.g., 1000 data from 0 means 0~999
                    $docs='';
                } else {
                    break;
                }
            }
        }
        catch (Exception $e) {
            if($e->getCode() == 0) {
                drupal_goto('vbsearch/download/timeout');
                return;
            }
        }
    } // end of if ($results_count <= $rowcount || $rowcount == 5000)
    // in case $results_count > $rowcount
    else {
        // //get username and uid - this does not work in the new way
        // global $user;
        // $user_name = $user->name;
        // $user_id = $user->uid;

        //add this here
        $solrQuery->replaceParam('rows', $results_count);

        //DB entries: create unique id via md5 hash
        $md5hash = 'vbmd_' . md5(uniqid(rand(), true));
        $current_time = time();
        //record to DB whether Export download or sequence
        $md_contents = 'Export download';
        //current_status: defined to check whether export is ready (1) or not (0). need to be updated at BG process
        $current_status = 0;

        //serialize - be careful for protected members at object: use base64_encode/decode
        $fieldlist_serial = serialize($fieldlist);
        $solrQuery_serial = base64_encode(serialize($solrQuery));
        $fieldlist_unserial = unserialize($fieldlist_serial);
        $solrQuery_unserial = unserialize(base64_decode($solrQuery_serial));

        //save to db table
        $db_fields = array('md_userid' => $user_id, 'md_status' => $current_status, 'md_md5hash' => $md5hash, 'md_time' => $current_time, 'md_totalnumber' => $results_count, 'md_contents' => $md_contents, 'md_description' => $md_description, 'md_fieldlist' => $fieldlist_serial, 'md_solrquery' => $solrQuery_serial);
        db_insert('vbsearch_md_background')->fields($db_fields)->execute();

        //for using drush in shell_exec, somehow its path needs to be set - for new export, getcwd() offers correct path without invoking drupal_get_path()
        // $command = '/usr/local/bin/drush php-script ' . getcwd() . '/' . drupal_get_path('module', 'vbsearch_md') . '/vbsearch_md_bg_down.php ' . $md5hash;
        // $command = '/usr/local/bin/drush php-script ' . getcwd() . '/vbsearch_md_bg_down.php ' . $md5hash;
        // With the change of the base path to DRUPAL_ROOT due to the File Search Fix (VBHELP-403), need to set the path correctly
        $command = '/usr/local/bin/drush php-script ' . drupal_get_path('module','vbsearch_md') . '/vbsearch_md_bg_down.php ' . $md5hash;

        //run background process
        shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));
        //checking drush errors
        // shell_exec(sprintf('%s > /home/testdown/temp1/list.log 2>/home/testdown/temp1/error.log ', $command));

        // //message for popup
        // $message = 'Export request was sent to a Background Job. \\nIn general, output file will be ready within an hour and its Download link will be provided at "Export" under "Your jobs" in your Profile page. \\nPressing "OK" will redirect to the aformentioned page';
        // // echo "<script type='text/javascript'>alert('$message'); </script>";
        // // echo "<script type='text/javascript'>alert('$message'); window.close();</script>";
        // echo "<script type='text/javascript'>alert('$message'); window.location.href='/users/$user_name/#collapse-Export';</script>";
        // // echo "<script type='text/javascript'>alert('$message'); window.location.href='/jobstatus';</script>";


        // // checking error!
        // // //open file - possibly use DRUPAL_ROOT here?
        // $savePath = '/home/testdown/temp1/';
        // $php_arg = 'dk_error1';
        // $myfile = fopen("$savePath$php_arg.log", "w");
        // $solrCSV = $command;
        // fwrite($myfile, $solrCSV);
        // fclose($myfile);

    }

    // Reset timeout to previous value
    // ini_set('default_socket_timeout', $oldTO);
    // ini_set('max_execution_time', $oldExTime);
 //    ini_set('memory_limit', $oldMemory);
    // ini_set('max_input_time', $oldInputTime);   //DKDK

    return;

}

function clean($string){
    return preg_replace('/[^A-Za-z0-9\-\." "\/\=\?\_\:\%]/', ' ', $string); // Removes special chars.
}

//DKDK for switch function!!!
function getFieldset($fqarray){
    $domain = null;
    $subdomain = null;

    foreach ($fqarray as $fqterm){
        if(is_null($domain)){
            $domain = extractParam($fqterm,'site:"');
        }
        if(is_null($subdomain)){
            $subdomain = extractParam($fqterm,'bundle_name:"');
        }
    }
    //DKDK update fl and add more cases even if this function may not be used eventually
    //https://confluence.vectorbase.org/pages/viewpage.action?spaceKey=DB&title=Bulk+export+field+configuration
    switch ($domain) {
        case "Comparative":
            switch ($subdomain) {
                case "Homology":
                    $fieldList = array('accession','gene','symbol','species','description');
                    break;
                case "Gene tree":
                    $fieldList = array('accession','description','members');
                    break;
                default:
                    $fieldList = array('label','site','description');
            }
            break;
        case "Genome":
            switch ($subdomain) {
                case "Genomic sequence assembly":
                    $fieldList = array('accession_insdc','species','strain','assembly_version','seq_length','description');     //DKDK id was removed 032317
                    break;
                case "Transcript":
                    $fieldList = array('accession','species','strain','biotype','description','seq_length','exon_number','seq_region_name','seq_region_start','seq_region_end');
                    break;
                case "Gene":
                    $fieldList = array('accession','symbol','species','strain','biotype','description','domain','go_cvterms','cvterms','seq_region_name','seq_region_start','seq_region_end');
                    break;
                case "Translation":
                    $fieldList = array('accession','species','strain','description','domain','go_cvterms','cvterms','seq_length','molecular_mass','isoelectric_point','seq_region_name','seq_region_start','seq_region_end');
                    break;
                case "Mitochondrial genome":
                    $fieldList = array('accession','gene','species','description');
                    break;
                default:
                    $fieldList = array('label','site','description');
            }
            break;
        // raw_data_type_s added - VB-6454; fixed two issues on RNA-Seq...: field list switched & sub-domain name changed (seq->Seq) - VB-
        case "Expression":
            switch ($subdomain) {
                case "Probe":
                    $fieldList = array('accession','label','description','gene_ids','symbol','species','platform_id_ss');
                    break;
                case "Expression statistic":
                    $fieldList = array('accession','label','description','projects','species','gene_ids','symbol','experimental_factors','p_value','statistical_test','high_conditions','low_conditions','raw_data_type_s');
                    break;
                case "Sample":
                    $fieldList = array('label','species','annotations','projects');
                    break;
                case "RNA-Seq track groups":
                    $fieldList = array('label','description','species','strain','pubmed_ids');
                    break;
                case "Experiment":
                    $fieldList = array('label','description','authors','pubmed','raw_data_type_s');
                    break;
                case "RNA-Seq tracks":
                    $fieldList = array('label','description','species','strain','study_accessions','experiment_accessions','run_accessions','sample_accessions','bam_s_url','bigwig_s_url');
                    break;
                case "Platform":
                    $fieldList = array('label','description','projects');
                    break;
                case "Gene list":
                    $fieldList = array('label','description','projects','raw_data_type_s');
                    break;
            }
            break;
        case "Ontology":
            $fieldList = array('accession', 'label','description','synonym','is_a');
            break;
        case "Population Biology":
            switch ($subdomain) {
                case "Sample genotype":     //DKDK updated for 19-04, VB-7133
                    $fieldList = array('label','sample_id_s','assay_id_s','description','sample_type','projects','species','has_geodata','geo_coords','geolocations','collection_protocols','collection_assay_id_s','collection_date','collection_date_range','protocols','phenotypes','annotations','genotype_type_s','genotype_name_s','genotype_inverted_allele_count_i','genotype_microsatellite_length_i','genotype_mutated_protein_value_f','genotype_mutated_protein_unit_s','exp_citations_ss','tags_ss','attractants_ss','licenses_ss','sex_s','sample_name_s','dev_stages_ss');
                    break;
                case "Sample":    //DKDK updated for 17-04, VB-6059
                    $fieldList = array('accession','label','description','sample_type','projects','species','has_geodata','geo_coords','geolocations','collection_protocols','collection_assay_id_s','collection_date','collection_date_range','collection_duration_days_i','has_abundance_data_b','protocols','genotypes','phenotypes','annotations','sample_size_i','exp_citations_ss','tags_ss','attractants_ss','licenses_ss','sex_s','dev_stages_ss');
                    break;
                case "Sample phenotype":    //DKDK updated for 17-04, VB-6059, 19-04, VB-7133
                    $fieldList = array('label','sample_id_s','assay_id_s','description','sample_type','projects','species','has_geodata','geo_coords','geolocations','collection_protocols','collection_assay_id_s','collection_date','collection_date_range','protocols','genotypes','annotations','phenotype_type_s','phenotype_value_f','phenotype_value_unit_s','phenotype_value_type_s','insecticide_s','concentration_f','concentration_unit_s','duration_f','duration_unit_s','sample_size_i','blood_meal_source_s','blood_meal_status_s','infection_source_s','infection_status_s','exp_citations_ss','tags_ss','attractants_ss','licenses_ss','sex_s','sample_name_s','dev_stages_ss');
                    break;
                case "Project":
                    $fieldList = array('accession','label','description','study_designs','pubmed','authors','publications_status','exp_citations_ss','tags_ss','licenses_ss','dev_stages_ss');
                    break;
                default:
                    $fieldList = array('accession','authors','collection_date','description','study_designs','label','exp_citations_ss');
            }
            break;
        case "Transcriptome":
            switch ($subdomain) {
                // DKDK only one sub-domain exists
                case "RNA-Seq assembled transcript":
                    $fieldList = array('accession','label','description','species','sequence_length_i','sequence_type_s','projects','InterPro_ID_ss','analysis_domain_ID_ss','GO_terms_txt','pathways_txt');
                    break;
            }
            break;
        case "Proteome":
            switch ($subdomain) {
                // DKDK only one sub-domain exists
                case "Mass spectrometry peptide":
                    $fieldList = array('accession','label','description','species','sequence_length_i','sequence_type_s','sequence_s','projects','protein_matches_ss','annotations');
                    break;
            }
            break;
        // VB-7625 Variation domain and its subdomains   
        case "Variation":
            switch ($subdomain) {
                case "Short variations":
                    $fieldList = array('label','allele_string_s','phenotypes_ss','consequence_ss','sift_ss','genes_ss','gene_symbols_ss','transcripts_ss','projects_ss','seq_region_name','seq_region_start','seq_region_end','species','strain');
                    break;
                case "Structural variations":
                    $fieldList = array('label','class_s','seq_region_name','seq_region_start','seq_region_end','species','strain');
                    break;
                case "Variation sets":
                    $fieldList = array('label','description','species','strain','popbio_id_s');
                    break;
            }
            break;
        default:
            $fieldList = array('label','site','description');    //DKDK Case-sensitive!!! fieldList, not fieldlist (L vs l) - actually original code was also wrong
    }
    return($fieldList);
}

?>