<?php

// **** load drupal enviornment ****
define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
chdir(DRUPAL_ROOT);

// need to get these values via GET
$rowcount = $_GET['value0'];
$user_id  = $_GET['value1'];
//cookie
$downloadTokenValue = $_GET['value2'];

// run the main download function & cookie
vbsearch_md_return_data_seq($rowcount,$user_id,$downloadTokenValue);

//DKDK add new function for sequence download
function vbsearch_md_return_data_seq($rowcount,$user_id,$downloadTokenValue){
    // Production code

    //  this function will be beyond drupal module in the new export, thus a bypass is to get the value from JS
    if (empty($user_id) != True) {
        $user_id = (int)$user_id;
        $user = user_load($user_id);
        $user_name = $user->name;
    }


    //
    //Step0: get url information, parsing
    //
    // $request_url=$_SERVER['REQUEST_URI'];    //this does not work because vbsearch_md_return_data() is running under /vbsearch/download/
    $url_referer = parse_url($_SERVER['HTTP_REFERER']);
    if ($url_referer['query'] == '') {
        $request_url = $url_referer['path'];
    } else {
        $request_url = $url_referer['path'] . '?' . $url_referer['query'];
    }

    //Don't know exactly why but sometimes below strange url encoding characters are shown so let's convert it to normal: e.g., %2522PMID%253A22346749%2522
    $request_url = urldecode($request_url);
    // $request_url = str_replace('%2522', '%22', $request_url);
    // $request_url = str_replace('%253A', '%3A', $request_url);
    // $request_url = str_replace('%2520', '%20', $request_url);
    // $request_url = str_replace('%252A', '%2A', $request_url);

    //Start url parsing - sometimes, especially after export, the url do not have ?& -> it is converted to ? (before: ?&, after: ?)
    // DKDK VB-7801 add limit to 4 to handle slash in some species, e.g., Anopheles daciae/messeae group (Brugman et al.) 
    $explode_slash=explode('/',$request_url,4);
    if(strpos($explode_slash[3], '?&') !== false) {
        $explode_qand=explode('?&',$explode_slash[3]);
    } else if (strpos($explode_slash[3], '?') !== false) {
        $explode_qand=explode('?',$explode_slash[3]);
    } else {
        $explode_qand=explode('?&',$explode_slash[3]);
    }

    // //just in case replace " into %22 if exist. And, (space) to %20 - for query term like "expression browser" - for curl
    // $explode_qand[0] = str_replace('"', '%22', $explode_qand[0]);
    // $explode_qand[0] = str_replace(' ', '%20', $explode_qand[0]);

    //For $solrQuery, decode q=. $explode_qand[0] = search term, q= & $explode_qand[1] = for fq=
    $explode_qand[0] = urldecode($explode_qand[0]);

    //
    //Step1: construct $solrQuery
    //

    //construct initial $solrQuery
    // $query_init = '';
    $query_init = $explode_qand[0];     // use here instead of replacing q parameter to accept file search
    $solr_url = vbsearch_find_solr_url();
    $queryParameters = array();


    //make initial $solrQuery
    $solrQuery = vbsearch_createSolrQuery($query_init, $queryParameters['qf'], $queryParameters['fl'], 1, $solr_url);

    // functions are changed to use instance
    //using $_GET made by url parsing
    parse_str($explode_qand[1], $_GET);

    // //set $vbsearch_instance if search_id exists
    // if (isset($_GET['search_id'])) {
    //   //Get the list of fields from search so we can acceess their values
    //   $vbsearch_instance = vbsearch_load($_GET['search_id']);
    // } else {
    //   //Initializing field list so we can pass it as a variable
    //   $vbsearch_instance = array();
    // }
    // //make complete $solrQuery including qf
    // $solrQuery = vbsearch_setup_query($solrQuery,$vbsearch_instance);   //changed

    // //set basic fq - domain, sub-domain, species etc.
    // $solrQuery->removeParam('fq');
    // $solrQuery = vbsearch_add_facet_parameters($solrQuery);

    // //set advanced fq - advanced search regarding search_id
    // $solrQuery = vbsearch_add_advanced_parameters($solrQuery,$vbsearch_instance);   //changed

    //DKDK replace above to single command
    $solrQuery = vbsearch_apachesolr_query_alter($solrQuery);
    $solrQuery = vbsearch_facets_apachesolr_query_alter($solrQuery);

    //Add q= and remove unnecessary query parameters
    // $solrQuery->replaceParam('q', $explode_qand[0]); //DKDK this is not necessary to be here. Similar one is moved to $quuery_init
    $solrQuery->removeParam('bq');
    $solrQuery->removeParam('boost');

    //Replace parameters for query once (to get site and bundle_name)
    $solrQuery->replaceParam('rows', 1);    //DKDK set rows 1 to get only one result
    $solrQuery->replaceParam('hl', 'false');
    $solrQuery->replaceParam('facet', 'false');
    $solrQuery->replaceParam('fl',array('site','bundle_name'));

    //
    // Step 2: Increase PHP time limits to 5 minutes.
    //
    // drupal_set_time_limit(300);
    // $oldTO = ini_get('default_socket_timeout');
    // ini_set('default_socket_timeout', 300);
    // $oldExTime = ini_get('max_execution_time');
    // ini_set('max_execution_time', 300);
    // $oldMemory = ini_get('memory_limit');
    // ini_set('memory_limit', '1024M');
    // $oldInputTime = ini_get('max_input_time');
    // ini_set('max_input_time', 0);       // 0 = unlimited time


    //Step 3: query for single data to get domain, sub-domain information from the result
    try {
        $solrResults_once = $solrQuery->search();
    }
    catch (Exception $e) {
        if($e->getCode() == 0) {
            drupal_goto('vbsearch/download/timeout');
            return;
        }
    }

    $docs_once = $solrResults_once->response->docs;
    $domain_subdomain = array('site:"'.$docs_once[0]->site.'"', 'bundle_name:"'.$docs_once[0]->bundle_name.'"');
    //for background process
    $md_description = $docs_once[0]->site . ', ' . $docs_once[0]->bundle_name;

    // check total number of available data
    $results_count = $solrResults_once->response->numFound;

    //
    // Step 4: actual query for export
    //
    // Set the download count depending on login status.
    // if (user_is_logged_in()){
    //     $rowcount = 300000;
    // }else{
    //     $rowcount = 5000;
    // }

    // need to use this value instead of finding it via login or not
    $rowcount = (int)$rowcount;

    // $solrQuery->replaceParam('rows', $rowcount);
    $solrQuery->replaceParam('hl', 'false');
    $solrQuery->replaceParam('facet', 'false');
    // $solrQuery->replaceParam('fl', getFieldset($fqarray));  //DKDK using fl restricts results which become inconsistent labels for search cases
    $fieldlist_seq = getFieldset_seq($domain_subdomain);
    $solrQuery->replaceParam('fl', $fieldlist_seq);  //DKDK use new variable to have domain & subdomain here
    //$solrQuery->replaceParam('fl','');
    //$solrQuery->replaceParam('fl', array('label','description','accession','species','projects'));


    if ($results_count <= $rowcount || $rowcount == 5000) {

        //move to here
        $solrQuery->replaceParam('rows', $rowcount);

        // Step 5: Process the results, creating text file (.seq)
        // Holds the file for each data line
        $solrCSV = "";
        // Holds the file header line
        $header = "";
        // Temporary storage for the key values pairs returned by Solr.
        // Required to preserve the order of the columns, as Solr does not
        // maintain column order from row to row for some reason.
        $dataArray = array();

        // Set filename with timestamp - Eastern Daylight Saving Time
        date_default_timezone_set('EST5EDT');
        $filename = 'sequence_' . date('omdHis') . '.fa';

        // Get the array of docs
        // $docs = $solrResults->response->docs;

        //Set the document headers
            header('Content-Encoding: UTF-8');  //DKDK
            header("Content-type: text; charset=UTF-8");   //DKDK
            // header('Content-Disposition: attachment; filename="download.fa"');
            header('Content-Disposition: attachment; filename=' . $filename);
            // header("Content-Transfer-Encoding: ascii");  //DKDK
            header('Expires: 0');   //DKDK
            // header("Expires: Mon, 01 Jan 1990 05:00:00 GMT");   //DKDK
            header('Pragma: no-cache');
            // cookie
            setcookie("fileSequenceToken", $downloadTokenValue, 0, '/');

        ob_clean();    //DKDK delete output buffer before starting to record
        //set initial counts
        $startcount = 0;
        $rowcount_once = 50000;

        //set the number of loops
        $loop_count = ceil($rowcount/$rowcount_once);

        // //set the number of loops
        // if($rowcount_once > $rowcount)  {
        //     $loop_count = intval($rowcount/$rowcount_once) + 1;
        // } else {
        //     $loop_count = intval($rowcount/$rowcount_once);
        // }

        //Execute the query
        try {
            //Make a loop for dealing with massive download?
            for ($iii = 0; $iii < $loop_count; $iii++) {
                $solrQuery->replaceParam('start', $startcount);
                $solrQuery->replaceParam('rows', $rowcount_once);
                $solrResults = $solrQuery->search();
                $docs = $solrResults->response->docs;      //DKDK this remains the same with previous one
                if (empty($docs) != True) {
                    foreach ($docs as $doc){
                        //DKDK below works but two issues considered
                        // a) Genome does always not have data - checking subdomain and implemented at md.js
                        // b) depending on sub-domain, several cases exist: sequence_cds or sequence_aa, both, or none
                        if ($docs_once[0]->site == "Genome") {
                            if( (($docs_once[0]->bundle_name == "Translation") || ($docs_once[0]->bundle_name == "Gene") && ($doc->sequence_aa != "")) ) {
                                $solrCSV = ">" . $doc->accession . "\r\n" . $doc->sequence_aa . "\r\n\r\n";
                                print($solrCSV); //DKDK
                            } elseif ($docs_once[0]->bundle_name == "Transcript" && ($doc->sequence_cds != "")) {
                                $solrCSV = ">" . $doc->accession . "\r\n" . $doc->sequence_cds . "\r\n\r\n";
                                print($solrCSV); //DKDK
                            }
                        } elseif ($doc->sequence_s != "") {
                            $solrCSV = ">" . $doc->accession . "\r\n" . $doc->sequence_s . "\r\n\r\n";
                            print($solrCSV); //DKDK
                        }
                    }
                    $startcount = $startcount + $rowcount_once;  //Increase the startcount. No need to have +1 as $startcount starts from 0, e.g., 1000 data from 0 means 0~999
                    $docs='';   // foreach ($docs as $doc)
                } else {
                    break;
                }
            }
        }
        catch (Exception $e) {
            if($e->getCode() == 0) {
                drupal_goto('vbsearch/download/timeout');
                return;
            }
        }
    } // end of if ($results_count <= $rowcount || $rowcount == 5000)
    // in case $results_count > $rowcount
    else {
        // //get username and uid
        // global $user;
        // $user_name = $user->name;
        // $user_id = $user->uid;

        //add this here
        $solrQuery->replaceParam('rows', $results_count);

        //create unique id via md5 hash
        $md5hash = 'vbmd_' . md5(uniqid(rand(), true));
        $current_time = time();
        //record to db whether Export download or sequence
        $md_contents = 'Export sequence';
        //current_status: defined to check whether export is ready (1) or not (0). need to be updated at BG process
        $current_status = 0;

        //serialize - be careful for protected members at object: use base64_encode/decode
        $fieldlist_serial = serialize($fieldlist_seq);
        $solrQuery_serial = base64_encode(serialize($solrQuery));
        $fieldlist_unserial = unserialize($fieldlist_serial);
        $solrQuery_unserial = unserialize(base64_decode($solrQuery_serial));

        //save to db table
        $db_fields = array('md_userid' => $user_id, 'md_status' => $current_status, 'md_md5hash' => $md5hash, 'md_time' => $current_time, 'md_totalnumber' => $results_count, 'md_contents' => $md_contents, 'md_description' => $md_description, 'md_fieldlist' => $fieldlist_serial, 'md_solrquery' => $solrQuery_serial);
        db_insert('vbsearch_md_background')->fields($db_fields)->execute();

        //for using drush in shell_exec, somehow its path needs to be set - for new export, getcwd() offers correct path without invoking drupal_get_path()
        // $command = '/usr/local/bin/drush php-script ' . getcwd() . '/' . drupal_get_path('module', 'vbsearch_md') . '/vbsearch_md_bg_seq.php ' . $md5hash;
        // $command = '/usr/local/bin/drush php-script ' . getcwd() . '/vbsearch_md_bg_seq.php ' . $md5hash;
        // With the change of the base path to DRUPAL_ROOT due to the File Search Fix (VBHELP-403), need to set the path correctly        
        $command = '/usr/local/bin/drush php-script ' . drupal_get_path('module','vbsearch_md') . '/vbsearch_md_bg_seq.php ' . $md5hash;           
             
        shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));
        //checking drush errors
        // shell_exec(sprintf('%s > /home/testdown/temp1/list.log 2>/home/testdown/temp1/error.log ', $command));

        // $message = 'Export request was sent to a Background Job. \\nIn general, output file will be ready within an hour and its Download link will be provided at "Export" under "Your jobs" in your Profile page. \\nPressing "OK" will redirect to the aformentioned page';
        // // echo "<script type='text/javascript'>alert('$message'); </script>";
        // // echo "<script type='text/javascript'>alert('$message'); window.close();</script>";
        // echo "<script type='text/javascript'>alert('$message'); window.location.href='/users/$user_name/#collapse-Export';</script>";
        // // echo "<script type='text/javascript'>alert('$message'); window.location.href='/jobstatus';</script>";

    }

    // Reset timeout to previous value
    // ini_set('default_socket_timeout', $oldTO);
    // ini_set('max_execution_time', $oldExTime);
 //    ini_set('memory_limit', $oldMemory);
    // ini_set('max_input_time', $oldInputTime);   //DKDK

    return;
}

//DKDK function for sequence download
function getFieldset_seq($fqarray){
    $domain = null;
    $subdomain = null;

    foreach ($fqarray as $fqterm){
        if(is_null($domain)){
            $domain = extractParam($fqterm,'site:"');
        }
        if(is_null($subdomain)){
            $subdomain = extractParam($fqterm,'bundle_name:"');
        }
    }
    //DKDK update fl and add more cases even if this function may not be used eventually
    // changed from label to accession
    switch ($domain) {
        case "Genome":
            $fieldList = array('accession','sequence_aa','sequence_cds'); //DKDK sequence_aa or sequence_cds
            break;
        case "Transcriptome":
            $fieldList = array('accession','sequence_s');
            break;
        case "Proteome":
            $fieldList = array('accession','sequence_s');
            break;
    }
    return($fieldList);
}

?>