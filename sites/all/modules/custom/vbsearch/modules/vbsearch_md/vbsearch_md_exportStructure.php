<?php

// **** load drupal enviornment ****
define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
require_once(DRUPAL_ROOT.'/includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
chdir(DRUPAL_ROOT);

// need to get these values via GET
$rowcount = $_GET['value0'];
$user_id  = $_GET['value1'];
//cookie
$downloadTokenValue = $_GET['value2'];

// run the main download function
vbsearch_md_Structure($rowcount,$user_id,$downloadTokenValue);

// main download function
function vbsearch_md_Structure($rowcount,$user_id,$downloadTokenValue) {
    // Production code

    // test if this works - this does not work as this function will be beyond drupal module in the new export
        // global $user;
        // $user_name = $user->name;
        // $user_id = $user->uid;
    // thus, a bypass is to get the value from JS
    if (empty($user_id) != True) {
        $user_id = (int)$user_id;
        $user = user_load($user_id);
        $user_name = $user->name;
    }

    // need to use this GET value instead of finding it via login or not
    $rowcount = (int)$rowcount;

    // maybe necessary to control the size?
    // if ($rowcount == 300000) {
    //     // set rowcount for STRUCTURE as 100000: 300000 means logged in user
    //     $rowcount = 500000;
    // }

    //
    // Step 2: Increase PHP time limits to 5 minutes.
    //
    // drupal_set_time_limit(240);
    // // $oldTO = ini_get('default_socket_timeout');
    // // ini_set('default_socket_timeout', 300);
    // $oldExTime = ini_get('max_execution_time');
    // ini_set('max_execution_time', 300);
    // // $oldMemory = ini_get('memory_limit');
    // // ini_set('memory_limit', '1024M');
    // // $oldInputTime = ini_get('max_input_time');
    // // ini_set('max_input_time', 0);       // 0 = unlimited time

    //
    //Step0: get url information, parsing
    //
    // $request_url=$_SERVER['REQUEST_URI'];    //this does not work because vbsearch_md_return_data() is running under /vbsearch/download/
    $url_referer = parse_url($_SERVER['HTTP_REFERER']);
    if ($url_referer['query'] == '') {
        $request_url = $url_referer['path'];
    } else {
        $request_url = $url_referer['path'] . '?' . $url_referer['query'];
    }

    //sometimes encoding characters are included so let's decode them: e.g., %2522PMID%253A22346749%2522
    $request_url = urldecode($request_url);
    // $request_url = str_replace('%2522', '%22', $request_url);
    // $request_url = str_replace('%253A', '%3A', $request_url);
    // $request_url = str_replace('%2520', '%20', $request_url);
    // $request_url = str_replace('%252A', '%2A', $request_url);

    //Start url parsing - sometimes, especially after export, the url do not have ?& -> it is converted to ? (before: ?&, after: ?)
    // DKDK VB-7801 add limit to 4 to handle slash in some species, e.g., Anopheles daciae/messeae group (Brugman et al.)
    $explode_slash=explode('/',$request_url,4);
    if(strpos($explode_slash[3], '?&') !== false) {
        $explode_qand=explode('?&',$explode_slash[3]);
    } else if (strpos($explode_slash[3], '?') !== false) {
        $explode_qand=explode('?',$explode_slash[3]);
    } else {
        $explode_qand=explode('?&',$explode_slash[3]);
    }

    // //just in case replace " into %22 if exist. And, (space) to %20 - for query term like "expression browser" - for curl
    // $explode_qand[0] = str_replace('"', '%22', $explode_qand[0]);
    // $explode_qand[0] = str_replace(' ', '%20', $explode_qand[0]);

    //For $solrQuery, decode q=. $explode_qand[0] = search term, q= & $explode_qand[1] = for fq=
    $explode_qand[0] = urldecode($explode_qand[0]);

    //
    //Step1: construct $solrQuery
    //

    //construct initial $solrQuery
    // $query_init = '';
    $query_init = $explode_qand[0];     // use here instead of replacing q parameter to accept file search
    $solr_url = vbsearch_find_solr_url();
    $queryParameters = array();

    //make initial $solrQuery
    $solrQuery = vbsearch_createSolrQuery($query_init, $queryParameters['qf'], $queryParameters['fl'], 1, $solr_url);

    // functions are changed to use instance
    //using $_GET made by url parsing
    parse_str($explode_qand[1], $_GET);


    //DKDK replace above to single command
    $solrQuery = vbsearch_apachesolr_query_alter($solrQuery);
    $solrQuery = vbsearch_facets_apachesolr_query_alter($solrQuery);
    $solrQueryTemp = $solrQuery;    // simply for test purpose

    //Add q= and remove unnecessary query parameters
    // $solrQuery->replaceParam('q', $explode_qand[0]); //DKDK this is not necessary to be here. Similar one is moved to $quuery_init
    $solrQuery->removeParam('bq');
    $solrQuery->removeParam('boost');

    //Replace parameters for query once (to get site and bundle_name)
    $solrQuery->replaceParam('rows', 1);    //DKDK set rows to large value to get all result
    $solrQuery->replaceParam('hl', 'false');
    $solrQuery->replaceParam('facet', 'false');
    $solrQuery->replaceParam('fl',array('site','bundle_name'));
    // // add fq for genotype_type_s:microsatellite
    // $solrQuery->addParam('fq','genotype_type_s:"microsatellite"');

    // query for data to get domain, sub-domain information from the result
    try {
        $solrResults_once = $solrQuery->search();
    }
    catch (Exception $e) {
        if($e->getCode() == 0) {
            drupal_goto('vbsearch/download/timeout');
            return;
        }
    }

    $docs_once = $solrResults_once->response->docs;
    // $domain_subdomain = array('site:"'.$docs_once[0]->site.'"', 'bundle_name:"'.$docs_once[0]->bundle_name.'"');
    // for background process
    $md_description = $docs_once[0]->site . ', ' . $docs_once[0]->bundle_name;


    // check total number of available data
    $results_count = $solrResults_once->response->numFound;

    $solrQuery->replaceParam('hl', 'false');
    $solrQuery->replaceParam('facet', 'false');
    // // change count
    // // $solrQuery->replaceParam('rows', 2147483647);    //DKDK set rows to large value to get all result
    // $solrQuery->replaceParam('rows', 1);    //DKDK set rows to large value to get all result
    $solrQuery->replaceParam('rows', $results_count);
    // refine fl
    //DKDK VB-7133 adding new field - no need to change this for background process - adding country_s & species too
    $fieldlist_STRUCTURE = array('id','sample_name_s','country_s','species','sample_id_s','genotype_name_s','genotype_microsatellite_length_i');
    $solrQuery->replaceParam('fl',$fieldlist_STRUCTURE);
    // add fq for genotype_type_s:microsatellite: here is the best place as Search results in display do not include below fq
    $solrQuery->addParam('fq','genotype_type_s:"microsatellite"');

    // on-the-fly or background
    if ($results_count <= $rowcount || $rowcount == 5000) {
        //set initial counts
        $startcount = 0;
        $rowcount_once = 50000;

        //set the number of loops and initialize $docs
        $loop_count = ceil($rowcount/$rowcount_once);
        $docs = array();

        // query for data to get domain, sub-domain information from the result
        try {
            // get segmented data to avoid ApacheSolr timeout issue
            for ($iii = 0; $iii < $loop_count; $iii++) {
                $solrQuery->replaceParam('start', $startcount);
                $solrQuery->replaceParam('rows', $rowcount_once);
                $solrResults = $solrQuery->search();
                // convert PHP object to PHP Array
                $jsonfile = json_decode(json_encode($solrResults), true);
                $docs_temp = $jsonfile['response']['docs'];
                if (empty($docs_temp) != True) {
                    // merge array
                    $docs = array_merge($docs,$docs_temp);
                }
                $jsonfile = '';
                $docs_temp = '';
                //Increase the startcount. No need to have +1 as $startcount starts from 0, e.g., 1000 data from 0 means 0~999
                $startcount = $startcount + $rowcount_once;
            }

            // // // // checking error!
            // // // // //open file - possibly use DRUPAL_ROOT here?
            // $savePath_ct = '/home/testdown/temp1/';
            // $php_arg_ct = 'dk_error1_count';
            // $myfile_ct = fopen("$savePath_ct$php_arg_ct.log", "w");
            // // $solrCSV = $command;
            // fwrite($myfile_ct, count($docs));
            // fclose($myfile_ct);

            // // temporarily
            // $savePath_docs = '/home/testdown/temp1/';
            // $php_arg_docs = 'dk_error1_bg_docs';
            // $myfile_docs = fopen("$savePath_docs$php_arg_docs.log", "w");
            // fwrite($myfile_docs, print_r($docs,true));
            // fclose($myfile_docs);


            // var_dump($docs);
            $fieldlist = array();
            $idlist = array();
            for ($i = 0; $i < count($docs); $i++) {
                $fieldlist[$i] = $docs[$i]['genotype_name_s'];
                $idlist[$i] = $docs[$i]['sample_id_s'];
                //DKDK VB-7133 adding new field - adding country_s & species too
                $namelist[$i] = $docs[$i]['sample_name_s'];
                $countrylist[$i] = $docs[$i]['country_s'];
                $specieslist[$i] = $docs[$i]['species'];
            }

            // get unique values
            $fieldlist = array_values(array_unique($fieldlist));
            //DKDK VB-7133 adding new field - adding country_s & species too
            $idlistUniqueIndex = array_unique($idlist);
            $idlist_unique = array_values($idlistUniqueIndex);
            $uniqueIndex = array_keys($idlistUniqueIndex);
            if (empty($docs) != True) {
                for ($i = 0; $i < count($idlistUniqueIndex); $i++) {
                    $namelistUnique[$i] = $namelist[$uniqueIndex[$i]];
                    $countrylistUnique[$i] = $countrylist[$uniqueIndex[$i]];
                    $specieslistUnique[$i] = $specieslist[$uniqueIndex[$i]];
                }
            }

            // Set filename with timestamp - Eastern Daylight Saving Time
            date_default_timezone_set('EST5EDT');
            $filename = 'structure_' . date('omdHis') . '.csv';

            //Set the document headers
            header('Content-Encoding: UTF-8');
            header("Content-type: text/csv; charset=UTF-8");
            // header('Content-Disposition: attachment; filename="structure.csv"');
            header('Content-Disposition: attachment; filename=' . $filename);
            // header("Content-Transfer-Encoding: ascii");
            header('Expires: 0');
            // header("Expires: Mon, 01 Jan 1990 05:00:00 GMT");
            header('Pragma: no-cache');
            // cookie
            setcookie("fileStructureToken", $downloadTokenValue, 0, '/');

            // header
            $solrCSV = '';
            // header starts with ID,
            //DKDK VB-7133 adding new field - adding country_s & species too
            $header_new = 'ID,Species,Sample label,Country,';
            foreach ($fieldlist as $fieldvalue){
                $header_new = $header_new . $fieldvalue . ",";
            }
            // remove trailing comma for header
            $header_new = rtrim($header_new,",");
            $solrCSV = $header_new . "\r\n";
            ob_clean();    //DKDK delete output buffer before starting to record
            print($solrCSV);
            // echo "<br><br>";
            $solrCSV = '';

            // count each idlist: output is an associative array, sample_id_s => the number of the sample_id_s.
            $key_num = array_count_values($idlist);

            // initiate $inc_num for looping through partial docs & $solrCSV_even & $solrCSV_odd
            $incr_num = 0;
            if (empty($docs) != True) {
                for ($i = 0; $i < count($idlist_unique); $i++) {
                    // may need to handle single exception, VBS0104008 which has only 23 data???
                    //DKDK VB-7133 adding new field - adding country_s & species too: species is array so need to indicate index!
                    $solrCSV_init = $idlist_unique[$i] . ',' . $specieslistUnique[$i][0] . ',' . $namelistUnique[$i] . ',' . $countrylistUnique[$i] . ',';
                    // initialize array with default values: in this manner, no need to check empty element nor assign value for those empty elements
                    $solrCSV_even = array_fill(0,count($fieldlist),'-9');
                    $solrCSV_odd  = array_fill(0,count($fieldlist),'-9');
                    // print_r(count($solrCSV_even));

                    // change for loop range so that only partial $doc is checked per each $idlist_unique
                    for ($j = 0 + $incr_num;$j < $key_num[$idlist_unique[$i]] + $incr_num; $j++) {
                        if ($j % 2 == 0) {
                            // check genotype_name_s exists
                            if ($docs[$j]['genotype_name_s']) {
                                // check if the key value, genotype_name_s, exists in the fieldlist and get index (return key)
                                $indexnum = array_search($docs[$j]['genotype_name_s'],$fieldlist);
                                // if index exists - well always exists, so no need to check
                                $solrCSV_even[$indexnum] = (string)$docs[$j]['genotype_microsatellite_length_i'];
                            }
                        } else {
                            // check genotype_name_s exists
                            if ($docs[$j]['genotype_name_s']) {
                                // check if the key value, genotype_name_s, exists in the fieldlist and get index (return key)
                                $indexnum = array_search($docs[$j]['genotype_name_s'],$fieldlist);
                                // if $indexnum exists - well always exists, so no need to check
                                $solrCSV_odd[$indexnum] = (string)$docs[$j]['genotype_microsatellite_length_i'];
                            }
                        }
                        // echo $j;
                    }
                    // convert array to string with comma
                    $solrCSV_even_out = implode(',', $solrCSV_even);
                    $solrCSV_odd_out  = implode(',', $solrCSV_odd);

                    // print to $solrCSV
                    // ob_clean();
                    $solrCSV = $solrCSV_init . $solrCSV_even_out . "\r\n";
                    // echo "<br>";
                    print($solrCSV);
                    $solrCSV = $solrCSV_init . $solrCSV_odd_out  . "\r\n";
                    // echo "<br>";
                    // ob_clean();
                    print($solrCSV);
                    // echo "<br>";

                    $solrCSV = '';

                    // go to next $docs to get data
                    $incr_num = $incr_num + $key_num[$idlist_unique[$i]];

                    // echo "<br><br>";
                    // var_dump($solrCSV_even);
                    // echo "<br><br>";
                    // var_dump($solrCSV_odd);
                    // echo "<br><br>";
                    // var_dump($solrCSV_even_out);
                    // echo "<br><br>";
                    // var_dump($solrCSV_odd_out);
                    // echo "<br><br>";
                    // var_dump($solrCSV);
                    // echo "<br><br>";
                }
            } else {
                // the case of no results
                $solrCSV = 'No results are found';
                print($solrCSV);
                $solrCSV = '';
            }

        } catch (Exception $e) {

           // // temporarily
           //  $savePath_bg = '/home/testdown/temp1/';
           //  $php_arg_bg = 'dk_error1_bgERROR';
           //  $myfile_bg = fopen("$savePath_bg$php_arg_bg.log", "w");
           //  fwrite($myfile_bg, $e);
           //  fclose($myfile_bg);

            if($e->getCode() == 0) {
                drupal_goto('vbsearch/download/timeout');
                return;
            }
        }

    } else {
        // background process
        // //get username and uid - this does not work in the new way
        // global $user;
        // $user_name = $user->name;
        // $user_id = $user->uid;

        //add this here
        $solrQuery->replaceParam('rows', $results_count);

        //DB entries: create unique id via md5 hash
        $md5hash = 'vbmd_' . md5(uniqid(rand(), true));
        $current_time = time();
        //record to DB as Export STRUCTURE
        $md_contents = 'Export STRUCTURE';
        //current_status: defined to check whether export is ready (1) or not (0). need to be updated at BG process
        $current_status = 0;

        //serialize - be careful for protected members at object: use base64_encode/decode
        $fieldlist_serial = serialize($fieldlist_STRUCTURE);
        $solrQuery_serial = base64_encode(serialize($solrQuery));
        $fieldlist_unserial = unserialize($fieldlist_serial);
        $solrQuery_unserial = unserialize(base64_decode($solrQuery_serial));

        //save to db table - note that results_count is different from actual results in this STRUCTURE
        $db_fields = array('md_userid' => $user_id, 'md_status' => $current_status, 'md_md5hash' => $md5hash, 'md_time' => $current_time, 'md_totalnumber' => $results_count, 'md_contents' => $md_contents, 'md_description' => $md_description, 'md_fieldlist' => $fieldlist_serial, 'md_solrquery' => $solrQuery_serial);
        db_insert('vbsearch_md_background')->fields($db_fields)->execute();

        //for using drush in shell_exec, somehow its path needs to be set - for new export, getcwd() offers correct path without invoking drupal_get_path()
        // $command = '/usr/local/bin/drush php-script ' . getcwd() . '/' . drupal_get_path('module', 'vbsearch_md') . '/vbsearch_md_bg_down.php ' . $md5hash;
        // $command = '/usr/local/bin/drush php-script ' . getcwd() . '/vbsearch_md_bg_structure.php ' . $md5hash;
        // With the change of the base path to DRUPAL_ROOT due to the File Search Fix (VBHELP-403), need to set the path correctly
        $command = '/usr/local/bin/drush php-script ' . drupal_get_path('module','vbsearch_md') . '/vbsearch_md_bg_structure.php ' . $md5hash;

        //run background process
        shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));
        //checking drush errors
        // shell_exec(sprintf('%s > /home/testdown/temp1/list.log 2>/home/testdown/temp1/error.log ', $command));

        // //message for popup
        // $message = 'Export request was sent to a Background Job. \\nIn general, output file will be ready within an hour and its Download link will be provided at "Export" under "Your jobs" in your Profile page. \\nPressing "OK" will redirect to the aformentioned page';
        // // echo "<script type='text/javascript'>alert('$message'); </script>";
        // // echo "<script type='text/javascript'>alert('$message'); window.close();</script>";
        // echo "<script type='text/javascript'>alert('$message'); window.location.href='/users/$user_name/#collapse-Export';</script>";
        // // echo "<script type='text/javascript'>alert('$message'); window.location.href='/jobstatus';</script>";


        // // checking error!
        // // //open file - possibly use DRUPAL_ROOT here?
        // $savePath = '/home/testdown/temp1/';
        // $php_arg = 'dk_error1';
        // $myfile = fopen("$savePath$php_arg.log", "w");
        // $solrCSV = $command;
        // fwrite($myfile, $solrCSV);
        // fclose($myfile);

    }

    // Reset timeout to previous value
    // ini_set('default_socket_timeout', $oldTO);
    // ini_set('max_execution_time', $oldExTime);
 //    ini_set('memory_limit', $oldMemory);
    // ini_set('max_input_time', $oldInputTime);   //DKDK

    return;

} // end of function vbsearch_md_Structure()

?>
