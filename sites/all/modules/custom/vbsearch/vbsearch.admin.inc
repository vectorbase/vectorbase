<?php

/**
 * Creates the VectorBase Search Administration page.
 * It lists the items that can be configured.
 *
 * @return
 *   The output HTML
 */
function vbsearch_admin_menu_page() {
  $item = menu_get_item();
  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', array('content' => $content));
  }
  else {
    $output = t('You do not have any VectorBase Search administrative items.');
  }

  return $output;
}

function solr_fields_edit_form($form, &$form_state, $solr_field = array()) {

  if(empty($solr_field)) {
    unset($solr_field);
  }

  $form['field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Solr Field Name'),
    '#default_value' => isset($solr_field) ? $solr_field['field_name'] : '',
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => isset($solr_field) ? $solr_field['description'] : '',
    '#required' => FALSE,
  );

  $form['solr_parameters'] = array(
    '#type' => 'textfield',
    '#title' => t('Solr Parameters'),
    '#default_value' => isset($solr_field) ? $solr_field['solr_parameters'] : '',
    '#required' => TRUE,
    '#description' => t('Specify the Solr parameters this field will be used in separated by a comma e.g fl, fq, qf etc.'),
  );

  $form['boost'] = array(
    '#type' => 'textfield',
    '#title' => t('Query Field Boost'),
    '#default_value' => isset($solr_field) ? $solr_field['boost'] : '',
    '#required' => FALSE,
    '#description' => t('Specify the importance a field will have in the search.  Currently it uses a query field boost only.'),
  );

  $content_type_options = _vbsearch_get_content_type_options();

  $selected_content_type = isset($form_state['values']['content_type']) ? $form_state['values']['content_type'] : key($content_type_options);

  if (!isset($form_state['indexing_mapping_options'])) {
    $form_state['indexing_mapping_options'] = _vbsearch_get_indexing_mappings_options($solr_field['indexing_mapping_field']);
  }

  $form['indexing'] = array(
    '#title' => t('Content Type to Solr Field Index Map'),
    '#type' => 'fieldset',
    '#description' => t('Lists the content type fields that will be indexed into the solr field'),
  );

  $form['indexing']['content_type'] = array(
    '#title' => 'Select Content Type',
    '#type' => 'select',
    '#options' => $content_type_options,
    '#default_value' => $selected_content_type,
    '#ajax' => array(
      'callback' => 'vbsearch_content_type_dropdown_callback',
      'wrapper' => 'content-type-dependent-replace',
    ),
  );

  $form['indexing']['content_type_dependent'] = array(
    '#title' => 'Select Content Type Field',
    '#type' => 'select',
    '#prefix' => '<div id="content-type-dependent-replace">',
    '#suffix' => '</div>',
    '#options' => _vbsearch_content_type_dependent_options($selected_content_type),
    '#default_value' => isset($form_state['values']['content_type_dependent']) ? $form_state['values']['content_type_dependent'] : '',
  );

  //AJAX button that adds the content type and field selected to the list of
  //content type to solr field indexing map
  $form['indexing']['controls']['add_index_map'] = array(
    '#type' => 'submit',
    '#value' => t('Add index map'),
    '#submit' => array('vbsearch_solr_field_index_map_add'),
    '#ajax' => array(
      'callback' => 'vbsearch_solr_field_index_map_callback',
      'wrapper' => 'indexing-mapping-replace',
    ),
  );

  $form['indexing']['controls']['remove_index_map'] = array(
    '#type' => 'submit',
    '#value' => t('Remove index map'),
    '#submit' => array('vbsearch_solr_field_index_map_remove'),
    '#ajax' => array(
      'callback' => 'vbsearch_solr_field_index_map_callback',
      'wrapper' => 'indexing-mapping-replace',
    ),
  );

  //Build the table header
  $header = array(
    'content_type_name' => array('data' => t('Content Type Name')),
    'content_type_field' => array('data' => t('Content Type Field')),
  );

  $form['indexing']['indexing_mappings'] = array(
    '#type' => 'tableselect',
    '#description' => 'Chosen content type and its fields that will be used as data to index into this Solr field.',
    '#header' => $header,
    '#options' => $form_state['indexing_mapping_options'],
    '#empty' => t('This Solr field is not mapped to any content type fields for indexing.'),
    '#prefix' => '<div id="indexing-mapping-replace">',
    '#suffix' => '</div>',
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['save'] = array(
    '#type' => 'submit',
    //Validate function will be added in the future
    //'#validate' => array('solr_field_edit_validate'),
    '#submit' => array('solr_field_edit_form_submit'),
    '#value' => t('Save'),
  );

  // VB-6815-2 this is for Delete button inside a solr field (when editing) - /solr_fields/solr_field_name form
  if (!empty($solr_field)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#submit' => array('solr_field_edit_form_delete_submit'),
      '#value' => t('Delete'),
      // add simple popup to confirm Delete
      '#attributes' => array('onclick' => 'if(!confirm("Are you sure you want to delete this solr field?")){return false;}'),
    );
  }

  // Ensures destination is an internal URL, builds "cancel" link.
  if (isset($_GET['destination']) && !url_is_external($_GET['destination'])) {
    $destination = $_GET['destination'];
  }
  else {
    $destination = 'admin/config/search/vbsearch/solr_fields';
  }
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $destination,
  );

  return $form;
}

function solr_field_edit_form_submit($form, &$form_state) {
  $record = array(
    'solr_field_description' => $form_state['values']['description'],
    'solr_field_boost' => NULL,
    'solr_field_parameters' => NULL,
    'content_field_solr_mappings' => NULL,
  );

  if (!empty($form_state['values']['boost'])) {
    $record['solr_field_boost'] = intval($form_state['values']['boost']);
  }

  if (!empty($form_state['values']['solr_parameters'])) {
    $record['solr_field_parameters'] = serialize(explode(',', $form_state['values']['solr_parameters']));
  }

  //Every options in the tableselect form field is a field and a value that maps
  //a content type field to a solr field for data for indexing.
  $indexing_mappings = $form['indexing']['indexing_mappings']['#options'];

  if (!empty($indexing_mappings)) {
    $record['content_field_solr_mappings'] = array();
    foreach($indexing_mappings as $indexing_mapping) {
      $content_type_name = $indexing_mapping['content_type_name']['data'];
      $content_type_field = $indexing_mapping['content_type_field']['data'];

      //if (empty($record['content_field_solr_mappings'][$content_type_name])) {
      //  $record['content_field_solr_mappings'][$content_type_name] = array();
      //}

      $record['content_field_solr_mappings'][$content_type_name] = $content_type_field;
    }

    $record['content_field_solr_mappings'] = serialize($record['content_field_solr_mappings']);
  }

  // VB-6815-34 START
  //Insert or update the solr field's configuration settings
  // db_merge() does not work for update as the table does not have primary/unique key: in this case, all are treated as db_insert()
  // add solr_field_name using $record array for either db_insert() or db_update() in the following
    $record_db = $record;
    $record_db['solr_field_name'] = $form_state['values']['field_name'];
  // check #default_value (pre-existing) at field_name: $form_state['complete form']['field_name']['#default_value'] or $form_state['build_info']['args'][0]['field_name']
  // add new field to db table
  if (empty($form_state['complete form']['field_name']['#default_value'])) {
    // check the new field exists
    $query_object = db_query('SELECT id FROM {vbsearch_solr_field_config} WHERE solr_field_name=:field_name', array('field_name'=>$form_state['values']['field_name']));
    if (!$query_object->rowCount()) {
        // add new solr field to db table - db_merge can also be used with key() while db_insert somehow does not accept key()
        db_insert('vbsearch_solr_field_config')
          ->fields($record_db)
          ->execute();
        // db_merge('vbsearch_solr_field_config')
        //   ->key(array('solr_field_name' => $form_state['values']['field_name']))
        //   ->fields($record_db)
        //   ->execute();
        // give a message after done
        drupal_set_message(t('%field_name was newly added.', array('%field_name' => $form_state['values']['field_name'])));

    } else {
        // duplcate, give an error message
        form_set_error('values][field_name',t('%field_name already exists. Please Check the field name if correct or Edit the existing field.', array('%field_name' => $form_state['values']['field_name'])));
    }
  // Edit existing solr field, i.e., update db table
  } else {
    $query_object = db_query('SELECT id FROM {vbsearch_solr_field_config} WHERE solr_field_name=:field_name', array('field_name'=>$form_state['complete form']['field_name']['#default_value']));
    $query_result = $query_object->fetchAssoc();
    // update db table, vbsearch_solr_field_config
    db_update('vbsearch_solr_field_config')
      ->fields($record_db)
      ->condition('id',$query_result['id'])
      ->execute();
    // give a message after done
    drupal_set_message(t('%field_name was updated.', array('%field_name' => $form_state['values']['field_name'])));
  }
  // VB-6815-34 END

  $form_state['redirect'] = 'admin/config/search/vbsearch/solr_fields';
}

// VB-6815-2 this is the action of Delete button inside a solr field (when editing) - /solr_fields/solr_field_name page
function solr_field_edit_form_delete_submit($form, $form_state) {
  $solr_field_name = $form_state['values']['field_name'];

  db_delete('vbsearch_solr_field_config')
    ->condition('solr_field_name',$solr_field_name)
    ->execute();

  drupal_set_message(t('%field_name was deleted.', array('%field_name' => $solr_field_name)));
}


// VB-6815-1 START: this is the action of Delete link at /solr_fields admin UI
function solr_field_delete_confirm($form, &$form_state, $solr_field = 'all') {
// function solr_field_delete_confirm($form, $form_state, $solr_field) {  
  // dpm($solr_field['field_name']);
  return confirm_form($form,
    t('Are you sure you want to delete the solr field, %title?', array('%title' => $solr_field['field_name'])),
    '#overlay=admin/config/search/vbsearch/solr_fields/',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function solr_field_delete_confirm_submit($form, &$form_state) {
  // dpm($form_state);
  $solr_field_name = $form_state['build_info']['args'][0]['field_name'];

  // delete solr field from db table
  db_delete('vbsearch_solr_field_config')
    ->condition('solr_field_name',$solr_field_name)
    ->execute();

  drupal_set_message(t('%field_name was deleted.', array('%field_name' => $solr_field_name)));
  $form_state['redirect'] = 'admin/config/search/vbsearch/solr_fields';
}
// VB-6815-1 END


function vbsearch_solr_searchable_fields(array $form, array $form_state) {
  // VB-6815-3 Add Sort so that the new field can be shown in the last
  // $solr_fields_config = db_query("SELECT * from {vbsearch_solr_field_config}");
  $solr_fields_config = db_query("SELECT * from {vbsearch_solr_field_config} ORDER BY id");
  $headers = '';
  $rows = array();

  $headers = array(
    array('data' => t('Solr Field Name'), 'colspan' => 2),
    array('data' => t('Description'), 'colspan' => 1),
    array('data' => t('Parameters'), 'colspan' => 1),
    array('data' => t('Boost'), 'colspan' => 1),
    array('data' => t('Content Type Mappings'), 'colspan' => 1),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  foreach ($solr_fields_config as $solr_field_config) {
    $ops = array();
    $solr_field_name = $solr_field_config->solr_field_name;
    $solr_field_description = $solr_field_config->solr_field_description;
    $solr_field_boost = $solr_field_config->solr_field_boost;

    $solr_field_parameters = array();
    if (!empty($solr_field_config->solr_field_parameters)) {
      $solr_field_parameters = unserialize($solr_field_config->solr_field_parameters);
    }

    $content_field_solr_mappings = array();
    if (!empty($solr_field_config->content_field_solr_mappings)) {
      foreach(unserialize($solr_field_config->content_field_solr_mappings) as $key => $value) {
        $content_field_solr_mappings[] = $key . '|' . $value;
      }
    }

    $ops['edit'] = array(
      'class' => 'operation',
      'data' => l(t('Edit'),
        'admin/config/search/vbsearch/solr_fields/' . $solr_field_name . '/edit',
        array('query' => array('destination' => current_path()))
      ),
    );

    // VB-6815-1 this is for Delete at /solr_fields admin page
    $ops['delete'] = array(
      'class' => 'operation',
      'data' => l(t('Delete'),
        'admin/config/search/vbsearch/solr_fields/' . $solr_field_name . '/delete',
        array('query' => array('destination' => $_GET['q']))
        // array('query' => array('destination' => current_path()))
      ),
    );

    // dpm($_GET['q']);     //DKDK
    // dpm($ops);           //DKDK

    $solr_field_name_link = l($solr_field_name,
      'admin/config/search/vbsearch/solr_fields/' . $solr_field_name . '/edit',
      array('query' => array('destination' => $_GET['q']))
    );

    $solr_fields_class_row = '';
    $class = '';

    $rows[$solr_field_name] = array('data' =>
      array(
        // Cells
        array(
          'class' => 'status-icon',
          'data' => '<div title="' . $class . '"><span class="element-invisible">' . $class . '</span></div>',
        ),
        array(
          'class' => $solr_fields_class_row,
          'data' => $solr_field_name_link,
        ),
      ),
      'class' => array(drupal_html_class($class)),
    );
    // Add the links to the page

    $description = array(
      array(
        'data' => $solr_field_description == '' ? t('<i>No Description Available</i>') : $solr_field_description,
      ),
    );

    $parameters = array(
      array(
        'data' => implode(',', $solr_field_parameters),
      ),
    );

    $boost = array(
      array(
        'data' => $solr_field_boost == '' ? t('<i>Default</i>') : $solr_field_boost,
      ),
    );

    $field_solr_mappings = array(
      array(
        'data' => implode("\n", $content_field_solr_mappings),
      ),
    );

    $rows[$solr_field_name]['data'] = array_merge($rows[$solr_field_name]['data'], $description);
    $rows[$solr_field_name]['data'] = array_merge($rows[$solr_field_name]['data'], $parameters);
    $rows[$solr_field_name]['data'] = array_merge($rows[$solr_field_name]['data'], $boost);
    $rows[$solr_field_name]['data'] = array_merge($rows[$solr_field_name]['data'], $field_solr_mappings);
    $rows[$solr_field_name]['data'] = array_merge($rows[$solr_field_name]['data'], $ops);
  }

  $form['vbsearch_solr_fields_settings']['actions'] = array(
    '#markup' => '<ul class="action-links">' . drupal_render($actions) . '</ul>',
  );

  if(!empty($rows)) {
    $form['vbsearch_solr_fields_settings']['table'] = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => array_values($rows),
      '#attributes' => array('class' => array('admin-apachesolr')),
    );
  }

  // dpm($form);
  //$vbsearch_name = l($data)
  //return "Here you will be able to see the different search types and manage them";

  return $form;
}

function vbsearch_type_field_config_form($form, &$form_state, $vbsearch_type, $vbsearch_type_field) {
  $search_field_options = _vbsearch_get_search_field_options($vbsearch_type->type);

  //Get the value that will be used to set the value in 'search_field' and also
  //to get the options for the 'search_field_values' form field.
  $selected_search_field = isset($form_state['values']['search_field']) ? $form_state['values']['search_field'] : key($search_field_options);

  $form['field_name'] = array(
    '#type' => 'value',
    '#value' => $vbsearch_type_field['field_name'],
  );

  $form['vbsearch_type'] = array(
    '#type' => 'value',
    '#value' => $vbsearch_type->type,
  );

  $form['solr_mapping'] = array(
    '#title' => t('Solr Fields Mapping'),
    '#type' => 'textfield',
    '#default_value' => isset($vbsearch_type_field) ? $vbsearch_type_field['solr_mapping'] : '',
    '#description' => t('The solr fields to map to.  When searching, the fields listed will be used to search against.'),
    '#autocomplete_path' => 'vbsearch/solr_fields/autocomplete',
  );

  //DKDK VB-8046 Autocomplete option
  $form['autocomplete_option'] = array(
    '#title' => t('Autocomplete suggestion'),
    '#type' => 'select',
    '#options' => array(
      'enable' => 'Enable',
      'disable' => 'Disable',
    ),  
    '#default_value' => isset($vbsearch_type_field['vbsearch_field_autocomplete_option']) ? $vbsearch_type_field['vbsearch_field_autocomplete_option'] : 'enable',    
    '#description' => t('Change option to enable (default) or disable Autocomplete functionality for this field'),
  );

  //Adding an option that allows us to set where we would like the field to get
  //displayed
  $form['display_location'] = array(
    '#title' => t('Display Location'),
    '#type' => 'select',
    '#options' => array(
      'AdvancedSearch' =>'Advanced Search',
      'MainSearch' => 'Main Search'
    ),
    '#default_value' => isset($vbsearch_type_field['display_location']) ? $vbsearch_type_field['display_location'] : 'AdvancedSearch',
    '#description' => t('Select where you would like this field to get displayed, if none selected, it will be displayed in the Advanced Search form by default'),
  );

  $form['visible'] = array(
    '#title' => t('Visibility Behavior'),
    //'#type' => 'textarea',
    //'#default_value' => isset($vbsearch_type_field) ? $vbsearch_type_field['visible'] : '',
    '#type' => 'fieldset',
    '#description' => t('The fields and values that needs to be selected in order for this field to be visible.'),
  );

  //Array to temporarily keep added/removed visible settings
  //Set it to what is in the database first and then use for adding and removing
  //the fields from the table.
  if (!isset($form_state['visible_settings_options'])) {
    $form_state['visible_settings_options'] = _vbsearch_get_field_visible_settings_options($vbsearch_type_field['visible_settings']);
  }

  $form['visible']['search_field'] = array(
    '#title' => 'Select Field',
    '#type' => 'select',
    '#options'=> $search_field_options,
    '#default_value' => $selected_search_field,
    '#ajax' => array(
      'callback' => 'vbsearch_field_dependent_dropdown_callback',
      'wrapper' => 'search-field-dependent-replace',
    ),
  );

  $form['visible']['search_field_dependent'] = array(
    '#title' => 'Select a Field Value',
    '#type' => 'select',
    '#prefix' => '<div id="search-field-dependent-replace">',
    '#suffix' => '</div>',
    //  When the form is rebuilt during ajax processing, the $selected variable
    //  will now have the new values and so the options will change
    '#options' => _vbsearch_search_field_dependent_options($selected_search_field),
    '#default_value' => isset($form_state['values']['search_field_dependent']) ? $form_state['values']['search_field_dependent'] : '',
  );

  //AJAX button that adds the option selected to the list of Visible Settings
  $form['visible']['controls']['add_visible_setting'] = array(
    '#type' => 'submit',
    '#value' => t('Add field setting'),
    '#submit' => array('vbsearch_field_visible_setting_add'),
    '#ajax' => array(
      'callback' => 'vbsearch_field_visible_settings_callback',
      'wrapper' => 'visible-settings-replace',
    ),
  );

  $form['visible']['controls']['remove_visible_setting'] = array(
    '#type' => 'submit',
    '#value' => t('Remove field settings'),
    '#submit' => array('vbsearch_field_visible_setting_remove'),
    '#ajax' => array(
      'callback' => 'vbsearch_field_visible_settings_callback',
      'wrapper' => 'visible-settings-replace',
    ),
  );

  //Build the sortable header
  $header= array(
    'field_name' => array('data' => t('Search Field')),
    'field_value' => array('data' => t('Search Field Value')),
  );

  $form['visible']['visible_settings'] = array(
    '#type' => 'tableselect',
    '#description' => 'Chosen fields and values for field to be visible.',
    '#header' => $header,
    '#options' => $form_state['visible_settings_options'],
    '#empty' => t('The field does not have any visible settings'),
    '#prefix' => '<div id="visible-settings-replace">',
    '#suffix' => '</div>',
  );

  if ($vbsearch_type_field['field_type'] == 'text') {
    $form['placeholder'] = array(
      '#title' => t('Placeholder'),
      '#type' => 'textfield',
      '#default_value' => isset($vbsearch_type_field) ? $vbsearch_type_field['placeholder'] : '',
      '#description' => t('The placeholder you would like to give to the field.  Only available for text fields.'),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => t('/admin/structure/vbsearch/manage/' . $vbsearch_type->type . '/fields'),
  );

  return $form;
}

function vbsearch_type_field_config_form_submit($form, &$form_state) {
  $record = array(
    'vbsearch_type_name' => $form_state['values']['vbsearch_type'],
    'vbsearch_field_placeholder' => $form_state['values']['placeholder'],
    'vbsearch_field_visibility' => NULL,
    'field_solr_mappings' => NULL,
    'vbsearch_field_display_location' => $form_state['values']['display_location'],
    //DKDK VB-8046 autocomplete option  
    'vbsearch_field_autocomplete_option' => $form_state['values']['autocomplete_option'], 
  );

  if (!empty($form_state['values']['solr_mapping'])) {
    $record['field_solr_mappings'] = serialize(explode(',', $form_state['values']['solr_mapping']));
  }

  //Every option in the tableselect form field is a field and value that will
  //make the field being configured visible
  $vbsearch_visible_settings = $form['visible']['visible_settings']['#options'];

  if (!empty($vbsearch_visible_settings)) {
    $record['vbsearch_field_visibility'] = array();
    foreach ($vbsearch_visible_settings as $setting) {
      $vbsearch_field_name = $setting['field_name']['data'];
      $vbsearch_field_value = $setting['field_value']['data'];

      if (empty($record['vbsearch_field_visibility'][$vbsearch_field_name])) {
        $record['vbsearch_field_visibility'][$vbsearch_field_name] = array();
      }

      $record['vbsearch_field_visibility'][$vbsearch_field_name][] = $vbsearch_field_value;
    }

    $record['vbsearch_field_visibility'] = serialize($record['vbsearch_field_visibility']);
  }

  //Insert or Update the field's configuration settings
  db_merge('vbsearch_field_config')
    ->key(array('vbsearch_field_name' => $form_state['values']['field_name']))
    ->fields($record)
    ->execute();

  drupal_set_message(t('Configuration has been saved for %vbsearch_field', array('%vbsearch_field' => $form_state['values']['field_name'])));

  $form_state['redirect'] = 'admin/structure/vbsearch/manage/advanced_search/fields';
}

/**
 * Generates the VectorBase Search type editing form.
 */
function vbsearch_type_form($form, &$form_state, $vbsearch_type, $op = 'edit') {

  if ($op == 'clone') {
    $vbsearch_type->label .= ' (cloned)';
    $vbsearch_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $vbsearch_type->label,
    '#description' => t('The human-readable name of this vbsearch type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($vbsearch_type->type) ? $vbsearch_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $vbsearch_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'vbsearch_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this vbsearch type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($vbsearch_type->description) ? $vbsearch_type->description : '',
    '#description' => t('Description about the vbsearch type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save vbsearch type'),
    '#weight' => 40,
  );

  if (!$vbsearch_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete vbsearch type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('vbsearch_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing vbsearch_type.
 */
function vbsearch_type_form_submit(&$form, &$form_state) {
  $vbsearch_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  vbsearch_type_save($vbsearch_type);

  // Redirect user back to list of vbsearch types.
  $form_state['redirect'] = 'admin/structure/vbsearch';
}

function vbsearch_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/vbsearch/' . $form_state['vbsearch_type']->type . '/delete';
}

/**
 * VectorBase Search type delete form.
 */
function vbsearch_type_form_delete_confirm($form, &$form_state, $vbsearch_type) {
  $form_state['vbsearch_type'] = $vbsearch_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['vbsearch_type_id'] = array('#type' => 'value', '#value' => entity_id('vbsearch_type' ,$vbsearch_type));
  return confirm_form($form,
    t('Are you sure you want to delete vbsearch type %title?', array('%title' => entity_label('vbsearch_type', $vbsearch_type))),
    'vbsearch/' . entity_id('vbsearch_type' ,$vbsearch_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * VectorBase Search type delete form submit handler.
 */
function vbsearch_type_form_delete_confirm_submit($form, &$form_state) {
  $vbsearch_type = $form_state['vbsearch_type'];
  vbsearch_type_delete($vbsearch_type);

  watchdog('vbsearch_type', '@type: deleted %title.', array('@type' => $vbsearch_type->type, '%title' => $vbsearch_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $vbsearch_type->type, '%title' => $vbsearch_type->label)));

  $form_state['redirect'] = 'admin/structure/vbsearch';
}


/**
 * Load Bulk Operations page.
 */
function vbsearch_bulk_ops_page() {
    header("Location: /vbsearch/bulkops");
}



/**
 * Page to select vbsearch Type to add new vbsearch.
 */
function vbsearch_admin_add_page() {
  $items = array();
  foreach (vbsearch_types() as $vbsearch_type_key => $vbsearch_type) {
    $items[] = l(entity_label('vbsearch_type', $vbsearch_type), 'vbsearch/add/' . $vbsearch_type_key);
  }
  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of vbsearch to create.')));
}

/**
 * Add new vbsearch page callback.
 */
function vbsearch_add($type) {
  $vbsearch_type = vbsearch_types($type);

  $vbsearch = entity_create('vbsearch', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('vbsearch_type', $vbsearch_type))));

  $output = drupal_get_form('vbsearch_form', $vbsearch);

  return $output;
}

/**
 * VectorBase Search Form.
 */
function vbsearch_form($form, &$form_state, $vbsearch) {
  $form_state['vbsearch'] = $vbsearch;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $vbsearch->title,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $vbearch->description,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $vbsearch->uid,
  );

  field_attach_form('vbsearch', $vbsearch, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save vbsearch'),
    '#submit' => $submit + array('vbsearch_form_submit'),
  );

  // Show Delete button if we edit vbsearch.
  $vbsearch_id = entity_id('vbsearch' ,$vbsearch);
  if (!empty($vbsearch_id) && vbsearch_access('edit', $vbsearch)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('vbsearch_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'vbsearch_form_validate';

  return $form;
}

function vbsearch_form_validate($form, &$form_state) {

}


/**
 * VectorBase Search submit handler.
 */
function vbsearch_form_submit($form, &$form_state) {
  $vbsearch = $form_state['vbsearch'];

  entity_form_submit_build_entity('vbsearch', $vbsearch, $form, $form_state);

  vbsearch_save($vbsearch);

  $vbsearch_uri = entity_uri('vbsearch', $vbsearch);

  $form_state['redirect'] = $vbsearch_uri['path'];

  drupal_set_message(t('VectorBase Search %title saved.', array('%title' => entity_label('vbsearch', $vbsearch))));
}


function vbsearch_form_submit_delete($form, &$form_state) {
  $vbsearch = $form_state['vbsearch'];
  $vbsearch_uri = entity_uri('vbsearch', $vbsearch);
  $form_state['redirect'] = $vbsearch_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function vbsearch_delete_form($form, &$form_state, $vbsearch) {
  $form_state['vbsearch'] = $vbsearch;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['vbsearch_type_id'] = array('#type' => 'value', '#value' => entity_id('vbsearch' ,$vbsearch));
  $vbsearch_uri = entity_uri('vbsearch', $vbsearch);
  return confirm_form($form,
    t('Are you sure you want to delete vbsearch %title?', array('%title' => entity_label('vbsearch', $vbsearch))),
    $vbsearch_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}


/**
 * Delete form submit handler.
 */
function vbsearch_delete_form_submit($form, &$form_state) {
  $vbsearch = $form_state['vbsearch'];
  vbsearch_delete($vbsearch);

  drupal_set_message(t('VectorBase Search %title deleted.', array('%title' => entity_label('vbsearch', $vbsearch))));

  $form_state['redirect'] = '/vbsearch/';
}

/***
 * AJAX Helper Functions
 *
 * The rest of the document contains all the functions that are used so that the
 * forms can have AJAX functionality
 */

/**
 * The AJAX callback for the search_field form field select list.
 */

function vbsearch_field_dependent_dropdown_callback($form, $form_state) {
  return $form['visible']['search_field_dependent'];
}


/**
 * Helper function to get the options for the search_field select list in the
 * configuration of a field.
 */

function _vbsearch_get_search_field_options($vbsearch_type) {
  $fields_config = db_query("SELECT vbsearch_field_name FROM {vbsearch_field_config}, {field_config} WHERE vbsearch_field_name=field_name AND vbsearch_type_name = :vbsearch_type_name AND type = :field_type", array(
    ':vbsearch_type_name' => $vbsearch_type,
    ':field_type' => 'list_text',
  ));

  $search_field_options = array();
  foreach ($fields_config as $field_config) {
    $search_field_options[$field_config->vbsearch_field_name] = $field_config->vbsearch_field_name;
  }

  return $search_field_options;
}


/**
 * Helper function to get the options of the select form field that is depended
 * on the search_field form field (I know difficult to understand).
 */

function _vbsearch_search_field_dependent_options($selected_search_field) {
  $field_data = db_query("SELECT data FROM {field_config} WHERE field_name = :field_name", array(
       ':field_name' => $selected_search_field,
     )
   )->fetchObject()->data;

  $field_data = unserialize($field_data);
  $search_field_dependent_options = $field_data['settings']['allowed_values'];

  return $search_field_dependent_options;
}


/**
 * Helper function used to add a visibility setting to the Visibility Settings
 * table.
 */

function vbsearch_field_visible_setting_add($form, &$form_state) {
  $vbsearch_field = $form_state['values']['search_field'];
  $vbsearch_field_value = $form_state['values']['search_field_dependent'];

  if(empty($form_state['visible_settings_options'][$vbsearch_field_value])) {
    $form_state['visible_settings_options'][$vbsearch_field_value] = array(
      'field_name' => array(
        'data' => $vbsearch_field,
      ),
      'field_value' => array(
        'data' => $vbsearch_field_value,
      ),
    );
  } else {
    drupal_set_message('Search field and value already added', 'error');
  }

  $form_state['rebuild'] = TRUE;
}


/**
 * Helper function used to remove the selected visibility settings from the
 * visibility settings table.
 */

function vbsearch_field_visible_setting_remove($form, &$form_state) {
  foreach($form_state['values']['visible_settings'] as $value) {
    if ($value) {
      unset($form_state['visible_settings_options'][$value]);
    }
  }

  $form_state['rebuild'] = TRUE;
}


/**
 * Returns the visible settings of the VectorBase Search field being configured
 * in the format needed by the TableSelect forms api field.
 *
 * Only called once to get the initial visible settings table
 */

function _vbsearch_get_field_visible_settings_options($vbsearch_field_visible_settings) {
  $visible_settings_options = array();

  //Put the visible settings stored in the database in a format readable by
  //tableselect form field.
  foreach ($vbsearch_field_visible_settings as $vbsearch_field => $vbsearch_field_values) {
    foreach ($vbsearch_field_values as $vbsearch_field_value) {
      //Need to add the $vbsearch_field_value as the key to return a unique
      //value when it is checked in the table
      $visible_settings_options[$vbsearch_field_value] = array(
        'field_name' => array(
          'data' => $vbsearch_field,
        ),
        'field_value' => array(
          'data' => $vbsearch_field_value,
        ),
      );
    }
  }

  return $visible_settings_options;
}


/**
 * Ajax callback function used to replace the Visibility Behavior table when
 G* configuring a VectorBase Search field
 */

function vbsearch_field_visible_settings_callback($form, $form_state) {
  return $form['visible']['visible_settings'];
}


/**
 * Helper function used to return a list of a nodes in a format that can be used
 * by the select fields so that they can appear as options.
 */

function _vbsearch_get_content_type_options() {
  $content_types = node_type_get_types();
  $content_type_options = array('All' => 'All');

  foreach($content_types as $content_type => $data){
    $content_type_options[$content_type] = $content_type;
  }

  return $content_type_options;
}


/**
 * Helper function used to return the fields associated with a specific content
 * type.
 */

function _vbsearch_content_type_dependent_options($content_type) {
  $entity_info = entity_get_info('node');
  $entity_keys = $entity_info['entity keys'];
  $entity_fields = array();

  foreach ($entity_keys as $key => $value) {
    $entity_fields[$value] = $value;
  }

  if ($content_type != 'All') {
    $content_type_fields = field_info_instances('node', $content_type);
    foreach($content_type_fields as $key => $value) {
      $content_type_fields[$key] = $key;
    }
  } else {
    //Do not know how to get the default fields dynamically so hardcoding them
    $content_type_fields = array('title' => 'title', 'body' => 'body', 'created' => 'created');
  }

  return array_merge($entity_fields, $content_type_fields);;
}


/**
 * AJAX callback function used to update the content_type_dependent select list
 * with fields only available to the content type selected.
 */

function vbsearch_content_type_dropdown_callback($form, $form_state) {
  return $form['indexing']['content_type_dependent'];
}


/**
 * Returns what Node fields will be used as data to index into this solr field.
 *
 * Only called once to get the initial indexing mapping table
 */

function _vbsearch_get_indexing_mappings_options($vbsearch_indexing_mappings) {
  // dpm($vbsearch_indexing_mappings);
  $indexing_mappings_options = array();

  //Put the indexing mappings sotred int he database in the format allowed for
  //the options of the tableselect field
  foreach ($vbsearch_indexing_mappings as $content_type => $content_type_field) {
    //foreach($contet_type_fields as $content_type_field) {
    //Return  the content type and its value as the key
    $indexing_mappings_options[$content_type] = array(
      'content_type_name' => array(
        'data' => $content_type,
      ),
      'content_type_field' => array(
        'data' => $content_type_field,
      ),
    );
    //}
  }

  return $indexing_mappings_options;
}


/**
  *
 **/

function vbsearch_solr_field_index_map_add($form, &$form_state) {
  $content_type = $form_state['values']['content_type'];
  $content_type_field = $form_state['values']['content_type_dependent'];
  $key = $content_type;

  if(empty($form_state['indexing_mapping_options'][$key])) {
    $form_state['indexing_mapping_options'][$key] = array(
      'content_type_name' => array(
        'data' => $content_type,
      ),
      'content_type_field' => array(
        'data' => $content_type_field,
      ),
    );
  } else {
    drupal_set_message('A solr mapping with the content type has already been added.  Remove the existing content type field to solr mapping first.', 'error');
  }

  $form_state['rebuild'] = TRUE;
}


function vbsearch_solr_field_index_map_remove($form, &$form_state) {
  foreach($form_state['values']['indexing_mappings'] as $value) {
    if ($value) {
      unset($form_state['indexing_mapping_options'][$value]);
    }
  }

  $form_state['rebuild'] = TRUE;
}


function vbsearch_solr_field_index_map_callback($form, $form_state) {
  return $form['indexing']['indexing_mappings'];
}
