  function UpdateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");

    if (re.test(url)) {
      if (typeof value !== 'undefined' && value !== null)
        return url.replace(re, '$1' + key + "=" + value + '$2$3');
      else {
        var hash = url.split('#');
        url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
        if (typeof hash[1] !== 'undefined' && hash[1] !== null) 
          url += '#' + hash[1];
        return url;
      }
    }
    else {
      if (typeof value !== 'undefined' && value !== null) {
        var separator = url.indexOf('?') !== -1 ? '&' : '?',
        hash = url.split('#');
        url = hash[0] + separator + key + '=' + value;
        if (typeof hash[1] !== 'undefined' && hash[1] !== null)
          url += '#' + hash[1];
        return url;
      }
      else
        return url;
    }
  }


(function ($) {
  $(document).ready(function () {
    //Go through each popup field and add the click event for the done button
    jQuery("[id$=popup-0]").each( function() {
     jQuery('#'+ jQuery(this).attr('id')).focus(function() {
       var thisCalendar = $(this);
       jQuery('.ui-datepicker-calendar').detach();
       jQuery('.ui-datepicker-close').click(function() {
         var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
         var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
         thisCalendar.datepicker('setDate', new Date(year, month, 1));
       });
     });
    });


    jQuery('.fieldset-wrapper').attr('id','advanced-search-wrapper');

    jQuery('#edit-field-search-site-und').change(function(event) {
    
      var hiddenTextfields = jQuery(':text',':hidden');
      var length = hiddenTextfields.length;

      if(!jQuery('#edit-field-search-popbio-subdomain-und').is(':visible')) {
          jQuery("#edit-field-search-popbio-subdomain-und").val("All").trigger("change");
      };
      if(!jQuery('#edit-field-search-exp-subdomain-und').is(':visible')) {
        jQuery("#edit-field-search-exp-subdomain-und").val("All").trigger("change");
      };

      for (var i = 0; i < length; ++i) {
        if (jQuery('#' + hiddenTextfields[i].id).closest('.locentryradius-field').length == 0) {
          jQuery('#' + hiddenTextfields[i].id).val('');
        }
      }

      if (jQuery('#edit-field-search-site').val() == 'Genome') {
        jQuery('#edit-field-search-pubmed').val('PMID:');
        jQuery('#edit-field-search-go-cvterms').val('GO:');
        jQuery('#edit-field-search-pathway').val('KEGG:');
      }

      jQuery('#edit-field-search-expt-factors option:selected').removeAttr('selected')
      jQuery('#edit-date option:selected').removeAttr('selected')
      jQuery("#edit-field-search-has-geodata").val("All").trigger("change");

    });

    jQuery('#edit-field-search-popbio-subdomain-und').change(function(event) {
   
      var hiddenTextfields = jQuery(':text',':hidden');
      var length = hiddenTextfields.length;
      for (var i = 0; i < length; ++i) {
        if (jQuery('#' + hiddenTextfields[i].id).closest('.locentryradius-field').length == 0) {
          $('#' + hiddenTextfields[i].id).val('');
        }
      }

      jQuery('#edit-field-search-date option:selected').removeAttr('selected')
      jQuery("#edit-field-search-has-geodata").val("all").trigger("change");
    });

    jQuery('#edit-field-search-exp-subdomain-und').change(function(event) {
      var hiddenTextfields = jQuery(':text',':hidden');
      var length = hiddenTextfields.length;
      for (var i = 0; i < length; ++i) {
        if (jQuery('#' + hiddenTextfields[i].id).closet('.locentryradius-field').length ==0) {
          $('#' + hiddenTextfields[i].id).val('');
        }
      }

      if(!jQuery('#edit-field-search-exp-subdomain-und').is(':visible')) {
        jQuery("#edit-field-search-popbio-subdomain-und").val("All").trigger("change");
      };

      jQuery('#edit-field-search-expt-factors option:selected').removeAttr('selected')
    });

    jQuery("#edit-advanced-search a").click(function() {
        var url = window.location.href;
        if(url.indexOf('?as=True') != -1) {
          $("#search-form").attr("action", UpdateQueryString('as'));
        } else if (url.indexOf('&as=True') != -1) {
          $("#search-form").attr("action", UpdateQueryString('as'));
        } else {
          $("#search-form").attr("action", UpdateQueryString('as', 'True'));
        }
    });
  
    jQuery(".ui-datepicker-close").click(function() {
      var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
      $(this).datepicker("setDate", new Date(year, month, 1));
    });
  });
})(jQuery);
