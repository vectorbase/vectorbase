/*
 * Javascript for the About page.
 */

(function($) {
  $(function() {
    /*
     * This section handles the page's slideshows
     */

    // Start all slideshows at first slide
    $('.slideshow-container', '#use-cases-wrapper').each(function() {
      aboutShowSlide(this, 0);
    });

    // Previous slide button listener
    $('.prev', '#use-cases-wrapper').click(function() {
      var $slideshow = $(this).closest('.slideshow-container');
      var index = $slideshow.find('.active-slide').index();
      aboutShowSlide($slideshow, index -= 1);
    });

    // Next slide button listener
    $('.next', '#use-cases-wrapper').click(function() {
      var $slideshow = $(this).closest('.slideshow-container');
      var index = $slideshow.find('.active-slide').index();
      aboutShowSlide($slideshow, index += 1);
    });

    // Slide indicator listener ('dot')
    $('.dot', '#use-cases-wrapper').click(function() {
      // Find out which dot was clicked
      var n = $(this).index();
      var $slideshow = $(this).closest('.slideshow-container');
      aboutShowSlide($slideshow, n);
    });

    // Show a slide given the slideshow object and the slide's number
    function aboutShowSlide($slideshow, n) {
      var $slides = $($slideshow).find('.slide');
      var $dots = $($slideshow).find('.dot');

      // Wrap slideshow
      if (n >= $slides.length) {
        slideIndex = 0;
      } 
      else if (n < 0) {
        slideIndex = $slides.length - 1
      }
      else {
        slideIndex = n;
      }

      $($slides).removeClass('active-slide'); 
      $($dots).removeClass('active-dot');
      $slides[slideIndex].className += ' active-slide'; 
      $dots[slideIndex].className += ' active-dot';
    }

    // Allow keyboard controls for fullscreen slideshow
    $(document).keyup(function(event) {
      var $overlay = $('#image-overlay');

      if ($overlay.css('display') === 'block') {
        var $slideshow = $overlay.find('.slideshow-container');

        switch (event.which) {
          // Right arrow
          case 39:
            $slideshow.find('.next').click();
            break;
          // Left arrow
          case 37:
            $slideshow.find('.prev').click();
            break;
          // Escape
          case 27:
            $overlay.click();
            break;
        }
      }
    });


    /*
     * This section handles the image overlay
     */

    // Show overlay when an image is clicked
    $('.use-case-image', '#use-cases-wrapper').click(function() {
      var $container = $(this).closest('.fullscreen-container');

      // Copy so we don't move the existing object
      // clone(true) because we want to keep most event handlers
      // But do need to remove image click event
      var $copy = $($container).clone(true);
      $copy.find('.use-case-image').unbind('click');

      // Also remove fade animation
      $copy.find('.slide.fade').removeClass('fade');

      // Put $copy into #image-overlay and display overlay
      $('#image-overlay').html($copy).css('display', 'block');
    });

    // Hide overlay when it's clicked
    $('#image-overlay').click(function(event) {
      var target = event.target;

      // Only hide if the user clicked on the overlay background
      // First condition is for image overlay, second is for slideshow overlay
      if (target === this || $(target)[0] === $(this).find('.fullscreen-container')[0]) {
        $(this).css('display', 'none');
      }
    });
  })
})(jQuery);

