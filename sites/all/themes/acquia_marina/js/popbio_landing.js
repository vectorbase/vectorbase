(function($) {
  $(function() {
    $("#map_views_wrapper").mouseenter(function() {
      $(this).children("#map_views_list").stop().css({'height': 'auto'}).slideDown(100);
      $(this).find(".fa-caret-down").css({
        'transform': 'rotate(-180deg) translate(0, 50%)'
      });
    })

    $("#map_views_wrapper").mouseleave(function() {
      $(this).children("#map_views_list").stop().slideUp(100, function() {
        $(this).css({'height': 'auto'});
      });

      $(this).find(".fa-caret-down").css({
        'transform': 'rotate(0deg) translate(0, -50%)'
      });
    });
  })
})(jQuery);

