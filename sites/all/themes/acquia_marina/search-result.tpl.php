<?php

/**
 * @file
 * Default theme implementation for displaying a single search result.
 *
 * This template renders a single search result and is collected into
 * search-results.tpl.php. This and the parent template are
 * dependent to one another sharing the markup for definition lists.
 *
 * Available variables:
 * - $url: URL of the result.
 * - $title: Title of the result.
 * - $snippet: A small preview of the result. Does not apply to user searches.
 * - $info: String of all the meta information ready for print. Does not apply
 *   to user searches.
 * - $info_split: Contains same data as $info, split into a keyed array.
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Default keys within $info_split:
 * - $info_split['type']: Node type (or item type string supplied by module).
 * - $info_split['user']: Author of the node linked to users profile. Depends
 *   on permission.
 * - $info_split['date']: Last update of the node. Short formatted.
 * - $info_split['comment']: Number of comments output as "% comments", %
 *   being the count. Depends on comment.module.
 *
 * Other variables:
 * - $classes_array: Array of HTML class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $title_attributes_array: Array of HTML attributes for the title. It is
 *   flattened into a string within the variable $title_attributes.
 * - $content_attributes_array: Array of HTML attributes for the content. It is
 *   flattened into a string within the variable $content_attributes.
 *
 * Since $info_split is keyed, a direct print of the item is possible.
 * This array does not apply to user searches so it is recommended to check
 * for its existence before printing. The default keys of 'type', 'user' and
 * 'date' always exist for node searches. Modules may provide other data.
 * @code
 *   <?php if (isset($info_split['comment'])): ?>
 *     <span class="info-comment">
 *       <?php print $info_split['comment']; ?>
 *     </span>
 *   <?php endif; ?>
 * @endcode
 *
 * To check for all available data within $info_split, use the code below.
 * @code
 *   <?php print '<pre>'. check_plain(print_r($info_split, 1)) .'</pre>'; ?>
 * @endcode
 *
 * @see template_preprocess()
 * @see template_preprocess_search_result()
 * @see template_process()
 */

  $displayTitle = $title;
  if (isset($result['fields']['site'])){
      $siteType = $result['fields']['site'];
  }
  $hitUrl = $result['fields']['url'];
  //drupal_set_message($hitUrl);
  //dpm($result['fields']);
  if (is_null($result['fields']['url'])) {
    $hitUrl = "/vbsearch/details/" . $result['fields']['id'];
  }
  if (isset($result['fields']['bundle_name'])){
      $hitBundle = $result['fields']['bundle_name'];
  }
  if (isset($result['fields']['species'])){
      $hitSpecies = $result['fields']['species'][0];
      $hitSpeciesCategory = $result['fields']['species'];
  }
  // DKDK VB-7799 adding the case of Variation and its Sub-Domains
  if ($siteType == 'Variation' && isset($result['fields']['strain'])) {
    $hitStrainCategory = $result['fields']['strain'][0];
  }
  // DKDK VB-7822 Genome
  if (($siteType == 'Genome' || $siteType == 'Variation') && isset($result['fields']['reference_genome_s'])) {
    $variationSpecies = $result['fields']['reference_genome_s'];
  }
  if (isset($result['fields']['label'])) {
    $label = $result['fields']['label'];
  }

  if (isset($result['fields']['seq_region_name']) && isset($result['fields']['seq_region_start']) && isset($result['fields']['seq_region_end'])) {
    $accession = $result['fields']['accession'];
    // DKDK VB-7822 Genome: no need to use $geneSpecies
    $seq_region_name = $result['fields']['seq_region_name'];
    $seq_region_start = $result['fields']['seq_region_start'];
    $seq_region_end = $result['fields']['seq_region_end'];
    // DKDK VB-7822 Genome: change $locationUrl with $variationSpecies referring to reference_genome_s
    $locationUrl = "/" . $variationSpecies . "/Location/View?db=core;g=" . $accession . ";r=" . $seq_region_name . ":" . $seq_region_start . "-" . $seq_region_end;    
    $locationTitle = $seq_region_name . ":" . $seq_region_start . "-" . $seq_region_end;
    // DKDK VB-7799 adding the case of Variation and its Sub-Domains
    if ($hitBundle == 'Short variations') {
      $locationUrl = "/" . $variationSpecies . "/Location/View?db=core;r=" . $seq_region_name . ":" . $seq_region_start . "-" . $seq_region_end . ";v=" . $label . ";vdb=variation";        
    } elseif ($hitBundle == 'Structural variations') {
      $locationUrl = "/" . $variationSpecies . "/Location/View?db=core;r=" . $seq_region_name . ":" . $seq_region_start . "-" . $seq_region_end . ";sv=" . $label . ";vdb=variation";
    }    
  }
  $hitDesc = $result['fields']['description'];
  if (strlen($hitDesc) > 200) {
    $hitDesc = substr($hitDesc, 0, 200) . "...";
  }

?>

<?php print render($title_prefix); ?>

<div class="search_result_item <?php print $search_zebra; ?>">
  <h3 id="result_title"><a href="<?php print $hitUrl; ?>"><?php print $displayTitle; ?></a></h3>
  <div class="display_fields">
  <div class="main_display_fields">
    <div class="search_result_item_detail">
      <?php print $siteType; ?>  > <?php print $hitBundle; ?>
    </div>
    <div> 
      <?php if (isset($hitDesc) && strlen($hitDesc) > 1) {
        print strip_tags($hitDesc);
      } else {
        print "No metadata available for this content.";
      }?>
    </div>
  </div>
  <div class="extra_display_fields">
    <!-- DKDK VB-7799 Add Species info for Variation at if: $siteType == 'Variation' -->
    <?php if (isset($hitSpeciesCategory) && ($siteType == 'Genome' || $hitBundle == 'RNA-Seq tracks' || $hitBundle == 'RNA-Seq track groups' || $siteType == 'Variation')) {
      foreach($hitSpeciesCategory as $value) {
        print '<p><span>Species:</span> '. $value .' </p>';
      }
    }?>
    <!-- DKDK VB-7799 Add Strain info for Variation -->
    <?php if (isset($hitStrainCategory) && $siteType == 'Variation') {
      print '<p><span>Strain:</span> '. $hitStrainCategory . ' </p>';
    }?>
    <!-- DKDK VB-7799 Add Location info for Variation (Short and Structural variations only)-->
    <?php if (isset($locationUrl) && ($siteType == 'Genome' || $hitBundle == 'Short variations' || $hitBundle == 'Structural variations')) {
      print '<p><span>Location:</span> '. '<a href="' . $locationUrl . '">' . $locationTitle . '</a></p>';
    }?>
  </div>
  </div>
</div>
