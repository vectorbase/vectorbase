<?php
/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 */
?>
<?php if ($search_results): ?>
<!--<div id="search_filter_div">i-->
<?php
  //Adding block through code instead of the UI because of position issues.
  //Might revise this when the way the facets are done is redone
/*  $facetBlock = block_load('vbsearch_facets', 'vbsearch_facets');
  $block = _block_get_renderable_array(_block_render_blocks(array($facetBlock)));
  $output = drupal_render($block);
  print $output;*/
?>
<!--</div>-->
<div style="width:72%;float:right;padding:10px;">
  <!--<h2><?php print t('Search results');?></h2>-->
	<?php
	global $pager_page_array, $pager_total_items;
	$pager_amount = 20;
	$count = variable_get('apachesolr_rows', $pager_amount);
	$start = $pager_page_array[0] * $count + 1;
	$end = $start + $count - 1;
	if( $end > $pager_total_items[0] ){
	  $end = $pager_total_items[0];
	}
	print t('%start-%end OF <span id="total_items"> @total </span>  RESULTS', 
	  array(
	    '%start' => $start,
      '%end' => $end,
      '@total' => $pager_total_items[0],
	  )
	);
	?>
      <?php if ($pager_total_items[0] == 1) { 
        $link_start = strpos($search_results, 'href="')+6;
	$link_end = strpos($search_results, '"', $link_start);
	$list = substr($search_results, $link_start, $link_end-$link_start);
	?>
	<script>
	    window.location = "<?php print $list; ?>";
	</script>
      <?php } ?>


  <?php print $pager; ?>
  <!--<table>
    <tr><th>Result</th><th>Page Type</th><th>Domain</th><th>Species</th></tr>
  -->
  <?php
    //Adding block through code instead of the UI because of position issues.
    $cardBlock = block_load('vbsearch_card', 'knowledge_card');
    $block = _block_get_renderable_array(_block_render_blocks(array($cardBlock)));
    $output = drupal_render($block);
    print $output;
  ?>

  <?php print $search_results; ?>
  <!--</table>-->
  <?php print $pager; ?>
</div>
<?php else : ?>
  <h2><?php print t('Your search yielded no results');?></h2>
  <?php print search_help('search#noresults', drupal_help_arg()); ?>

<?php endif; ?>
