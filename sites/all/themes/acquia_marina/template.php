<?php

function acquia_marina_preprocess_node(&$variables) {
  $pth = drupal_get_path_alias("node/{$variables['nid']}");
  $pthArr = explode('/', $pth);
  $nodeUrlName = end($pthArr);
  $themePath = drupal_get_path('theme', 'acquia_marina');
  switch($nodeUrlName) {	
    case 'transcriptomes':
      drupal_add_css($themePath . '/css/table-formatting.css');
      break;
    case 'about':
      drupal_add_css($themePath . '/css/about.css');
      drupal_add_js($themePath . '/js/about.js');
      break;
    case 'downloads':
      drupal_add_css($themePath . '/css/table-formatting.css');
      break;
    case 'tools':
      drupal_add_css($themePath . '/css/table-formatting.css');
      break;
    case 'data':
      drupal_add_css($themePath . '/css/table-formatting.css');
      break;
    case 'community':
      drupal_add_css($themePath . '/css/table-formatting.css');
      break;
    case 'help':
      drupal_add_css($themePath . '/css/table-formatting.css');
      break;
    case 'zika':
      drupal_add_css($themePath . '/css/zika-formatting.css');
      break;
    case 'popbio':
      drupal_add_css($themePath . '/css/popbio_landing.css');
      drupal_add_js($themePath . '/js/popbio_landing.js');
      break;
  }

  // Used to fix the CSS issue in the Organisms page
  $node = $variables['node'];
  if($node->type == 'organism') {
    drupal_add_css($themePath . '/css/organism-table-formatting.css');
  }
}

function acquia_marina_preprocess_page(&$vars) {
	// pass module paths to javascript
    drupal_add_js(array('blast' => array('blastPath' => drupal_get_path('module','blast'))), 'setting');
    drupal_add_js(array('clustalw' => array('clustalwPath' => drupal_get_path('module','clustalw'))), 'setting');
    drupal_add_js(array('hmmer' => array('hmmerPath' => drupal_get_path('module','hmmer'))), 'setting');
    drupal_add_js(array('xgrid' => array('xgridPath' => drupal_get_path('module','xgrid'))), 'setting');
/*
	// this is a nice idea but creates a completely blank page (no header/footer)
	// custom 404 page: use page--404.tpl.php
	$status = drupal_get_http_header("status");  
	if($status == "404 Not Found")
	  $vars['theme_hook_suggestions'][] = 'page__404';
*/

}


function acquia_marina_preprocess_html(&$vars) {
  // set site tiles and background color css for dev, pre, and !www here

  //bender is dev
  if(strstr(gethostname(),"bender")){
    variable_set('site_name', 'Dev.VectorBase');
    drupal_add_css('#site-name a{background-color: rgba(255,0,0,0.4);}', array('group' => CSS_THEME, 'type' => 'inline'));

  //adama is pre
  }else if(strstr(gethostname(),"adama")){
    variable_set('site_name', 'Pre.VectorBase');
    drupal_add_css('#site-name a{background-color: rgba(0,0,255,0.4);}', array('group' => CSS_THEME, 'type' => 'inline'));

  //bella is live. if not bella, show the name of this machine
  } else if (strstr(gethostname(),"bella")){
    variable_set('site_name', 'VectorBase');
  }else if($_SERVER['SERVER_NAME']!='www.vectorbase.org' && $_SERVER['SERVER_NAME']!='vectorbase.org' && strstr(gethostname(),"bella") ){
   $newName=strstr($_SERVER['SERVER_NAME'],'.',TRUE);
    variable_set('site_name', ucfirst($newName).'.VectorBase');
    drupal_add_css('#site-name a{background-color: rgba(0,255,0,0.4);}', array('group' => CSS_THEME, 'type' => 'inline'));
  }

  if($vars['head_title_array']['title'] === 'Anopheles gambiae') {
	drupal_add_css(drupal_get_path('theme', 'acquia_marina') . '/css/cytogenetic-map-imgs.css');	
  }


}


function acquia_marina_link($variables) {
	/*$menuObject = FALSE;
	  $menuObject = menu_get_object();
	  if ($menuObject && property_exists($menuObject,'type') && ($menuObject->type == 'navigation_page')) {
	  $variables['options']['html'] = FALSE;
	  } else {
	  $variables['options']['html'] = TRUE;
	  }*/
	$variables['options']['html'] = TRUE;
	return '<a href="' . check_plain(url($variables['path'], $variables['options'])) . '"' . drupal_attributes($variables['options']['attributes']) . '>' . ($variables['options']['html'] ? $variables['text'] : check_plain($variables['text'])) . '</a>';
}

function acquia_marina_apachesolr_search_noresults() {

  /* ideally this method should be overridden elsewhere.
   * also, we should have the query parameters stored in $query, so something is off wiht our $query = apachesolr_current_query() command
   * we'll use $_GET and not worry about it for now, though
   */

  global $user;
  if($user->uid) {
    $sid = $user->sid;
  }
  else {
    session_start();
    $sid = session_id();
  }

  //Adding CSS for the noresults page
  drupal_add_css(drupal_get_path('module', 'vbsearch') . '/css/no_results.css');
  $new_query = '/search/site/';
  $new_query_terms = '';
  $searched_value = '';
  $suggestion = '';
  $environments = apachesolr_load_all_environments();
  foreach ($environments as $env_id => $environment) {
    if (apachesolr_has_searched($env_id) && !apachesolr_suppress_blocks($env_id) ) {
      $query = apachesolr_current_query($env_id);
      if ($query) {
        $searched_value = $query->getParam('q');
        $params = explode(' ', $query->getParam('q'));
        foreach ($params as $param=>$value){
          if ($value != '*') {
            $new_query_terms = $new_query_terms . '*' .  $value . '* ';
          }
        }
      }
    }
  }

    if(isset($_GET['search_id'])) {

    $vbsearch_instance = vbsearch_load($_GET['search_id']);
    $advancedSearchTerms = '';

    $url = $_SERVER['REQUEST_URI'];
    if (strstr($url, "?")) {
        $terms = preg_split('/\?/', $url);
        $terms = $terms[0];
    }
    else {
        $terms = $url;
    }
    $terms = preg_split('/\//',$terms);
    $searchTerms = ' *' . $terms[3] . '*';
    if(strstr($searchTerms, "%")) $searchTerms = '';

    //Adding ntoe here that this needs to get improved
    foreach($vbsearch_instance as $key=>$value) {
      if($value != '' AND $value != '*' AND $key != 'sites' AND $key !='bundle_name' AND $key != 'collapsed'
        AND $key != 'get_site' AND $key != 'experimental_factors' AND $key != 'has_geodata' AND $key != 'date'
        AND $key != 'date_values' AND $key != 'collection_date' AND $key != 'experimental_factors_values'
        AND $key != 'sample_type_values' AND $key != 'vbsid' AND $key != 'type' AND $key != 'title' AND $key != 'uid'
        AND $key != 'created' AND $key != 'changed' AND (isset($value['und'][0]['value']) AND $value['und'][0]['value'] != '') AND $key != 'field_search_site'
        AND $key != 'field_search_has_geodata' AND $key != 'field_search_popbio_subdomain'
        AND $key != 'field_search_exp_subdomain' AND $key != 'field_search_expt_factors') {
          $advancedSearchTerms = $advancedSearchTerms . ' *' . $value['und'][0]['value'] . '*';
      }
    }

    if($searchTerms != '') {
      $advancedSuggestion = '<br /> You can try using wildcards on the terms entered in the Search terms: ' . '<b>' . $searchTerms . '</b>';
        if($advancedSearchTerms != '') {
            $advancedSuggestion = $advancedSuggestion . '<br /> You can also use wildcards on the terms entered in the Advanced Search form: ' . $advancedSearchTerms;
        }
    }
    else if($searchTerms == ''){
        if($advancedSearchTerms != '') {
            $advancedSuggestion = '<br /> You can try using wildcards on the terms entered in the Advanced Search form: ' . '<b>' . $advancedSearchTerms . '</b>';
        }
    }
  }

  $query_result = 'No Results Found.';
  $query_warning = '';
  if (isset($_GET['site'])) {
    $query_warning .= 'Searching the <em>' . trim($_GET['site'], "\"") . '</em> domain';
  }
  if (isset($_GET['bundle_name'])) {
    if ($query_warning == '') {
      $query_warning .= 'Searching the <em>' . trim($_GET['bundle_name'], "\"") . '</em> subdomain';
    }
    else {
      $query_warning .= ' and the <em>' . trim($_GET['bundle_name'], "\"") . '</em> subdomain';
    }
  }
  if (isset($_GET['species_category'])) {
    if ($query_warning == '') {
      $query_warning .= 'Searching the <em>' . trim($_GET['species_category'], "\"") . '</em> species category';
    }
    else {
      $query_warning .= ' and the <em>' . trim($_GET['species_category'], "\"") . '</em> species category';
    }
  }

  if ($new_query_terms) { // only offer a suggestion if a term was actually queried (i.e. search field isn't blank)
    //$suggestion =  ' You can try your search again using wildcards: <b><a href="'. $new_query . $new_query_terms . '">' . htmlspecialchars($new_query_terms) . '</a></b>' . $advancedSuggestion;
    $suggestion =  ' You can try your search again using wildcards in the search box: <b>' . htmlspecialchars($new_query_terms) . '</b>' . $advancedSuggestion;
  }
  else if (isset($advancedSearchTerms) && $advancedSearchTerms) {
    $suggestion = $advancedSuggestion;
  }

  if (strlen($query_warning) > 1) {
    $query_warning .= ': ';
    if ($suggestion == '') {
      $suggestion .= 'You can try <a href="'. $new_query . $searched_value . '">clearing search filters</a>.';
    }
    else {
      if ($advancedSearchTerms) {
      	$suggestion .= ', or by <a href="'. $new_query . $searched_value . '?as=True&search_id='. $_GET['search_id'] . '">clearing search filters</a>.';
      }
      else {
        $suggestion .= ', or by <a href="'. $new_query . $searched_value . '">clearing search filters</a>.';
      }
    }
  }

  return t('<br /><h3>' . $query_warning . $query_result . $suggestion . '</h3> <p>The following tips may be helpful in refining or expanding your search query:</p><ul>
    <li>Use the asterisk wildcard character (*) to broaden  matching criteria.</li>
    <li>Check if your spelling is correct, or try removing filters.</li>
    <li>Remove quotes around phrases to match each word individually: <em>"Anopheles gambiae"</em> will return fewer results than <em>Anopheles gambiae</em>.</li>
    <li>You can require or exclude terms using + and -: <em>Anopheles +gambiae </em> will require a match on <em>gambiae</em> while <em>Anopheles -gambiae</em> will exclude results that contain <em>gambiae</em>.</li>
    </ul>');

}



function acquia_marina_taxonomy_subterms_termsmap($terms)
{


/*  drupal_add_css(drupal_get_path('module', 'taxonomy_subterms') . '/taxonomy_subterms.css');
  $add_descriptions = variable_get('taxonomy_subterms_subterms_map_description', 'no')=='yes';
        
  $output = array();
  foreach ($terms as $term)
  {
    if ($term->description && $add_descriptions) {
      $desc = array("#prefix" => "<span class='taxonomy-term-description'>", 
                        "#markup" => filter_xss_admin($term->description),
                        "#suffix" => '</span>');
    } else { 
      $desc = NULL;
    }
   
    $output[] = array("#prefix" => '<li class="taxonomy-term">',
                      "#markup" => l($term->name, "taxonomy/term/". ($term->tid) ),
                      "description" => $desc,
                      "#suffix" => '</li>');
  }
  return array("#prefix" => '<ul id="taxonomy-subterms-map">',
                "map" => $output,
                "#suffix" => '</ul>',
                "#weight" => 1);*/
  drupal_add_css(drupal_get_path('module', 'taxonomy_subterms') . '/taxonomy_subterms.css');
  $add_descriptions = variable_get('taxonomy_subterms_subterms_map_description', 'no')=='yes';
  $output = array();
  //drupal_set_message(print_r($terms));
  $prevdepth = 0;
  foreach ($terms as $term)
  {
    if ($term->description && $add_descriptions) {
      $desc = array("#prefix" => "<span class='taxonomy-term-description'>",
                        "#markup" => filter_xss_admin($term->description),
                        "#suffix" => '</span>');
    } else {
      $desc = NULL;
    }
    for ($i=$term->depth; $i<$prevdepth; $i++) {
        $output[] = array("#markup" => '</ul>');
    }
    for ($i=$prevdepth; $i<$term->depth; $i++) {
        $output[] = array("#markup" => '<ul style="margin-top:0;margin-bottom:0;">');
    }
    if (property_exists($term,'taxonname')) {
        $thisTaxon = $term->taxonname;
    } else {
        $thisTaxon = 'Unclassified';
    }
    $linkClass = $linkClass = array('attributes'=>array('class'=>array('taxonomy-term taxonomic-rank-' . strtolower(preg_replace('~[^\\pL\d]+~u','-',$thisTaxon)))));
    if ($thisTaxon == 'Species'){
        //If the current taxon is the "species" name, then don't use a link (it is self-referential)
        $output[] = array("#prefix" => '<li class="taxonomy-term">' . $thisTaxon . ': ',
                      "#markup" => $term->name,
                      "description" => $desc,
                      "#suffix" => '</li>');
    } else {
        //Otherwise, add a link for navigation to the term
        $output[] = array("#prefix" => '<li class="taxonomy-term">' . $thisTaxon . ': ',
                      "#markup" => l($term->name, "taxonomy/term/". ($term->tid), $linkClass ),
                      "description" => $desc,
                      "#suffix" => '</li>');
    }
    $prevdepth = $term->depth;
  }
  //drupal_set_message(print_r($output));
  return array("#prefix" => '<ul id="taxonomy-subterms-map">',
                "map" => $output,
                "#suffix" => '</ul>',
                "#weight" => 1);

}

function acquia_marina_breadcrumb($variables) {
  //Some pages do not have a breadcrumb so need to make sure a breadcrumb is
  //present
  if (!empty($variables['breadcrumb'])) {
    end($variables['breadcrumb']);
    $key = key($variables['breadcrumb']);
    $last_item = $variables['breadcrumb'][$key];
    //For some reason I need to pass it to this function in order
    //for the strip_tags function to work
    //Pretty sure there are other ways, but this is what I found
    $last_item = drupal_html_to_text($last_item, array());
    $variables['breadcrumb'][$key] = strip_tags($last_item);

    $sep = ' &raquo; ';
    return implode($sep, $variables['breadcrumb']);
  }
}

/**
 * Implements theme_pager_link()
 *
 * Overriding the pager_link() function provided by Drupal to add more
 * attributes to the link.
 *
 * @param $variables
 *  Contains the pager info
 */
function acquia_marina_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];
  $page = isset($_GET['page']) ? $_GET['page'] : '';

  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }
  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }

  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }

    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array(
        '@number' => $text,
      ));
    }
  }


  $attributes['href'] = url($_GET['q'], array(
    'query' => $query,
  ));

  //Adding pager-link class to prevent GTM event from firing
  $attributes['class'] = t('pager-link');

  return '<a' . drupal_attributes($attributes) . '>' . check_plain($text) . '</a>';
}
